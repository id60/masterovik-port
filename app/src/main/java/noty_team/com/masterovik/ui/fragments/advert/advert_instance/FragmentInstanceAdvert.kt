package noty_team.com.masterovik.ui.fragments.advert.advert_instance

import android.arch.lifecycle.ViewModelProvider
import android.os.Bundle
import android.view.View
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_current_advert.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.api.response.city.cities_list.ResponseItem
import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.ui.adapters.items_adapter.CatalogAdvertItem
import noty_team.com.masterovik.ui.fragments.cities_list.CitiesListFragmentViewModel
import noty_team.com.masterovik.ui.fragments.orders.profile_order.FragmentProfileOrder
import noty_team.com.masterovik.utils.Failure
import noty_team.com.masterovik.utils.Success
import noty_team.com.masterovik.utils.injectViewModel
import noty_team.com.masterovik.utils.observe

class FragmentInstanceAdvert : BaseFragment<CitiesListFragmentViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): CitiesListFragmentViewModel {
        return injectViewModel(viewModelFactory)
    }

    companion object {
        private const val ADVERT_DATA_KEY = "ADVERT_DATA_KEY"
        fun newInstance(item: CatalogAdvertItem): FragmentInstanceAdvert {
            return FragmentInstanceAdvert().apply {
                val data = Gson().toJson(item)
                arguments = Bundle().apply {
                    putString(ADVERT_DATA_KEY, data)
                }
            }
        }
        fun getAdvertData(args: Bundle?): CatalogAdvertItem {
            val dataString = args?.getString(ADVERT_DATA_KEY)?:""
            if (dataString.isNotEmpty()){
                Gson().fromJson(dataString, CatalogAdvertItem::class.java)?.let {
                    return it
                }
            }
            return CatalogAdvertItem()
        }
    }

    override fun layout() = R.layout.fragment_current_advert

    override fun initialization(view: View, isFirstInit: Boolean) {
        toolbarSizeListener.hideEmptyToolbarSpace(true)
        baseActivity.getActionBarView()?.showActionBar(false)
        toolbarSizeListener.resizeContainerSmall()

        if (isFirstInit){
            initClicks()
            listenUpdates()
            getCities()
        }
    }

    private fun initClicks(){
        cross_instance_advert.getClick {
            onBackPressed()
        }

        user_name.getClick {
            fragmentManager.addFragment(FragmentProfileOrder.newInstance())
        }
    }

    private fun getCities(){
        showProgress(true)
        viewModel.getCities()
    }

    private fun listenUpdates() {
        viewModel.citiesData.observe(this) { response ->
            showProgress(false)
            when (response) {
                is Success -> {
                    response.value.response?.let {
                        initViews(it)
                    }
                }
                is Failure -> {
                    showErrorSnack(response.errorMessage)
                }
            }
        }
    }

    private fun initViews(citiesArray: ArrayList<ResponseItem>){
        val data = getAdvertData(arguments)
        user_name.text = data.name
        phone_text_view.text = data.phone
        email_text_view.text = data.email
        date_created_text_view.text = data.dateCreated
        date_ended_text_view.text = data.dateEnded
        price_text_view.text = data.price
        address_text_view.text = data.address
        short_description_text_view.text = data.shortDescription
        full_description_text_view.text = data.fullDescription

        city_text_view.text = citiesArray.filter { it.id == data.cityId }.let {
            if (it.isNotEmpty()) it[0].city
            else ""
        }
    }

}