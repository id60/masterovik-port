package noty_team.com.masterovik.ui.fragments.login.selection_role

import android.arch.lifecycle.ViewModelProvider
import android.view.View
import kotlinx.android.synthetic.main.fragment_selectino_role.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.ui.fragments.login.registration.RegistrationFragment
import noty_team.com.masterovik.utils.*
import noty_team.com.masterovik.utils.enums.UserRole
import noty_team.com.masterovik.utils.paper.PaperIO

class SelectionRoleFragment: BaseFragment<SelectionRoleFragmentViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): SelectionRoleFragmentViewModel {
        return injectViewModel(viewModelFactory)
    }

    companion object {
        fun newInstance(): SelectionRoleFragment {
            return SelectionRoleFragment()
        }
    }

    override fun layout() = R.layout.fragment_selectino_role

    override fun initialization(view: View, isFirstInit: Boolean) {
        baseActivity.getActionBarView()?.showActionBar(true)
        baseActivity.getActionBarView()?.setSmallContainerWidth()
        baseActivity.getActionBarView()?.setupCenterText(getString(R.string.registration))
        baseActivity.getActionBarView()?.showLeftButton(false)

        if (isFirstInit) {
            setupClicks()
            listenUpdates()
        }
    }

    private fun listenUpdates() {
        viewModel.roleData.observe(this) { role ->
            button_continue.enable()
            when (role) {
                UserRole.EXECUTOR -> {
                    showProvideServicesText(true)
                }
                UserRole.EMPLOYER -> {
                    showUseServicesText(true)
                }
                else -> {}
            }
            PaperIO.setRole(role)
        }
    }

    private fun setupClicks() {
        button_continue.disable()

        button_continue.getClick {
            viewModel.roleData.value?.let {
                fragmentManager.addFragment(RegistrationFragment.newInstance(it))
            }
        }

        provide_services.getClick {
            viewModel.setRole(UserRole.EXECUTOR)
        }

        use_services.getClick {
            viewModel.setRole(UserRole.EMPLOYER)
        }
    }

    private fun showProvideServicesText(show: Boolean) {
        if (show) {
            provide_services_text.visible()
            showUseServicesText(!show)
        } else {
            provide_services_text.gone()
        }

    }

    private fun showUseServicesText(show: Boolean) {
        if (show) {
            use_services_text.visible()
            showProvideServicesText(!show)
        } else {
            use_services_text.gone()
        }
    }
}