package noty_team.com.masterovik.ui.fragments.orders.complaint

import android.arch.lifecycle.ViewModelProvider
import android.view.View
import kotlinx.android.synthetic.main.fragment_complaint.*
import noty_team.com.masterovik.R

import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.utils.injectViewModel

class FragmentComplaint : BaseFragment<BaseViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): BaseViewModel {
        return injectViewModel(viewModelFactory)
    }

    companion object {
        fun newInstance() = FragmentComplaint()
    }

    override fun layout() = R.layout.fragment_complaint

    override fun initialization(view: View, isFirstInit: Boolean) {

        cross_complaint.setOnClickListener {
            onBackPressed()
        }
    }
}