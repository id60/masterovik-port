package noty_team.com.masterovik.ui.activities.main

import android.app.Activity
import android.app.AlertDialog
import android.arch.lifecycle.ViewModelProvider
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.widget.DrawerLayout
import android.view.Gravity
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.nav_header_main.view.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.DrawerPressedListener
import noty_team.com.masterovik.base.ToolbarSizeListener
import noty_team.com.masterovik.base.base_action_bar.ActionBarContract
import noty_team.com.masterovik.base.base_action_bar.BaseActionBarPresenter
import noty_team.com.masterovik.base.base_action_bar.BaseActionBarView
import noty_team.com.masterovik.base.BaseActivity
import noty_team.com.masterovik.ui.activities.login.LoginActivity
import noty_team.com.masterovik.ui.fragments.advert.FragmentAdvert
import noty_team.com.masterovik.ui.fragments.faq.FragmentFAQBase
import noty_team.com.masterovik.ui.fragments.orders.OrderFragment
import noty_team.com.masterovik.ui.fragments.profile_setting.FragmentProfileSetting
import noty_team.com.masterovik.ui.fragments.rating.FragmentBaseRating
import noty_team.com.masterovik.ui.fragments.request.FragmentBaseRequest
import noty_team.com.masterovik.ui.fragments.wallet.FragmentBaseWallet
import noty_team.com.masterovik.utils.paper.PaperIO
import android.support.v4.view.GravityCompat
import android.view.LayoutInflater
import noty_team.com.masterovik.utils.*
import noty_team.com.masterovik.utils.enums.UserRole

class MainActivity : BaseActivity<MainActivityViewModel>(),
        NavigationView.OnNavigationItemSelectedListener, DrawerPressedListener, ToolbarSizeListener {

    private lateinit var actionBarView: ActionBarContract.View
    private lateinit var actionBarPresenter: ActionBarContract.Presenter
    private lateinit var userProfileChangesViewModel: UserProfileChangesViewModel

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): MainActivityViewModel {
        return injectViewModel(viewModelFactory)
    }

    companion object {
        private const val DRAWER_SELECTED_ITEM = "DRAWER_SELECTED_ITEM"
        fun start(baseActivity: Activity) {
            baseActivity.startActivity(
                    Intent(baseActivity, MainActivity::class.java)
                            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            )
        }
    }

    override fun layout() = R.layout.activity_main

    override fun initialization() {
        actionBarView = BaseActionBarView(toolbar_order, main_drawer)
        actionBarPresenter = BaseActionBarPresenter(actionBarView)
        userProfileChangesViewModel = injectViewModel(viewModelFactory)

        initDrawer()
        initClicks()
        listenUpdates()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        navigation_view.checkedItem?.itemId?.let {
            outState?.putInt(DRAWER_SELECTED_ITEM, it)
        }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        val selectedDrawerItem = savedInstanceState?.getInt(DRAWER_SELECTED_ITEM, -1)?:-1
        if (selectedDrawerItem >= 0){
            navigation_view.setCheckedItem(selectedDrawerItem)
            openDrawerScreen(selectedDrawerItem)
        }
    }

    private fun initClicks(){
        main_drawer.listenForDrawn {
            navigation_view.add_advert_button.getClick {

            }
        }
    }

    private fun listenUpdates(){
        supportFragmentManager.addOnBackStackChangedListener {
            val count = supportFragmentManager.backStackEntryCount
            if (count == 0) main_drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
            else main_drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        }

        userProfileChangesViewModel.profileData.observe(this) { response ->
            when (response) {
                is Success -> initDrawerProfileData()
                is Failure -> {}
            }
        }
    }

    private fun initDrawer() {
        actionBarView.setDrawerMenu(layoutInflater).getClick {
            onDrawerPressed()
        }
        navigation_view.setNavigationItemSelectedListener(this)
        navigation_view.setCheckedItem(R.id.nav_order)

        fragmentManager.replaceFragment(OrderFragment.newInstance())

        initDrawerProfileData()
        main_drawer.listenForDrawn {
            if (PaperIO.getRole() == UserRole.EMPLOYER){
                navigation_view.add_advert_button.visible()
                navigation_view.menu.findItem(R.id.nav_request).isVisible = false
                navigation_view.menu.findItem(R.id.nav_rating).isVisible = false
                navigation_view.menu.findItem(R.id.nav_packet).isVisible = false
                navigation_view.menu.findItem(R.id.nav_executors).isVisible = true
            }
            else {
                navigation_view.add_advert_button.gone()
                navigation_view.menu.findItem(R.id.nav_executors).isVisible = false
            }
        }
    }

    private fun initDrawerProfileData(){
        main_drawer.listenForDrawn {
            PaperIO.getUserProfile().apply {
                navigation_view.drawer_user_name.text = getString(R.string.drawer_profile_user_name,
                        "${this.firstName}")
                navigation_view.wallet_cache_text_view.text = getString(R.string.wallet_cash, this.balance
                        ?: "0.0")
            }
        }
    }

    override fun onDrawerPressed() {
        main_drawer.openDrawer(Gravity.START)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        openDrawerScreen(item.itemId)
        main_drawer.closeDrawer(Gravity.START)
        return true
    }

    private fun openDrawerScreen(menuItemId: Int){
        if (navigation_view.checkedItem?.itemId != menuItemId) {
            when (menuItemId) {
                R.id.nav_order -> {
                    fragmentManager.replaceFragment(OrderFragment.newInstance())
                }
                R.id.nav_advert -> {
                    fragmentManager.replaceFragment(FragmentAdvert.newInstance())
                }
                R.id.nav_request -> {
                    fragmentManager.replaceFragment(FragmentBaseRequest.newInstance())
                }
                R.id.nav_rating -> {
                    fragmentManager.replaceFragment(FragmentBaseRating.newInstance())
                }
                R.id.nav_packet -> {
                    fragmentManager.replaceFragment(FragmentBaseWallet.newInstance())
                }
                R.id.nav_executors -> {

                }
                R.id.nav_profile -> {
                    fragmentManager.replaceFragment(FragmentProfileSetting.newInstance())
                }
                R.id.item_order_services -> {

                }
                R.id.item_faq -> {
                    fragmentManager.replaceFragment(FragmentFAQBase.newInstance())
                }
                R.id.item_exit -> {
                    showLogoutDialog()
                }
            }
        }
    }


    private fun showLogoutDialog() {
        AlertDialog.Builder(this)
                .setMessage("Вы действительно хотите выйти из аккаунта?")
                .setPositiveButton("Да"){ _, _ ->
                    PaperIO.clear()
                    LoginActivity.start(this)
                }
                .setNegativeButton("Отмена"){ _, _ -> }
                .setCancelable(true)
                .show()
    }


    override fun getActionBarView(): ActionBarContract.View? = actionBarView

    override fun resizeContainerLarge() {
//        getActionBarView()?.showActionBar(true)
//        val layoutParams = main_fragment_container.layoutParams as CoordinatorLayout.LayoutParams
//        layoutParams.setMargins(0, convertDpToPixel(56f, baseContext).toInt(), 0, 0)
//        main_fragment_container.layoutParams = layoutParams
    }

    override fun resizeContainerSmall() {
//        getActionBarView()?.showActionBar(false)
//        val layoutParams = main_fragment_container.layoutParams as CoordinatorLayout.LayoutParams
//        layoutParams.setMargins(0, 0, 0, 0)
//        main_fragment_container.layoutParams = layoutParams
    }

    override fun hideEmptyToolbarSpace(enable: Boolean) {
//        val params = parent_coordinator_layout.layoutParams as RelativeLayout.LayoutParams
//        params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT)
//        params.addRule(RelativeLayout.LEFT_OF, noty_team.com.masterovik.R.id.id_to_be_left_of)

        if (enable) empty_toolbar_space.gone() else empty_toolbar_space.visible()
    }

    override fun enableSnackBarBehaviour(enable: Boolean) {
        main_fragment_container.enableSnackBarBehaviour(enable)
    }

    override fun onBackPressed() {
        if (main_drawer.isDrawerOpen(GravityCompat.START)) {
            main_drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }
}