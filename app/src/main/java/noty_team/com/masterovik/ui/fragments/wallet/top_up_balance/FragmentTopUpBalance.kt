package noty_team.com.masterovik.ui.fragments.wallet.top_up_balance

import android.arch.lifecycle.ViewModelProvider
import android.support.v7.widget.LinearLayoutManager
import android.text.TextUtils
import android.view.View
import kotlinx.android.synthetic.main.fragment_top_up_balance.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.api.request.wallet.add_money.AddMoneyRequest

import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.ui.activities.main.UserProfileChangesViewModel
import noty_team.com.masterovik.ui.adapters.items_adapter.MyCardsItem
import noty_team.com.masterovik.ui.adapters.recycler.CardsListSelectingAdapter
import noty_team.com.masterovik.ui.fragments.wallet.add_card.AddCardFragment
import noty_team.com.masterovik.ui.fragments.wallet.cards_list.CardsListFragmentViewModel
import noty_team.com.masterovik.utils.*

class FragmentTopUpBalance : BaseFragment<TopUpBalanceFragmentViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): TopUpBalanceFragmentViewModel {
        return injectViewModel(viewModelFactory)
    }

    companion object {
        fun newInstance(): FragmentTopUpBalance {
            return FragmentTopUpBalance()
        }
    }

    override fun layout() = R.layout.fragment_top_up_balance

    private lateinit var cardsAdapter: CardsListSelectingAdapter
    private lateinit var cardsListFragmentViewModel: CardsListFragmentViewModel

    override fun initialization(view: View, isFirstInit: Boolean) {
        initToolbar()

        if (isFirstInit){
            cardsListFragmentViewModel = baseActivity.injectViewModel(viewModelFactory)

            initAdapter()
            initClicks()
            listenUpdates()

            getMyCards()
        }
    }

    private fun getMyCards(){
        showProgress(true)
        cardsListFragmentViewModel.getMyCards()
    }

    private fun addMoney(){
        cardsAdapter.getCheckedItem()?.let { card ->
            showProgress(true)
            val sum = card_sum_edit_text.text.toString().trim()
            viewModel.addMoney(AddMoneyRequest(sum.toInt(), card.id))
        }
    }

    private fun initClicks(){
        add_card_button.getClick {
            fragmentManager.addFragment(AddCardFragment())
        }

        button_save.getClick {
            if (validateCardSum() && cardsAdapter.getCheckedItem() != null)
                addMoney()
        }
    }

    private fun listenUpdates() {
        cardsListFragmentViewModel.myCardsData.observe(this) { response ->
            showProgress(false)
            when (response) {
                is Success -> {
                    cardsAdapter.clear()
                    cardsAdapter.addCards(response.value.map().apply {
                        if (this.isNotEmpty())
                            this[0].isChecked = true
                    })
                }
                is Failure -> {
                    showErrorSnack(response.errorMessage)
                }
            }
            checkEmptyList()
        }

        viewModel.addMoneyData.observe(this) { response ->
            showProgress(false)
            when (response) {
                is Success -> {
                    response.value.response?.let { showSuccessSnack(it) }
                    sendBackResult(response.value)
                    updateProfile()
                    onBackPressed()
                }
                is Failure -> {
                    showErrorSnack(response.errorMessage)
                }
            }
        }

        subscribeForResult {
            if (it.classProvider == AddCardFragment::class) {
                sendBackResult(it.value)
                getMyCards()
            }
        }
    }

    private fun updateProfile(){
        (baseActivity.injectViewModel(viewModelFactory) as UserProfileChangesViewModel)
                .getProfile()
    }

    private fun checkEmptyList(){
        if (cardsAdapter.itemCount > 0) empty_list_text_view.gone()
        else empty_list_text_view.visible()
    }

    private fun initAdapter(){
        my_cards_list.layoutManager = LinearLayoutManager(baseContext)
        cardsAdapter = CardsListSelectingAdapter(arrayListOf()){

        }
        my_cards_list.adapter = cardsAdapter
    }

    private fun initToolbar(){
        toolbarSizeListener.hideEmptyToolbarSpace(true)
        baseActivity.getActionBarView()?.showActionBar(false)

        cross_top_up_balance.getClick {
            onBackPressed()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        baseActivity.getActionBarView()?.setLargeToolbarHeight()
    }

    private fun validateCardSum(): Boolean {
        card_sum_edit_text.setError(false)
        val sum = card_sum_edit_text.text.toString().trim()
        return card_sum_edit_text.validate(
                sum.isNotEmpty() && sum != "0" && TextUtils.isDigitsOnly(sum),
                viewForShake = card_sum_edit_text
        )
    }
}