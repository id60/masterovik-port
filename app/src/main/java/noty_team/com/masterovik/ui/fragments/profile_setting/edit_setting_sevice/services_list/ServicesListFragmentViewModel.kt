package noty_team.com.masterovik.ui.fragments.profile_setting.edit_setting_sevice.services_list

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import noty_team.com.masterovik.api.request.change_phone.ChangePhoneRequest
import noty_team.com.masterovik.api.request.services.AddServiceRequest
import noty_team.com.masterovik.api.response.BaseResponse
import noty_team.com.masterovik.api.response.change_phone.ChangePhoneResponse
import noty_team.com.masterovik.api.response.city.cities_list.CitiesResponse
import noty_team.com.masterovik.api.response.services.get_service.GetServiceResponse
import noty_team.com.masterovik.api.response.services.get_services.GetServicesResponse
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.ui.fragments.login.entrance.CountDownTimerLiveData
import noty_team.com.masterovik.utils.Result
import javax.inject.Inject

class ServicesListFragmentViewModel @Inject constructor(app: Application) : BaseViewModel(app) {

    val getServiceData = MutableLiveData<Result<GetServicesResponse>>()
    val addServiceData = MutableLiveData<Result<BaseResponse>>()

    fun getServices(){
        api.servicesApi.getServices() {
            getServiceData.postValue(it)
        }.call()
    }

    fun addService(data: AddServiceRequest){
        api.profileApi.addService(data) {
            addServiceData.postValue(it)
        }.call()
    }

    override fun onClear() {
        getServiceData.value = null
        addServiceData.value = null
    }
}