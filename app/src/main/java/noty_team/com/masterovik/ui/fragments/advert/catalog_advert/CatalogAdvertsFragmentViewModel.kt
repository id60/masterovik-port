package noty_team.com.masterovik.ui.fragments.advert.catalog_advert

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import noty_team.com.masterovik.api.request.advert.get_catalog_adverts.CatalogAdvertsRequest
import noty_team.com.masterovik.api.request.services.AddServiceRequest
import noty_team.com.masterovik.api.response.BaseResponse
import noty_team.com.masterovik.api.response.adverts.my_adverts.MyAdvertsResponse
import noty_team.com.masterovik.api.response.services.get_services.GetServicesResponse
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.utils.Failure
import noty_team.com.masterovik.utils.Result
import noty_team.com.masterovik.utils.Success
import javax.inject.Inject

class CatalogAdvertsFragmentViewModel @Inject constructor(app: Application) : BaseViewModel(app) {

    val catalogAdvertsData = MutableLiveData<Result<MyAdvertsResponse>>()

    fun getCatalogAdverts(request: CatalogAdvertsRequest){
        api.advertsApi.getCatalogAdverts(request) {
            catalogAdvertsData.postValue(it)
        }.call()
    }

    override fun onClear() {
        catalogAdvertsData.value = null
    }
}