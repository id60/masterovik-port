package noty_team.com.masterovik.ui.fragments.orders.done_order

import android.arch.lifecycle.ViewModelProvider
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.fragment_done.*
import noty_team.com.masterovik.R

import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.ui.adapters.items_adapter.DoneOrderItem
import noty_team.com.masterovik.ui.adapters.recycler.DoneOrderAdapter
import noty_team.com.masterovik.ui.fragments.orders.done_order.order.FragmentDoneItem
import noty_team.com.masterovik.utils.injectViewModel

class DoneFragment : BaseFragment<BaseViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): BaseViewModel {
        return injectViewModel(viewModelFactory)
    }

    companion object {
        fun newInstance(): DoneFragment {
            return DoneFragment()
        }
    }

    override fun layout() = R.layout.fragment_done

    override fun initialization(view: View, isFirstInit: Boolean) {
        recycler_done_order.layoutManager = LinearLayoutManager(baseActivity)
        recycler_done_order.adapter = DoneOrderAdapter(createList()){
            fragmentManager.addFragment(FragmentDoneItem.newInstance())
        }
    }

    private fun createList(): ArrayList<DoneOrderItem> {
        val list: ArrayList<DoneOrderItem> = ArrayList()
        val item1 = DoneOrderItem("Название Заказа", "Имя Фамилия", 15)
        val item2 = DoneOrderItem("Название Заказа", "Имя Фамилия", 22)
        val item3 = DoneOrderItem("Название Заказа", "Имя Фамилия", 18)
        val item4 = DoneOrderItem("Название Заказа", "Имя Фамилия", 2)

        list.add(item1)
        list.add(item2)
        list.add(item3)
        list.add(item4)

        return list
    }

}