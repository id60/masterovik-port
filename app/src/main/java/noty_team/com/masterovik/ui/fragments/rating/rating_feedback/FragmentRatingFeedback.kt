package noty_team.com.masterovik.ui.fragments.rating.rating_feedback

import android.arch.lifecycle.ViewModelProvider
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.fragment_rating_feedbeack.*
import noty_team.com.masterovik.R

import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.ui.adapters.items_adapter.FeedbackRatingItem
import noty_team.com.masterovik.ui.adapters.recycler.FeedbackRatingAdapter
import noty_team.com.masterovik.ui.fragments.rating.rating_review_detail.FragmentReviewDetail
import noty_team.com.masterovik.utils.injectViewModel

class FragmentRatingFeedback : BaseFragment<BaseViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): BaseViewModel {
        return injectViewModel(viewModelFactory)
    }
    override fun layout() = R.layout.fragment_rating_feedbeack

    override fun initialization(view: View, isFirstInit: Boolean) {

        recycler_rating_feedback.layoutManager = LinearLayoutManager(baseActivity)
        recycler_rating_feedback.adapter = FeedbackRatingAdapter(createList()){
            baseActivity.fragmentManager.addFragment(FragmentReviewDetail.newInstance())
            toolbarSizeListener.resizeContainerSmall()
        }
    }

    private fun createList(): ArrayList<FeedbackRatingItem> {
        var list: ArrayList<FeedbackRatingItem> = ArrayList()

        var item = FeedbackRatingItem("Имя заказчика", "20 мая 2018", 4, getString(R.string.feedback_setting))

        list.add(item)
        list.add(item)
        list.add(item)
        list.add(item)

        return list
    }
}