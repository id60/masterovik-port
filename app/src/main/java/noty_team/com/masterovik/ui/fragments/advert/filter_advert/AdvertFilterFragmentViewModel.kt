package noty_team.com.masterovik.ui.fragments.advert.filter_advert

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import noty_team.com.masterovik.api.request.advert.get_catalog_adverts.CatalogAdvertsRequest
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import javax.inject.Inject

class AdvertFilterFragmentViewModel @Inject constructor(app: Application) : BaseViewModel(app) {

    val advertSortingData = MutableLiveData<CatalogAdvertsRequest>()

    fun setSortingData(data: CatalogAdvertsRequest?){
        advertSortingData.postValue(data)
    }

    fun getSortingData() = advertSortingData.value?:CatalogAdvertsRequest()

    override fun onClear() {
//        advertSortingData.value = null
    }
}