package noty_team.com.masterovik.ui.adapters.items_adapter

data class HistoryPaymentItem(var type: OperationType,
                              var date: String,
                              var balance: Int,
                              var amount: Int,
                              var cardNumber: String = "",
                              var id: Int){
    enum class OperationType(val typeString: String){
        REFILL("Пополнение баланса"),
        WITHDRAWAL("Списание с внутреннего баланса")
    }
}