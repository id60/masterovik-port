package noty_team.com.masterovik.ui.adapters.recycler

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_feedback.view.*
import noty_team.com.masterovik.R

import noty_team.com.masterovik.ui.adapters.items_adapter.FeedbackRatingItem
import noty_team.com.masterovik.ui.fragments.rating.rating_review_detail.FragmentReviewDetail


class FeedbackRatingAdapter(val items: ArrayList<FeedbackRatingItem>,
                            val onClick:()->Unit) :
        RecyclerView.Adapter<FeedbackRatingAdapter.ViewHolder>(), View.OnClickListener {


    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_feedback, parent, false))
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder?.nameCustomerFeedback?.text = items.get(position).namecustomer
        holder?.dateFeedback?.text = items.get(position).date
        holder?.ratingFeedback?.text = items.get(position).rating.toString()
        holder?.descriptionFeedback?.text = items.get(position).descriptor

        holder?.feedbackItem.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.feedback_item -> {
//                baseActivity.navigator.navigateToFragment(FragmentReviewDetail.newInstance(), null, true)
                onClick()

                /*  baseActivity.navigator.navigateToFragment(FragmentOrderItem.newInstance(), null, true)

                  baseActivity.getActionBarView()?.showActionBar(false)
                  var lpconteriner = baseActivity.findViewById<View>(R.id.main_fragment_container).layoutParams as ConstraintLayout.LayoutParams
                  lpconteriner.setMargins(0, 0, 0, 0)
                  baseActivity.findViewById<View>(R.id.main_fragment_container).layoutParams = lpconteriner*/
            }
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var nameCustomerFeedback = view.name_customer_feedback
        var dateFeedback = view.date_feedback
        var ratingFeedback = view.rating_feedback
        var descriptionFeedback = view.description_feedback

        var feedbackItem = view.feedback_item

    }
}