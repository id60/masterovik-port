package noty_team.com.masterovik.ui.fragments.orders.profile_order

import android.arch.lifecycle.ViewModelProvider
import android.view.View
import noty_team.com.masterovik.R

import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.utils.injectViewModel

class FragmentProfileOrder : BaseFragment<BaseViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): BaseViewModel {
        return injectViewModel(viewModelFactory)
    }

    companion object {
        fun newInstance() = FragmentProfileOrder()
    }

    override fun layout() = R.layout.fragment_profile_order

    override fun initialization(view: View, isFirstInit: Boolean) {

        baseActivity.findViewById<View>(R.id.cross_profile).setOnClickListener {
            onBackPressed()
        }

    }
}