package noty_team.com.masterovik.ui.fragments.login.registration

import android.arch.lifecycle.ViewModelProvider
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.PopupMenu
import android.view.*
import kotlinx.android.synthetic.main.button_continue.view.*
import kotlinx.android.synthetic.main.fragment_registration_form.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.api.request.auth.register.RegistrationRequest
import noty_team.com.masterovik.api.response.city.cities_list.ResponseItem
import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.ui.activities.login.LoginActivityViewModel
import noty_team.com.masterovik.ui.fragments.login.terms_agreement.TermsFragment
import noty_team.com.masterovik.utils.*
import noty_team.com.masterovik.utils.enums.UserRole

class RegistrationFragment : BaseFragment<RegistrationFragmentViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): RegistrationFragmentViewModel {
        return injectViewModel(viewModelFactory)
    }

    companion object {
        private const val USER_ROLE_KEY = "USER_ROLE_KEY"
        fun newInstance(role: UserRole): RegistrationFragment {
            return RegistrationFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(USER_ROLE_KEY, role)
                }
            }
        }
        fun getUserRole(arguments: Bundle?) = arguments?.getSerializable(USER_ROLE_KEY) as UserRole
    }

    private lateinit var loginActivityViewModel: LoginActivityViewModel
    var citiesList = ArrayList<ResponseItem>()
    var cityId = -1

    override fun layout() = R.layout.fragment_registration_form

    override fun initialization(view: View, isFirstInit: Boolean) {
        baseActivity.getActionBarView()?.showActionBar(true)
        baseActivity.getActionBarView()?.setupLeftButton(layoutInflater.inflate(R.layout.ab_back, null))
        baseActivity.getActionBarView()?.setSmallContainerWidth()
        baseActivity.getActionBarView()?.showLeftButton(true)
        baseActivity.getActionBarView()?.getLeftButton()?.getClick {
            onBackPressed()
        }

        if (isFirstInit) {
            loginActivityViewModel = baseActivity.injectViewModel(viewModelFactory)

            name_title_text_view.text = getString(R.string.name).fromHtml()
            last_name_title_text_view.text = getString(R.string.surname).fromHtml()
            city_title_text_view.text = getString(R.string.city).fromHtml()

            setupClicks()
            setUpWithRole()
            listenUpdates()

            getCities()
        }
    }

    private fun getCities(){
        showProgress(true)
        viewModel.getCities()
    }

    private fun listenUpdates(){
        viewModel.getCityData.observe(this) { response ->
            showProgress(false)
            when(response){
                is Success -> {
                    response.value.response?.apply {
                        citiesList = this
                    }
                }
                is Failure -> {
                    showErrorSnack(response.errorMessage)
                }
            }
        }
    }

    private fun setupClicks() {
        city_edit_text.getClick {
            showCitiesPopUp(it)
        }

        invite_code_button.getClick {
            showInviteDialog()
        }

        button_continue.getClick {
            if (validate()) {
                val userRole = getUserRole(arguments)
                val request = RegistrationRequest(
                        firstName = name_edit_text.text.toString().trim(),
                        lastName = last_name_edit_text.text.toString().trim(),
                        cityId = cityId,
                        roleId = userRole.id
                )
                email_edit_text.text.trim().apply {
                    if (isNotEmpty()) request.email = toString()
                }
                if (userRole == UserRole.EXECUTOR) {
                    company_name_edit_text.text.trim().apply {
                        if (isNotEmpty()) request.companyName = toString()
                    }
                    description_edit_text.text.trim().apply {
                        if (isNotEmpty()) request.companyDescription = toString()
                    }
                }
                loginActivityViewModel.registrationRequestData.value = request
                fragmentManager.addFragment(TermsFragment.newInstance())
            }
        }
    }

    private fun setUpWithRole(){
        when (getUserRole(arguments)){
            UserRole.EMPLOYER -> {
                showGiveService(false)
            }
            UserRole.EXECUTOR -> {
                showGiveService(true)
            }
        }
    }

    private fun showGiveService(show: Boolean) {
        if (show) {
            ll_company_info.visibility = View.VISIBLE
            invite_code.visibility = View.VISIBLE
        } else {
            ll_company_info.visibility = View.GONE
            invite_code.visibility = View.GONE
        }
    }

    private fun showSuccessInfo(show: Boolean) {
        if (show) {
            success_info.visibility = View.VISIBLE
        } else {
            success_info.visibility = View.GONE
        }
    }

    private fun showInviteDialog() {
        val mDialogView = LayoutInflater.from(baseActivity).inflate(R.layout.dialog_user_invite, null)
        val mBuilder = AlertDialog.Builder(baseActivity).setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mDialogView.continue_btn.setOnClickListener {
            mAlertDialog.dismiss()
            showSuccessInfo(true)
        }
    }

    private fun validate(): Boolean {
        name_edit_text.setError(false)
        last_name_edit_text.setError(false)
        email_edit_text.setError(false)
        city_edit_text.setError(false)
//        company_name_edit_text.setError(false)
//        description_edit_text.setError(false)

        val name = name_edit_text.text.toString().trim()
        if (!name_edit_text.validate(
                name.isNotEmpty(),
                viewForShake = name_edit_text,
                viewForScrollingTo = name_title_text_view,
                container = fragment_registration)
        ) return false

        val lastName = last_name_edit_text.text.toString().trim()
        if (!last_name_edit_text.validate(
                        lastName.isNotEmpty(),
                        viewForShake = last_name_edit_text,
                        viewForScrollingTo = last_name_title_text_view,
                        container = fragment_registration)
        ) return false

        val email = email_edit_text.text.toString().trim()
        if (email.isNotEmpty() && !email_edit_text.validate(
                        validator.isEmailValid(email),
                        viewForShake = email_edit_text,
                        viewForScrollingTo = email_title_text_view,
                        container = fragment_registration)
        ) return false

        val city = city_edit_text.text.toString().trim()
        if (!city_edit_text.validate(
                        city.isNotEmpty(),
                        viewForShake = city_layout,
                        viewForScrollingTo = city_title_text_view,
                        container = fragment_registration)
        ) return false

//        val companyName = company_name_edit_text.text.toString().trim()
//        if (companyName.isNotEmpty() && !company_name_edit_text.validate(
//                        companyName.isNotEmpty(),
//                        viewForShake = company_name_edit_text,
//                        viewForScrollingTo = company_name_text_view,
//                        container = fragment_registration)
//        ) return false
//
//        val description = description_edit_text.text.toString().trim()
//        if (description.isNotEmpty() && !description_edit_text.validate(
//                        description.isNotEmpty(),
//                        viewForShake = description_edit_text,
//                        viewForScrollingTo = description_text_view,
//                        container = fragment_registration)
//        ) return false

        if (!check_licence.isChecked) {
            showErrorSnack("Примите лицензионное соглашение")
            return false
        }

        return true
    }

    private fun showCitiesPopUp(v: View){
        val popup = PopupMenu(baseActivity, v, Gravity.END)
        citiesList.forEachIndexed { index, item ->
            popup.menu.add(0, item.id, 0, item.city)
        }
        popup.setOnMenuItemClickListener {
            cityId = it.itemId

            val selectedCities = citiesList.filter { it.id == cityId}
            if (selectedCities.isNotEmpty()){
                city_edit_text.setText(selectedCities[0].city)
            }

            true
        }
        popup.show()
    }
}