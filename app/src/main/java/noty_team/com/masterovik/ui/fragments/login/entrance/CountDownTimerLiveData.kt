package noty_team.com.masterovik.ui.fragments.login.entrance

import android.arch.lifecycle.LiveData
import android.os.CountDownTimer

data class TimerTickModel(val isFinished: Boolean, val time: Long)

class CountDownTimerLiveData(val totalTime: Long, val interval: Long): LiveData<TimerTickModel>() {

    private var isFinished = true

    private var timer = getTimer(totalTime, interval)

    override fun onInactive() {
        timer.cancel()
        isFinished = false
    }

    override fun onActive() {
        if (!isFinished) {
            timer = getTimer(value?.time ?: 0L, interval)
            timer.start()
        }
    }

    fun start(){
        if (isFinished) {
            isFinished = false
            timer.start()
        }
    }

    fun stop(){
        timer.cancel()
        isFinished = true
        value = null
    }

    private fun getTimer(totalTime: Long, interval: Long): CountDownTimer =
            object : CountDownTimer(totalTime, interval) {
                var ticksCount = totalTime
                override fun onFinish() {
                    isFinished = true
                    ticksCount = totalTime
                    postValue(TimerTickModel(true, totalTime))
                }

                override fun onTick(millisUntilFinished: Long) {
                    postValue(TimerTickModel(false, ticksCount))
                    ticksCount-=interval
                }
            }

}