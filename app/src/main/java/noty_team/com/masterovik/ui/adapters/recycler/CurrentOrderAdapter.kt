package noty_team.com.masterovik.ui.adapters.recycler

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_current_order.view.*
import noty_team.com.masterovik.R

import noty_team.com.masterovik.ui.adapters.items_adapter.CurrentOrderItem
import noty_team.com.masterovik.ui.fragments.orders.current_order.order.FragmentOrderItem

class CurrentOrderAdapter(val items: ArrayList<CurrentOrderItem>,
                          val onClick:()->Unit) :
        RecyclerView.Adapter<CurrentOrderAdapter.ViewHolder>(), View.OnClickListener {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_current_order, parent, false))
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder?.nameCurrentOrder?.text = items.get(position).nameOrder
        holder?.nameSurnameCurrenrOrder?.text = items.get(position).nameSurname
        holder?.currentOrderDay?.text = items.get(position).day.toString() + " дней"

        holder?.itemCanceledOrderBtn.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.item_order_btn -> {
//                baseActivity.navigator.navigateToFragment(FragmentOrderItem.newInstance(), null, true)
                onClick()

            }
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var nameCurrentOrder = view.name_current_order
        var nameSurnameCurrenrOrder = view.name_surname_current_order
        var currentOrderDay = view.current_order_day

        var itemCanceledOrderBtn = view.item_order_btn

    }
}