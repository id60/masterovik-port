package noty_team.com.masterovik.ui.fragments.request.in_work.request_in_detail.change_deadlines

import android.arch.lifecycle.ViewModelProvider
import android.view.View
import noty_team.com.masterovik.R

import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.utils.injectViewModel

class FragmentChangeDeadlines : BaseFragment<BaseViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): BaseViewModel {
        return injectViewModel(viewModelFactory)
    }

    companion object {
        fun newInstance() = FragmentChangeDeadlines()
    }

    override fun layout() = R.layout.fragment_change_deadline

    override fun initialization(view: View, isFirstInit: Boolean) {


        baseActivity.findViewById<View>(R.id.cross_change_deadline).setOnClickListener {
            onBackPressed()
        }

    }
}