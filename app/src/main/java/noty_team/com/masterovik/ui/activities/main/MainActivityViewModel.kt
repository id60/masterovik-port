package noty_team.com.masterovik.ui.activities.main

import android.app.Application
import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.ui.adapters.items_adapter.ProvideServiceItem
import javax.inject.Inject
import kotlin.reflect.KClass

class MainActivityViewModel @Inject constructor(app: Application) : BaseViewModel(app) {

    override fun onClear() {

    }
}