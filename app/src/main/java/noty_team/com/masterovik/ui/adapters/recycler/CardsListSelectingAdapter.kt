package noty_team.com.masterovik.ui.adapters.recycler

import android.view.View
import android.widget.CompoundButton
import kotlinx.android.synthetic.main.selectable_cards_item.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.ui.adapters.items_adapter.MyCardsItem
import noty_team.com.masterovik.utils.CreditCardUtils
import noty_team.com.masterovik.utils.loadImage
import java.lang.Exception

class CardsListSelectingAdapter (list: ArrayList<MyCardsItem>,
                                 val onClick:(item: MyCardsItem)->Unit) :
        BaseAdapter<MyCardsItem, CardsListSelectingAdapter.ViewHolder>(list) {

    override fun createViewHolder(view: View, layoutId: Int) = ViewHolder(view)

    override fun getItemViewType(position: Int) = R.layout.selectable_cards_item

    var lastCheckedIndex = -1

    init {
        initFirstInitialize()
    }

    private fun initFirstInitialize(){
        list.forEachIndexed { index, item ->
            if (item.isChecked) {
                lastCheckedIndex = index
                return@forEachIndexed
            }
        }
    }

    fun getCheckedItem(): MyCardsItem? {
        return list.find { it.isChecked }
    }

    fun addCards(list: ArrayList<MyCardsItem>){
        addAll(list)
        initFirstInitialize()
    }

    private fun getCheckedChangeListener(adapterPosition: Int): CompoundButton.OnCheckedChangeListener {
        return CompoundButton.OnCheckedChangeListener { _, _ ->
            list[adapterPosition].isChecked = true
            if (lastCheckedIndex >= 0) {
                list[lastCheckedIndex].isChecked = false
                try {
                    notifyItemChanged(lastCheckedIndex)
                } catch (e: IllegalStateException) {
                }
            }
            lastCheckedIndex = adapterPosition
        }
    }

    inner class ViewHolder(view: View) : BaseAdapter.BaseViewHolder(view) {

        init {
            itemView.getClick {
                try {
                    if (adapterPosition != lastCheckedIndex){
                        card_radio_button.isChecked = !card_radio_button.isChecked
                        notifyItemChanged(adapterPosition)
                    }
                    onClick(list[adapterPosition])
                }catch (e: Exception){}

            }
        }

        override fun bind(pos: Int) {
            card_radio_button.setOnCheckedChangeListener(null)
            card_radio_button.isChecked = list[pos].isChecked
            card_radio_button.setOnCheckedChangeListener(getCheckedChangeListener(pos))

            card_number_text_view.text = list[pos].cardNumber
            setCardImage(list[pos].cardNumber)
        }

        private fun setCardImage(number: String){
            val cardImageLink = CreditCardUtils.getCardImageLink(number)
            card_type_image_view.loadImage(cardImageLink)
        }
    }

}