package noty_team.com.masterovik.ui.adapters.recycler

import android.view.View
import kotlinx.android.synthetic.main.cities_list_item.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.ui.adapters.items_adapter.CityItem
import noty_team.com.masterovik.utils.gone
import noty_team.com.masterovik.utils.visible
import java.lang.Exception

class CitiesListAdapter(list: ArrayList<CityItem>,
                         val onClick:(item: CityItem)->Unit) :
        BaseAdapter<CityItem, CitiesListAdapter.ViewHolder>(list) {

    override fun createViewHolder(view: View, layoutId: Int) = ViewHolder(view)

    override fun getItemViewType(position: Int) = R.layout.cities_list_item

    inner class ViewHolder(view: View) : BaseAdapter.BaseViewHolder(view) {

        init {
            try {
                itemView.getClick {
                    list.forEach { it.isChecked = false }
                    list[adapterPosition].isChecked = true
                    onClick(list[adapterPosition])
                }
            }catch(e: Exception){}
        }

        override fun bind(pos: Int) {
            city_name_text_view.text = list[pos].cityName
            if (list[pos].isChecked) {
                city_check_box.visible()
                city_check_box.isChecked = true
            }
            else {
                city_check_box.gone()
                city_check_box.isChecked = false
            }

        }
    }

}