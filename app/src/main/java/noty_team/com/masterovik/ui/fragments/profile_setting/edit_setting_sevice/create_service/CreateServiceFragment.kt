package noty_team.com.masterovik.ui.fragments.profile_setting.edit_setting_sevice.create_service

import android.arch.lifecycle.ViewModelProvider
import android.view.View
import kotlinx.android.synthetic.main.fragment_create_service.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.api.request.create_service.CreateServiceRequest
import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.utils.*

class CreateServiceFragment : BaseFragment<CreateServiceFragmentViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): CreateServiceFragmentViewModel {
        return injectViewModel(viewModelFactory)
    }

    companion object {

        fun newInstance() = CreateServiceFragment()
    }
    override fun layout() = R.layout.fragment_create_service

    override fun initialization(view: View, isFirstInit: Boolean) {
        initToolbar()
        if (isFirstInit){
            initClicks()
            listenUpdates()
        }
    }

    private fun initClicks() {
        save_button.getClick {
            if (validate()){
                createService()
            }
        }
    }

    private fun createService(){
        showProgress(true)
        viewModel.createService(getCreateServiceRequest())
    }

    private fun listenUpdates() {
        viewModel.createServiceData.observe(this) { response ->
            showProgress(false)
            when (response) {
                is Success -> {
                    showSuccessSnack("Услуга успешно создана")
                    onBackPressed()
                }
                is Failure -> {
                    showErrorSnack(response.errorMessage)
                }
            }
        }
    }

    private fun initToolbar(){
        baseActivity.getActionBarView()?.setupCenterText("Создать услугу")
    }

    private fun getCreateServiceRequest(): CreateServiceRequest {
        val name = service_name_edit_text.text.toString().trim()
        val description = description_edit_text.text.toString().trim()
        val reasons = reasons_edit_text.text.toString().trim()
        return CreateServiceRequest(
                reason = reasons,
                name = name,
                description = description
        )
    }

    private fun validate(): Boolean {
        service_name_edit_text.setError(false)
        description_edit_text.setError(false)
        reasons_edit_text.setError(false)

        val name = service_name_edit_text.text.toString().trim()
        if (!service_name_edit_text.validate(
                        name.isNotEmpty(),
                        viewForShake = service_name_edit_text)
        ) return false

        val description = description_edit_text.text.toString().trim()
        if (!description_edit_text.validate(
                        description.isNotEmpty(),
                        viewForShake = description_edit_text)
        ) return false

        val reasons = reasons_edit_text.text.toString().trim()
        if (!reasons_edit_text.validate(
                        reasons.isNotEmpty(),
                        viewForShake = reasons_edit_text)
        ) return false

        return true
    }
}