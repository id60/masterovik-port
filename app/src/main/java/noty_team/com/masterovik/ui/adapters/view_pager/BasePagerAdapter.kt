package noty_team.com.masterovik.ui.adapters.view_pager


import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.view.ViewGroup

import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.base.ViewPagerListener
import noty_team.com.masterovik.utils.adapters.CustomViewPager

abstract class BasePagerAdapter(fm: FragmentManager,
                                val pager: CustomViewPager,
                                val fragments: ArrayList<BaseFragment<*>>) : FragmentPagerAdapter(fm) {
    val context: Context = pager.context

    private fun getVisibleCallback(fragment: Fragment): (fragment: Fragment) -> Boolean {
        return {
            val fragmentIndex = fragments.indexOf(fragment)
            fragmentIndex == pager.currentItem
        }
    }

    init {
        fragments.forEachIndexed { index, item ->
            item.isVisible = getVisibleCallback(item)
            item.viewPagerListener = object: ViewPagerListener{
                override fun getPageIndex() = pager.currentItem
                override fun isCurrentPageSelected() = index == pager.currentItem
            }
        }
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val fragment = super.instantiateItem(container, position) as BaseFragment<*>
        fragments[position] = fragment
        return fragment
    }

    override fun getItem(position: Int) = fragments[position]

    override fun getCount() = fragments.size

    override fun getPageTitle(position: Int) = getTitle(position)

    abstract fun getTitle(pageIndex: Int): String
}