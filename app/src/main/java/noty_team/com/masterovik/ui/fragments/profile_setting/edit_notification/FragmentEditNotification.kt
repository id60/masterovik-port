package noty_team.com.masterovik.ui.fragments.profile_setting.edit_notification

import android.arch.lifecycle.ViewModelProvider
import android.view.View
import noty_team.com.masterovik.R

import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.utils.injectViewModel

class FragmentEditNotification: BaseFragment<BaseViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): BaseViewModel {
        return injectViewModel(viewModelFactory)
    }
    companion object {
        fun newInstance() = FragmentEditNotification()
    }

    override fun layout() = R.layout.fragment_edit_notification

    override fun initialization(view: View, isFirstInit: Boolean) {
        initToolbar()
    }

    private fun initToolbar(){
        baseActivity.getActionBarView()?.setupCenterText("Уведомления")
        baseActivity.getActionBarView()?.showLeftButton(true)
        baseActivity.getActionBarView()?.setBackButton(layoutInflater)?.getClick {
            onBackPressed()
        }
    }
}