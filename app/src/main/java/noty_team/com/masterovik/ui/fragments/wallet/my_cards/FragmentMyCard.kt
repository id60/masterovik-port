package noty_team.com.masterovik.ui.fragments.wallet.my_cards

import android.arch.lifecycle.ViewModelProvider
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.fragment_my_cards.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.api.request.wallet.delete_card.DeleteCardRequest
import noty_team.com.masterovik.api.response.wallet.add_card.AddCardResponse
import noty_team.com.masterovik.api.response.wallet.edit_card.EditCardResponse

import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.ui.activities.main.UserProfileChangesViewModel
import noty_team.com.masterovik.ui.adapters.items_adapter.MyCardsItem
import noty_team.com.masterovik.ui.adapters.recycler.MyCardsListAdapter
import noty_team.com.masterovik.ui.fragments.wallet.add_card.AddCardFragment
import noty_team.com.masterovik.ui.fragments.wallet.cards_list.CardsListFragmentViewModel
import noty_team.com.masterovik.ui.fragments.wallet.top_up_balance.FragmentTopUpBalance
import noty_team.com.masterovik.utils.*
import noty_team.com.masterovik.utils.paper.PaperIO

class FragmentMyCard : BaseFragment<MyCardsFragmentViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): MyCardsFragmentViewModel {
        return injectViewModel(viewModelFactory)
    }

    override fun layout() = R.layout.fragment_my_cards

    private lateinit var myCardsAdapter: MyCardsListAdapter
    private lateinit var userProfileChangesViewModel: UserProfileChangesViewModel
    private lateinit var cardsListFragmentViewModel: CardsListFragmentViewModel

    override fun initialization(view: View, isFirstInit: Boolean) {

        if (isFirstInit) {
            userProfileChangesViewModel = baseActivity.injectViewModel(viewModelFactory)
            cardsListFragmentViewModel = baseActivity.injectViewModel(viewModelFactory)

            initViews()
            initClicks()
            initAdapter()
            listenUpdates()

            getMyCards()
        }
    }

    private fun initClicks() {
        top_up_balance_btn.getClick {
            fragmentManager.addFragment(FragmentTopUpBalance.newInstance())
            toolbarSizeListener.resizeContainerSmall()
        }

        add_card_button.getClick {
            fragmentManager.addFragment(AddCardFragment.newInstance())
        }
    }

    private fun getMyCards() {
        showProgress(true)
        cardsListFragmentViewModel.getMyCards()
    }

    private fun deleteCard(id: Int) {
        showProgress(true)
        viewModel.deleteCard(DeleteCardRequest(id))
    }

    private fun listenUpdates() {
        cardsListFragmentViewModel.myCardsData.observe(this) { response ->
            showProgress(false)
            when (response) {
                is Success -> {
                    myCardsAdapter.clear()
                    myCardsAdapter.addAll(response.value.map())

                }
                is Failure -> showErrorSnack(response.errorMessage)
            }
            checkEmptyList()
        }

        viewModel.deleteCardData.observe(this) { response ->
            showProgress(false)
            when (response) {
                is Success -> {
                    myCardsAdapter.deleteItem(response.value.request.cardId)
                }
                is Failure -> showErrorSnack(response.errorMessage)
            }
            checkEmptyList()
        }

        userProfileChangesViewModel.profileData.observe(this) { response ->
            showProgress(false)
            when (response) {
                is Success -> initViews()
                is Failure -> showErrorSnack(response.errorMessage)
            }
        }

        subscribeForResult {
            when (it.classProvider) {
                AddCardFragment::class,
                FragmentTopUpBalance::class -> {
                    if (it.value::class == AddCardResponse::class ||
                            it.value::class == EditCardResponse::class)
                        getMyCards()
                }
            }
        }
    }

    private fun checkEmptyList() {
        if (myCardsAdapter.itemCount > 0) empty_list_text_view.gone()
        else empty_list_text_view.visible()
    }

    private fun initViews() {
        val profileData = PaperIO.getUserProfile()
        user_name_text_view.text = profileData.firstName
        wallet_cache_text_view.text = getString(R.string.wallet_cash, profileData.balance ?: "0.0")
    }

    private fun initAdapter() {
        my_cards_list.layoutManager = LinearLayoutManager(baseContext)
        myCardsAdapter = MyCardsListAdapter(arrayListOf(),
                onDelete = {
                    deleteCard(it.id)
                },
                onEdit = {
                    fragmentManager.addFragment(AddCardFragment.newInstance(it))
                }
        )
        my_cards_list.adapter = myCardsAdapter
    }

}