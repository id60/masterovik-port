package noty_team.com.masterovik.ui.fragments.login.entrance

import android.arch.lifecycle.ViewModelProvider
import android.view.View
import android.widget.EditText
import android.widget.ProgressBar
import com.mikhaellopez.circularprogressbar.CircularProgressBar
import kotlinx.android.synthetic.main.fragment_entrance.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.api.request.auth.login.LoginRequest
import noty_team.com.masterovik.api.request.profile.ProfileRequest
import noty_team.com.masterovik.api.retrofit.CallbackWrapper.Companion.ERROR_NOT_FOUND_CODE
import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.ui.activities.login.LoginActivityViewModel
import noty_team.com.masterovik.ui.activities.main.MainActivity
import noty_team.com.masterovik.ui.activities.main.UserProfileChangesViewModel
import noty_team.com.masterovik.ui.fragments.login.selection_role.SelectionRoleFragment
import noty_team.com.masterovik.utils.*
import noty_team.com.masterovik.utils.enums.UserRole
import noty_team.com.masterovik.utils.paper.PaperIO
import java.util.concurrent.TimeUnit

class EntranceFragment : BaseFragment<EntranceFragmentViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): EntranceFragmentViewModel {
        return injectViewModel(viewModelFactory)
    }

    companion object {
        fun newInstance(): EntranceFragment {
            return EntranceFragment()
        }
    }

    override fun layout(): Int = R.layout.fragment_entrance

    private lateinit var userProfileChangesViewModel: UserProfileChangesViewModel
    private lateinit var loginActivityViewModel: LoginActivityViewModel

    override fun initialization(view: View, isFirstInit: Boolean) {
        baseActivity.getActionBarView()?.showActionBar(false)

        if (isFirstInit) {
            userProfileChangesViewModel = injectViewModel(viewModelFactory)
            loginActivityViewModel = baseActivity.injectViewModel(viewModelFactory)

            phoneManager.bindTo(phone_edit_text)

            initSmsButton()
            initLoginButton()
            listenUpdates()
        } else {
            clearData()
        }

    }

    private fun clearData() {
        userProfileChangesViewModel.profileData.value = null
        loginActivityViewModel.mediatorLiveData.value = null
        loginActivityViewModel.phoneData.value = null
        loginActivityViewModel.registrationRequestData.value = null
        phone_edit_text.listenForDrawn {
            (it as EditText).setText("")
        }
        sms_code_edit_text.listenForDrawn {
            (it as EditText).setText("")
        }
        progress_bar.listenForDrawn {
            (it as CircularProgressBar).progress = 100f
        }

        enableLoginButton(false)
        enableSmsButton(true)
        showSmsLayout(false)
        timerLayoutVisibility(false)
        viewModel.timerData.stop()
    }

    private fun getPhone() = phoneManager.phone.toString()/*.replace("+", "")*/

    private fun listenUpdates() {
        userProfileChangesViewModel.profileData.observe(this) {
            showProgress(false)
            when (it) {
                is Success -> {
                    it.value.apply {
                        PaperIO.setUserProfile(this)
                        MainActivity.start(baseActivity)
                    }
                }
                is Failure -> {
                    showErrorSnack(it.errorMessage)
                }
            }
        }

        viewModel.loginData.observe(this) { response ->
            showProgress(false)
            when (response) {
                is Success -> {
                    response.value.apply {
                        if (this.isExist == false) {
                            loginActivityViewModel.phoneData.value = getPhone()
                            fragmentManager.addFragment(SelectionRoleFragment.newInstance())
                        } else {
                            PaperIO.setAccessToken(accessToken ?: "")
                            PaperIO.setRefreshToken(refreshToken ?: "")
                            PaperIO.setRole(when (roleId) {
                                UserRole.EMPLOYER.id -> UserRole.EMPLOYER
                                UserRole.EXECUTOR.id -> UserRole.EXECUTOR
                                else -> UserRole.EMPLOYER
                            })
                            userProfileChangesViewModel.getProfile()
                        }
                    }
                }
                is Failure -> {
                    checkLoginError(response)
                }
            }
        }

        viewModel.smsData.observe(this) { response ->
            showProgress(false)
            when (response) {
                is Success -> {
                    response.value.apply {
                        message?.let { showSuccessSnack(it) }
                        showSmsFieldWithTimer()
                    }
                }
                is Failure -> {
                    checkSmsError(response)
                }
                is Loading -> {
                    showProgress(response.isLoading)
                }
            }
        }

        viewModel.timerData.observe(this) { tick ->
            if (tick.isFinished) enableSmsButton(true)
            else setTime(tick.time)
            timerLayoutVisibility(!tick.isFinished)
        }
    }

    private fun checkLoginError(failure: Failure<*>) {
        loginActivityViewModel.phoneData.value = getPhone()
        if (failure.errorCode == ERROR_NOT_FOUND_CODE)
            fragmentManager.addFragment(SelectionRoleFragment.newInstance())
        else showErrorSnack(failure.errorMessage)
    }

    private fun checkSmsError(failure: Failure<*>) {
        if (failure.errorCode == ERROR_NOT_FOUND_CODE)
            showSmsFieldWithTimer()
        else showErrorSnack(failure.errorMessage)
    }

    private fun initSmsButton() {
        get_sms_button.getClick {
            if (validatePhone()) {
                showProgress(true)
                viewModel.login(LoginRequest(getPhone()))
            }
        }
    }

    private fun showSmsFieldWithTimer() {
        enableLoginButton(true)
        enableSmsButton(false)
        showSmsLayout(true)
        timerLayoutVisibility(true)
        progress_bar.progress = 100f
        viewModel.timerData.start()
    }

    private fun initLoginButton() {
        enableLoginButton(false)
        login_button.getClick {
            if (validate()) {
                showProgress(true)
                val phone = getPhone()
                val smsCode = sms_code_edit_text.text.toString().trim()
                val request = LoginRequest(phone, smsCode)
                viewModel.login(request)
            }
        }
    }

    private fun showSmsLayout(show: Boolean) {
        if (show) {
            get_sms_button.text = getString(R.string.send_code_again)
            sms_code_layout.visible()
        } else {
            get_sms_button.text = getString(R.string.get_code)
            sms_code_layout.gone()
        }
    }

    private fun setTime(timeMillis: Long) {
        timer_text_view.text = TimeUnit.MILLISECONDS.toSeconds(timeMillis).toString()
        progress_bar.progress = 100f * timeMillis / viewModel.timerData.totalTime
    }

    private fun timerLayoutVisibility(show: Boolean) {
        if (show) timer_layout.visible()
        else timer_layout.gone()
    }

    private fun enableSmsButton(enable: Boolean) {
        if (enable) get_sms_button.enable()
        else get_sms_button.disable()
    }

    private fun enableLoginButton(enable: Boolean) {
        if (enable) login_button.enable()
        else login_button.disable()
    }

    private fun validatePhone(): Boolean {
        phone_edit_text.setError(false)
        val phone = phone_edit_text.text.toString().trim()
        return phone_edit_text.validate(
                validator.isPhoneValid(phone),
                viewForShake = phone_edit_text
        )
    }

    private fun validateSms(): Boolean {
        sms_code_edit_text.setError(false)
        val smsCode = sms_code_edit_text.text.toString().trim()
        return sms_code_edit_text.validate(
                validator.isSmsValid(smsCode),
                viewForShake = sms_code_edit_text
        )
    }

    private fun validate(): Boolean {
        return validatePhone() && validateSms()
    }
}