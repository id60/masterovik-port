package noty_team.com.masterovik.ui.fragments.profile_setting.edit_phone_number

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import noty_team.com.masterovik.api.request.change_phone.ChangePhoneRequest
import noty_team.com.masterovik.api.response.change_phone.ChangePhoneResponse
import noty_team.com.masterovik.api.response.city.cities_list.CitiesResponse
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.ui.fragments.login.entrance.CountDownTimerLiveData
import noty_team.com.masterovik.utils.Result
import javax.inject.Inject

class EditPhoneFragmentViewModel @Inject constructor(app: Application) : BaseViewModel(app) {

    val changePhoneData = MutableLiveData<Result<ChangePhoneResponse>>()
    val smsData = MutableLiveData<Result<ChangePhoneResponse>>()
    val timerData = CountDownTimerLiveData(1000, 1000)

    fun changePhone(data: ChangePhoneRequest){
        api.profileApi.changePhone(data) {
            if (data.sms == null) smsData.postValue(it)
            else changePhoneData.postValue(it)
        }.call()
    }

    override fun onClear() {
        changePhoneData.value = null
        smsData.value = null
        timerData.stop()
    }
}