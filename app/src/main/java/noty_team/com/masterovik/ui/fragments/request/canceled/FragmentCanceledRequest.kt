package noty_team.com.masterovik.ui.fragments.request.canceled

import android.arch.lifecycle.ViewModelProvider
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.fragment_canceled_request.*
import noty_team.com.masterovik.R

import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.ui.adapters.items_adapter.CanceledRequestItem
import noty_team.com.masterovik.ui.adapters.recycler.CanceledRequestAdapter
import noty_team.com.masterovik.utils.injectViewModel

class FragmentCanceledRequest : BaseFragment<BaseViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): BaseViewModel {
        return injectViewModel(viewModelFactory)
    }


    override fun layout() = R.layout.fragment_canceled_request

    override fun initialization(view: View, isFirstInit: Boolean) {

        recycler_canceled_request.layoutManager = LinearLayoutManager(baseActivity)
        recycler_canceled_request.adapter = CanceledRequestAdapter(createList()){

        }

    }

    fun createList(): ArrayList<CanceledRequestItem> {

        var list: ArrayList<CanceledRequestItem> = ArrayList()
        var item = CanceledRequestItem("Название объявления", "23 февраля 2019", 20000, getString(R.string.some_text))

        list.add(item)
        list.add(item)
        list.add(item)
        list.add(item)


        return list

    }
}