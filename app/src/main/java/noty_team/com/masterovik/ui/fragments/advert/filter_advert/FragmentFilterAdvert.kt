package noty_team.com.masterovik.ui.fragments.advert.filter_advert

import android.arch.lifecycle.ViewModelProvider
import android.view.View
import kotlinx.android.synthetic.main.fragment_filter_adverts.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.api.request.advert.get_catalog_adverts.CatalogAdvertsRequest

import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.ui.adapters.items_adapter.CityItem
import noty_team.com.masterovik.ui.adapters.items_adapter.ProvideServiceItem
import noty_team.com.masterovik.ui.fragments.advert.filter_advert.date_filter.DateFilterFragment
import noty_team.com.masterovik.ui.fragments.advert.filter_advert.price_filter.PriceFilterFragment
import noty_team.com.masterovik.ui.fragments.advert.filter_advert.time_filter.TimeFilterFragment
import noty_team.com.masterovik.ui.fragments.cities_list.CitiesListFragment
import noty_team.com.masterovik.ui.fragments.profile_setting.edit_setting_sevice.services_list.ServicesListFragment
import noty_team.com.masterovik.utils.*
import java.util.*
import kotlin.collections.ArrayList

class FragmentFilterAdvert : BaseFragment<AdvertFilterFragmentViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): AdvertFilterFragmentViewModel {
        return baseActivity.injectViewModel(viewModelFactory)
    }

    companion object {
        fun newInstance(): FragmentFilterAdvert = FragmentFilterAdvert()
    }

    override fun layout() = R.layout.fragment_filter_adverts

    private var cityItem: CityItem? = null
    private var servicesList = arrayListOf<ProvideServiceItem>()
    private var prices = arrayListOf<Int>()
    private var dates = arrayListOf<Long>()
    private var times = arrayListOf<Long>()

    override fun initialization(view: View, isFirstInit: Boolean) {
        initToolbar()
        if (isFirstInit) {
            initClicks()
            listenUpdates()
        }
    }

    private fun initClicks() {
        reset_filters_button.getClick {
            cityItem = null
            servicesList.clear()
            prices.clear()
            dates.clear()
            times.clear()

            city_text_view.text = ""
            service_text_view.text = ""
            price_text_view.text = ""

            date_text_view.gone()
            date_text_view.text = ""

            time_text_view.gone()
            time_text_view.text = ""

            viewModel.setSortingData(CatalogAdvertsRequest())
        }

        city_layout.getClick {
            fragmentManager.addFragment(CitiesListFragment.newInstance(cityItem?.id?:-1))
        }

        service_layout.getClick {
            fragmentManager.addFragment(ServicesListFragment.newInstance(
                    servicesList.flatMap { arrayListOf( it.id ) } as ArrayList<Int>, false))
        }

        price_layout.getClick {
            fragmentManager.addFragment(PriceFilterFragment.newInstance(prices))
        }

        date_layout.getClick {
            fragmentManager.addFragment(DateFilterFragment.newInstance(dates))
        }

        time_layout.getClick {
            fragmentManager.addFragment(TimeFilterFragment.newInstance(times))
        }

        search_button.getClick {
            val servicesId = if (servicesList.isNotEmpty())
                servicesList.flatMap { arrayListOf( it.id ) } as ArrayList<Int>
            else null
            val price = if (prices.size >= 2) prices else null
            val date = if (dates.size >= 2) dates else null
            val term = if (times.size >= 2) arrayListOf(
                    CustomTimePicker.getTimeCalendar(times[0]).get(Calendar.HOUR_OF_DAY),
                    CustomTimePicker.getTimeCalendar(times[1]).get(Calendar.HOUR_OF_DAY)
            ) else null
            val searchRequest = CatalogAdvertsRequest(
                    cityId = cityItem?.id,
                    servicesId = servicesId,
                    price = price,
                    date = date,
                    term = term,
                    cityItem = if (cityItem != null) cityItem else null,
                    servicesList = if (servicesList.isNotEmpty()) servicesList else null
            )
            viewModel.setSortingData(searchRequest)
            onBackPressed()
        }
    }

    private fun listenUpdates() {
        viewModel.advertSortingData.observe(this) { response ->
            initFilters(response)
        }

        subscribeForResult {
            when(it.classProvider){
                CitiesListFragment::class -> {
                    if (it.value is CityItem) {
                        cityItem = it.value
                        city_text_view.text = "(${cityItem?.cityName})"
                    }
                }
                ServicesListFragment::class -> {
                    if (it.value is ArrayList<*> && it.value.isNotEmpty()
                            && it.value[0] is ProvideServiceItem) {
                        servicesList = it.value as ArrayList<ProvideServiceItem>
                        service_text_view.text = "(${ servicesList.joinToString { it.serviceName } })"
                    }
                }
                PriceFilterFragment::class -> {
                    if (it.value is ArrayList<*> && it.value.size >= 2){
                        prices = it.value as ArrayList<Int>
                        price_text_view.text = "(от ${prices[0]} до ${prices[1]})"
                    }
                }
                DateFilterFragment::class -> {
                    if (it.value is ArrayList<*> && it.value.size >= 2){
                        dates = it.value as ArrayList<Long>
                        val dateFrom = CustomDatePicker.getFormattedDateString(dates[0])
                        val dateTo = CustomDatePicker.getFormattedDateString(dates[1])
                        date_text_view.visible()
                        date_text_view.text = "(от ${dateFrom} до ${dateTo})"
                    }
                }
                TimeFilterFragment::class -> {
                    if (it.value is ArrayList<*> && it.value.size >= 2){
                        times = it.value as ArrayList<Long>
                        val timeFrom = CustomTimePicker.getFormattedTimeString(times[0])
                        val timeTo = CustomTimePicker.getFormattedTimeString(times[1])
                        time_text_view.visible()
                        time_text_view.text = "(от ${timeFrom} до ${timeTo})"
                    }
                }
            }
        }
    }

    private fun initFilters(request: CatalogAdvertsRequest) {
        cityItem = request.cityItem
        servicesList = request.servicesList?: arrayListOf()
        prices = request.price?: arrayListOf()
        dates = request.date?: arrayListOf()
        times = request.term?.let {
            it.flatMap { hour ->
                arrayListOf(
                        CustomTimePicker.getTimeCalendar(
                                String.format("%02d", hour)
                        ).timeInMillis
                )
            } as ArrayList<Long>
        }?: arrayListOf()

        if (cityItem?.cityName?.isNotEmpty() == true)
            city_text_view.text = "(${cityItem?.cityName})"
        else
            city_text_view.text = ""

        if (servicesList.isNotEmpty())
            service_text_view.text = "(${servicesList.joinToString { it.serviceName }})"
        else
            service_text_view.text = ""

        if (prices.isNotEmpty())
            price_text_view.text = "(от ${prices[0]} до ${prices[1]})"
        else
            price_text_view.text = ""

        if (dates.size >= 2) {
            val dateFrom = CustomDatePicker.getFormattedDateString(dates[0])
            val dateTo = CustomDatePicker.getFormattedDateString(dates[1])
            date_text_view.visible()
            date_text_view.text = "(от ${dateFrom} до ${dateTo})"
        }
        else {
            date_text_view.gone()
            date_text_view.text = ""
        }

        if (times.size >= 2) {
            val timeFrom = CustomTimePicker.getFormattedTimeString(times[0])
            val timeTo = CustomTimePicker.getFormattedTimeString(times[1])
            time_text_view.visible()
            time_text_view.text = "(от ${timeFrom} до ${timeTo})"
        }
        else {
            time_text_view.gone()
            time_text_view.text = ""
        }
    }

    private fun initToolbar() {
        toolbarSizeListener.hideEmptyToolbarSpace(true)
        baseActivity.getActionBarView()?.showActionBar(false)

        cross_filter_advert.getClick {
            onBackPressed()
        }
    }
}