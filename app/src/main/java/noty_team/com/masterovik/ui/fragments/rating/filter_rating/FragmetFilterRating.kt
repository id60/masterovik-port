package noty_team.com.masterovik.ui.fragments.rating.filter_rating

import android.arch.lifecycle.ViewModelProvider
import android.view.View
import noty_team.com.masterovik.R

import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.utils.injectViewModel

class FragmetFilterRating : BaseFragment<BaseViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): BaseViewModel {
        return injectViewModel(viewModelFactory)
    }

    companion object {
        fun newInstance() = FragmetFilterRating()
    }


    override fun layout() = R.layout.fragment_filter_rating

    override fun initialization(view: View, isFirstInit: Boolean) {
        toolbarSizeListener.hideEmptyToolbarSpace(true)
        baseActivity.getActionBarView()?.showActionBar(false)

        baseActivity.findViewById<View>(R.id.cross_filter_rating).setOnClickListener {
            onBackPressed()
        }
    }
}