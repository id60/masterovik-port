package noty_team.com.masterovik.ui.fragments.rating

import android.arch.lifecycle.ViewModelProvider
import android.view.LayoutInflater
import android.view.View
import kotlinx.android.synthetic.main.fragment_base_rating.*
import noty_team.com.masterovik.R

import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.ui.adapters.view_pager.RatingPageAdapter
import noty_team.com.masterovik.ui.fragments.rating.filter_rating.FragmetFilterRating
import noty_team.com.masterovik.ui.fragments.rating.rating.FragmentRating
import noty_team.com.masterovik.ui.fragments.rating.rating_feedback.FragmentRatingFeedback
import noty_team.com.masterovik.utils.injectViewModel

class FragmentBaseRating : BaseFragment<BaseViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): BaseViewModel {
        return injectViewModel(viewModelFactory)
    }
    companion object {
        fun newInstance() = FragmentBaseRating()
    }

    override fun layout() = R.layout.fragment_base_rating
    override fun initialization(view: View, isFirstInit: Boolean) {
        initToolbar()

        view_pager_rating.adapter = RatingPageAdapter(childFragmentManager, view_pager_rating,
                arrayListOf(FragmentRating(), FragmentRatingFeedback()))
        tabLayout_rating.setupWithViewPager(view_pager_rating)

    }

    private fun initToolbar(){
        toolbarSizeListener.hideEmptyToolbarSpace(false)
        baseActivity.getActionBarView()?.setSmallContainerWidth()
        baseActivity.getActionBarView()?.showActionBar(true)
        toolbarSizeListener.resizeContainerLarge()
        baseActivity.getActionBarView()?.setupCenterText("Мой рейтинг")

        val rightView = LayoutInflater.from(baseActivity).inflate(R.layout.ab_right_filter, null)
        baseActivity.getActionBarView()?.setupRightButton(rightView)
        baseActivity.findViewById<View>(R.id.filter).setOnClickListener {
            //            baseActivity.navigator.navigateToFragment(FragmetFilterRating.newInstance(), null, true)
            fragmentManager.addFragment(FragmetFilterRating.newInstance())

            toolbarSizeListener.resizeContainerSmall()
        }

        baseActivity.getActionBarView()?.showLeftButton(true)
        baseActivity.getActionBarView()?.setDrawerMenu(layoutInflater)?.getClick {
            onDrawerPressedListener.onDrawerPressed()
        }
    }
}