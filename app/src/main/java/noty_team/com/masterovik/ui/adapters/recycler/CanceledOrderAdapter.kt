package noty_team.com.masterovik.ui.adapters.recycler

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import noty_team.com.masterovik.R
import noty_team.com.masterovik.ui.adapters.items_adapter.CanceledOrderItem

class CanceledOrderAdapter(val items: ArrayList<CanceledOrderItem>,
                           val onClick:()->Unit) :
        RecyclerView.Adapter<CanceledOrderAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_canceled_order, parent, false))
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder?.nameCurrentOrder?.text = items.get(position).nameOrder
        holder?.nameSurnameCurrenrOrder?.text = items.get(position).nameSurname
        holder?.currentOrderDay?.text = items.get(position).day.toString() + " января 2019"
        holder?.canceledInfo?.text = items.get(position).canceledInfo


    }


    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {

        var nameCurrentOrder = view.findViewById<TextView>(R.id.name_canceled_order)
        var nameSurnameCurrenrOrder = view.findViewById<TextView>(R.id.name_surname_canceled_order)
        var currentOrderDay = view.findViewById<TextView>(R.id.date_canceled_order)
        var canceledInfo = view.findViewById<TextView>(R.id.canceled_order_info)


        override fun onClick(v: View?) {
            when (v?.id) {

//                onClick()
            }
        }

    }
}