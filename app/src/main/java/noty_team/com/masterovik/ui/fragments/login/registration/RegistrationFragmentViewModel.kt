package noty_team.com.masterovik.ui.fragments.login.registration

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import noty_team.com.masterovik.api.request.auth.register.RegistrationRequest
import noty_team.com.masterovik.api.response.city.cities_list.CitiesResponse
import noty_team.com.masterovik.api.response.auth.registration.RegistrationResponse
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.utils.Result
import javax.inject.Inject

class RegistrationFragmentViewModel @Inject constructor(app: Application) : BaseViewModel(app) {

    val registerData = MutableLiveData<Result<RegistrationResponse>>()
    val getCityData = MutableLiveData<Result<CitiesResponse>>()

    fun register(request: RegistrationRequest){
        api.loginApi.register(request) {
            registerData.postValue(it)
        }.call()
    }

    fun getCities(){
        api.cityApi.getCities { getCityData.postValue(it) }.call()
    }

    override fun onClear() {

    }
}