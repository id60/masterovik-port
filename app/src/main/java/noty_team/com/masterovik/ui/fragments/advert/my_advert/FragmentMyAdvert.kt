package noty_team.com.masterovik.ui.fragments.advert.my_advert

import android.arch.lifecycle.ViewModelProvider
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.fragment_my_adver.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.api.request.services.AddServiceRequest
import noty_team.com.masterovik.api.response.city.cities_list.ResponseItem

import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.ui.activities.main.UserProfileChangesViewModel
import noty_team.com.masterovik.ui.adapters.items_adapter.ProvideServiceItem
import noty_team.com.masterovik.ui.adapters.recycler.AdvertAdapter
import noty_team.com.masterovik.ui.adapters.recycler.ProvideSeviceAdapter
import noty_team.com.masterovik.ui.fragments.advert.advert_instance.FragmentInstanceAdvert
import noty_team.com.masterovik.ui.fragments.cities_list.CitiesListFragmentViewModel
import noty_team.com.masterovik.ui.fragments.profile_setting.edit_setting_sevice.EditServiceFragmentViewModel
import noty_team.com.masterovik.ui.fragments.profile_setting.edit_setting_sevice.services_list.ServicesListFragment
import noty_team.com.masterovik.utils.*
import noty_team.com.masterovik.utils.adapters.SpacingItemDecoration
import noty_team.com.masterovik.utils.paper.PaperIO

class FragmentMyAdvert : BaseFragment<MyAdvertsFragmentViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): MyAdvertsFragmentViewModel {
        return injectViewModel(viewModelFactory)
    }

    override fun layout() = R.layout.fragment_my_adver

    private lateinit var servicesAdapter: ProvideSeviceAdapter
    private lateinit var advertAdapter: AdvertAdapter

    private lateinit var editServiceViewModel: EditServiceFragmentViewModel
    private lateinit var citiesViewModel: CitiesListFragmentViewModel
    private lateinit var userProfileChangesViewModel: UserProfileChangesViewModel

    override fun initialization(view: View, isFirstInit: Boolean) {
        initToolbar()

        if(isFirstInit){
            editServiceViewModel = injectViewModel(viewModelFactory)
            citiesViewModel = injectViewModel(viewModelFactory)
            userProfileChangesViewModel = baseActivity.injectViewModel(viewModelFactory)

            initAdvertAdapter()
            initServicesAdapter()
            initClicks()
            listenUpdates()

            getCities()
            getServices()
            getMyAdverts()
        }
    }

    private fun getCities(){
        showProgress(true)
        citiesViewModel.getCities()
    }

    private fun getServices(){
        showProgress(true)
        editServiceViewModel.getServices()
    }

    private fun deleteService(id: Int){
        showProgress(true)
        editServiceViewModel.deleteService(AddServiceRequest( services = arrayListOf(id) ))
    }

    private fun getMyAdverts(){
        showProgress(true)
        viewModel.getAdverts()
    }

    private fun initClicks(){
        add_service_button.getClick {
            val serviceIds = servicesAdapter.getItemsList().flatMap { arrayListOf(it.id) } as ArrayList
            fragmentManager.addFragment(ServicesListFragment.newInstance(serviceIds))
        }
    }

    private fun listenUpdates() {
        citiesViewModel.citiesData.observe(this) { response ->
            showProgress(false)
            when (response) {
                is Success -> {
                    response.value.response?.let {
                        initAdvertCity(it)
                    }
                }
                is Failure -> {
                    showErrorSnack(response.errorMessage)
                }
            }
        }

        viewModel.myAdvertsData.observe(this) { response ->
            showProgress(false)
            when (response) {
                is Success -> {
                    advertAdapter.clear()
                    advertAdapter.addAll(response.value.map())
                }
                is Failure -> {
                    showErrorSnack(response.errorMessage)
                }
            }
            checkEmptyList()
        }

        editServiceViewModel.userServicesData.observe(this) { response ->
            showProgress(false)
            when (response) {
                is Success -> {
                    servicesAdapter.clear()
                    servicesAdapter.addAll(response.value.map())
                }
                is Failure -> {
                    showErrorSnack(response.errorMessage)
                }
            }
        }

        editServiceViewModel.deleteServiceData.observe(this) { response ->
            showProgress(false)
            when (response) {
                is Success -> {
                    response.value.request.services.let {
                        if (it.isNotEmpty()){
                            servicesAdapter.deleteItem(it[0])

                            getMyAdverts()
                        }
                    }
                }
                is Failure -> {
                    showErrorSnack(response.errorMessage)
                }
            }
        }

        userProfileChangesViewModel.profileData.observe(this) { response ->
            showProgress(false)
            when (response) {
                is Success -> getCities()
                is Failure -> showErrorSnack(response.errorMessage)
            }
        }

        subscribeForResult {
            if (it.classProvider == ServicesListFragment::class
                    && viewPagerListener.isCurrentPageSelected()) {
                servicesAdapter.addWithSorting(it.value as ArrayList<ProvideServiceItem>)

                getMyAdverts()
            }
        }
    }

    private fun checkEmptyList() {
        if (advertAdapter.itemCount > 0) empty_list_text_view.gone()
        else empty_list_text_view.visible()
    }

    private fun initAdvertCity(cities: ArrayList<ResponseItem>){
        advert_city_text_view.text = cities.filter { it.id == PaperIO.getUserProfile().cityId }.let {
            if (it.isNotEmpty()) it[0].city
            else ""
        }
    }

    private fun initServicesAdapter() {
        recycler_provide_service.layoutManager = GridLayoutManager(baseContext, 3)
        recycler_provide_service.addItemDecoration(
                SpacingItemDecoration(3, convertDpToPixel(8f, baseContext).toInt(), false))
        servicesAdapter = ProvideSeviceAdapter(arrayListOf()){
            deleteService(it.id)
        }
        recycler_provide_service.adapter = servicesAdapter
    }

    private fun initAdvertAdapter(){
        recycler_my_advert.layoutManager = LinearLayoutManager(baseActivity)
        advertAdapter = AdvertAdapter(arrayListOf()){
            fragmentManager.addFragment(FragmentInstanceAdvert.newInstance(it))
        }
        recycler_my_advert.adapter = advertAdapter
    }

    private fun initToolbar() {

    }
}