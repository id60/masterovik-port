package noty_team.com.masterovik.ui.fragments.wallet.add_card

import android.arch.lifecycle.ViewModelProvider
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import com.github.vacxe.phonemask.PhoneMaskManager
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_add_card2.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.api.request.wallet.add_card.AddCardRequest
import noty_team.com.masterovik.api.request.wallet.edit_card.EditCardRequest
import noty_team.com.masterovik.api.response.wallet.add_card.AddCardResponse

import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.ui.adapters.items_adapter.MyCardsItem
import noty_team.com.masterovik.utils.*

class AddCardFragment : BaseFragment<AddCardFragmentViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): AddCardFragmentViewModel {
        return injectViewModel(viewModelFactory)
    }

    companion object {
        private const val CARD_KEY = "CARD_KEY"
        fun newInstance(card: MyCardsItem = MyCardsItem()): AddCardFragment {
            return AddCardFragment().apply {
                arguments = Bundle().apply {
                    val cardString = Gson().toJson(card)
                    putString(CARD_KEY, cardString)
                }
            }
        }
        fun getCard(arguments: Bundle?): MyCardsItem {
            val cardString = arguments?.getString(CARD_KEY)?:""
            if (cardString.isNotEmpty()){
                val card = Gson().fromJson(cardString, MyCardsItem::class.java)
                if (card != null) return card
            }
            return MyCardsItem()
        }
    }

//    data class CreditCardData(var number: String = "",
//                              var date: String = "",
//                              var cvv: String = ""){
//        fun checkFields(): Boolean{
//            return number.length == 16 && date.length == 4 && cvv.length == 3
//        }
//    }

    override fun layout() = R.layout.fragment_add_card2

    val cardFormatter = PhoneMaskManager().withMask("#### #### #### ####")
//    val dateFormatter = PhoneMaskManager().withMask("##/##")
//    val cvvFormatter = PhoneMaskManager().withMask("###")
//    var creditCardData = CreditCardData()

    override fun initialization(view: View, isFirstInit: Boolean) {
        initToolbar()

        if (isFirstInit){
            initEditTexts()
            initClicks()
            listenUpdates()
        }
    }

    private fun initEditTexts(){
        cardFormatter.bindTo(card_number_edit_text as EditText)
        val card = getCard(arguments)
        card_number_edit_text.setText(card.cardNumber)


//        dateFormatter.bindTo(card_date_edit_text as EditText)
//        cvvFormatter.bindTo(card_cvv_edit_text as EditText)
//        card_number_edit_text.addTextChangedListener(object: TextWatcher {
//            override fun afterTextChanged(p0: Editable) {
//                val number = numberFormatter.phone.toString()
//                creditCardData.number = number.replace("+", "")
//                setCardImage(creditCardData.number)
//            }
//            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
//            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
//        })
//        card_date_edit_text.addTextChangedListener(object: TextWatcher{
//            override fun afterTextChanged(p0: Editable) {
//                val date = dateFormatter.phone.toString()
//                creditCardData.date = date.replace("+", "")
//            }
//            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
//            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
//        })
//        card_cvv_edit_text.addTextChangedListener(object: TextWatcher{
//            override fun afterTextChanged(p0: Editable) {
//                val cvv = cvvFormatter.phone.toString()
//                creditCardData.cvv = cvv.replace("+", "")
//            }
//            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
//            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
//        })

    }

    private fun getCardNumber(): String {
        val number = cardFormatter.phone.toString()
        return number.replace("+", "")
    }

    private fun initClicks(){
        button_save.getClick {
            if (validateCardNumber()) {
                val card = getCard(arguments)
                val cardNumber = getCardNumber()
                if (card.id >= 0) editCard(cardNumber)
                else addCard(cardNumber)
            }
            else showErrorSnack("Неверные данные карты")
        }
    }

    private fun addCard(cardNumber: String){
        showProgress(true)
        viewModel.addCard(AddCardRequest(cardNumber))
    }

    private fun editCard(cardNumber: String){
        showProgress(true)
        val card = getCard(arguments)
        viewModel.editCard(EditCardRequest(card.id, cardNumber))
    }

    private fun listenUpdates() {
        viewModel.addCardData.observe(this) { response ->
            showProgress(false)
            when (response) {
                is Success -> {
                    setBackResult(response.value)
                }
                is Failure -> {
                    showErrorSnack(response.errorMessage)
                }
            }
        }

        viewModel.editCardData.observe(this) { response ->
            showProgress(false)
            when (response) {
                is Success -> {
                    response.value.response?.let {
                        showSuccessSnack(it)
                    }
                    setBackResult(response.value)
                }
                is Failure -> {
                    showErrorSnack(response.errorMessage)
                }
            }
        }
    }

    private fun setBackResult(response: Any){
        sendBackResult(response)
        onBackPressed()
    }

    private fun initToolbar(){
        if (getCard(arguments).id >= 0) center_text.text = "Редактировать карту"
        else center_text.text = "Добавить карту"
        toolbarSizeListener.enableSnackBarBehaviour(false)
        toolbarSizeListener.hideEmptyToolbarSpace(true)
        baseActivity.getActionBarView()?.showActionBar(false)
        back_button.getClick {
            onBackPressed()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        toolbarSizeListener.enableSnackBarBehaviour(true)
    }

    private fun validateCardNumber(): Boolean {
        card_number_edit_text.setError(false)
        val cardNumber = getCardNumber()
        return card_number_edit_text.validate(
                validator.isCardNumberValid(cardNumber),
                viewForShake = card_number_edit_text
        )
    }
}