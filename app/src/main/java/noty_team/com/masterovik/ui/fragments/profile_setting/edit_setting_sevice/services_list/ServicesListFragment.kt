package noty_team.com.masterovik.ui.fragments.profile_setting.edit_setting_sevice.services_list

import android.arch.lifecycle.ViewModelProvider
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.fragment_services_list.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.api.request.services.AddServiceRequest
import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.ui.adapters.recycler.ServicesListAdapter
import noty_team.com.masterovik.utils.*
import noty_team.com.masterovik.utils.adapters.SpacingItemDecoration

class ServicesListFragment : BaseFragment<ServicesListFragmentViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): ServicesListFragmentViewModel {
        return injectViewModel(viewModelFactory)
    }

    companion object {
        private const val USER_SERVICES_IDS_KEY = "USER_SERVICES_IDS_KEY"
        private const val SHOULD_SAVE_TO_SERVER_KEY = "SHOULD_SAVE_TO_SERVER_KEY"
        fun newInstance(ids: ArrayList<Int>, shouldSaveToServer: Boolean = true) = ServicesListFragment().apply {
            arguments = Bundle().apply {
                putIntArray(USER_SERVICES_IDS_KEY, ids.toIntArray())
                putBoolean(SHOULD_SAVE_TO_SERVER_KEY, shouldSaveToServer)
            }
        }
        fun getUserServiceIds(arguments: Bundle?): ArrayList<Int> =
                arguments?.getIntArray(USER_SERVICES_IDS_KEY)?.toCollection(arrayListOf())?: arrayListOf()
        fun shouldSaveToServer(arguments: Bundle?): Boolean =
                arguments?.getBoolean(SHOULD_SAVE_TO_SERVER_KEY)?:true
    }

    override fun layout() = R.layout.fragment_services_list

    private lateinit var servicesAdapter: ServicesListAdapter

    override fun initialization(view: View, isFirstInit: Boolean) {
        initToolbar()

        if (isFirstInit){
            initClicks()
            initServicesAdapter()
            listenUpdates()

            getServices()
        }
    }

    private fun setBackResult(){
        val returnServices = servicesAdapter.getCheckedItems()
        sendBackResult(returnServices)
        onBackPressed()
    }

    private fun initClicks(){
        save_button.getClick {
            addServices()
        }
    }

    private fun initServicesAdapter() {
        services_list.layoutManager = LinearLayoutManager(baseContext)
        servicesAdapter = ServicesListAdapter(arrayListOf()){}
        services_list.adapter = servicesAdapter
    }

    private fun addServices(){
        val checkedServices = servicesAdapter.getCheckedItems()
        if (checkedServices.isNotEmpty()) {
            if (shouldSaveToServer(arguments)){
                showProgress(true)
                val request = AddServiceRequest(
                        services = checkedServices.flatMap { arrayListOf( it.id ) }
                )
                viewModel.addService(request)
            }
            else setBackResult()
        }
        else showErrorSnack("Выберите хотя бы один сервис")
    }

    private fun getServices(){
        showProgress(true)
        viewModel.getServices()
    }

    private fun listenUpdates() {
        viewModel.getServiceData.observe(this) { response ->
            showProgress(false)
            when (response) {
                is Success -> {
                    servicesAdapter.clear()
                    if (shouldSaveToServer(arguments))
                        servicesAdapter.addUniqueItems(
                                response.value.map(),
                                getUserServiceIds(arguments)
                        )
                    else
                        servicesAdapter.addAll(
                                response.value.map().apply {
                                    val selectedServices = getUserServiceIds(arguments)
                                    forEach { item ->
                                        item.isChecked = selectedServices.any { it == item.id }
                                    }
                                }
                        )
                }
                is Failure -> {
                    showErrorSnack(response.errorMessage)
                }
            }
            checkEmptyList()
        }

        viewModel.addServiceData.observe(this) { response ->
            showProgress(false)
            when (response) {
                is Success -> {
                    setBackResult()
                }
                is Failure -> {
                    showErrorSnack(response.errorMessage)
                }
            }
        }
    }

    private fun checkEmptyList(){
        if (servicesAdapter.itemCount > 0) {
            save_button.enable()
            empty_list_text_view.gone()
        }
        else {
            save_button.disable()
            empty_list_text_view.visible()
        }
    }

    private fun initToolbar(){
        baseActivity.getActionBarView()?.showActionBar(true)
        toolbarSizeListener.hideEmptyToolbarSpace(false)
        baseActivity.getActionBarView()?.setupCenterText("Все услуги")
        baseActivity.getActionBarView()?.setSmallToolbarHeight()
        baseActivity.getActionBarView()?.showLeftButton(true)
        baseActivity.getActionBarView()?.setupLeftButton(layoutInflater.inflate(R.layout.ab_back, null))
        baseActivity.getActionBarView()?.getLeftButton()?.getClick {
            onBackPressed()
        }
        baseActivity.getActionBarView()?.setupRightButton(layoutInflater.inflate(R.layout.ab_empty, null))
    }
}