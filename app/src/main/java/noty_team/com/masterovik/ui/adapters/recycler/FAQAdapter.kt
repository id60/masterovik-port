package noty_team.com.masterovik.ui.adapters.recycler

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_faq.view.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.ui.adapters.items_adapter.FAQItem

class FAQAdapter(val items: ArrayList<FAQItem>, val context: Context) : RecyclerView.Adapter<FAQAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_faq, parent, false))
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder?.title?.text = items.get(position).title
        holder?.subtitle?.text = items.get(position).subtitle
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var title = view.title_faq
        var subtitle = view.description_faq
    }
}