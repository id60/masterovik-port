package noty_team.com.masterovik.ui.fragments.rating.rating

import android.arch.lifecycle.ViewModelProvider
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.fragment_rating.*
import noty_team.com.masterovik.R

import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.ui.adapters.items_adapter.RatingItem
import noty_team.com.masterovik.ui.adapters.recycler.RatingAdapter
import noty_team.com.masterovik.ui.fragments.rating.rating_review_detail.FragmentReviewDetail
import noty_team.com.masterovik.utils.injectViewModel

class FragmentRating : BaseFragment<BaseViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): BaseViewModel {
        return injectViewModel(viewModelFactory)
    }
    override fun layout() = R.layout.fragment_rating

    override fun initialization(view: View, isFirstInit: Boolean) {

        recycler_rating.layoutManager = LinearLayoutManager(baseActivity)
        recycler_rating.adapter = RatingAdapter(createList()){
            baseActivity.fragmentManager.addFragment(FragmentReviewDetail.newInstance())
            toolbarSizeListener.resizeContainerSmall()
        }

    }

    private fun createList(): ArrayList<RatingItem> {
        var list: ArrayList<RatingItem> = ArrayList()

        var item1 = RatingItem("Имя заказчика", "20 мая 2018", 3)
        var item2 = RatingItem("Имя заказчика", "20 мая 2018", 4)
        var item3 = RatingItem("Имя заказчика", "20 мая 2018", 4)
        var item4 = RatingItem("Имя заказчика", "20 мая 2018", 2)

        list.add(item1)
        list.add(item2)
        list.add(item3)
        list.add(item4)
        list.add(item4)

        return list
    }
}