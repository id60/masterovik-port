package noty_team.com.masterovik.ui.adapters.recycler

import android.support.v7.widget.PopupMenu
import android.view.Gravity
import android.view.View
import kotlinx.android.synthetic.main.my_cards_item.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.ui.adapters.items_adapter.MyCardsItem
import noty_team.com.masterovik.utils.CreditCardUtils
import noty_team.com.masterovik.utils.loadImage
import java.lang.Exception

class MyCardsListAdapter (list: ArrayList<MyCardsItem>,
                          val onDelete:(item: MyCardsItem)->Unit,
                          val onEdit:(item: MyCardsItem)->Unit) :
        BaseAdapter<MyCardsItem, MyCardsListAdapter.ViewHolder>(list) {

    override fun createViewHolder(view: View, layoutId: Int) = ViewHolder(view)

    override fun getItemViewType(position: Int) = R.layout.my_cards_item

    private val deletePopUpId = 66
    private val editPopUpId = 67

    inner class ViewHolder(view: View) : BaseAdapter.BaseViewHolder(view) {

        init {
            try {
                pop_up_button.getClick {
                    showPopUp(it, list[adapterPosition])
                }
            }catch(e: Exception){}
        }

        override fun bind(pos: Int) {
            card_number_text_view.text = list[pos].cardNumber
            setCardImage(list[pos].cardNumber)
        }

        private fun setCardImage(number: String){
            val cardImageLink = CreditCardUtils.getCardImageLink(number)
            card_type_image_view.loadImage(cardImageLink)
        }
    }

    fun deleteItem(itemId: Int){
        var indexForRemove = -1
        list.forEachIndexed { index, item ->
            if (item.id == itemId){
                indexForRemove = index
                return@forEachIndexed
            }
        }
        if (indexForRemove >= 0) {
            list.removeAt(indexForRemove)
            notifyItemRemoved(indexForRemove)
        }
    }

    private fun showPopUp(v: View, item: MyCardsItem){
        val popup = PopupMenu(v.context, v, Gravity.END)
        popup.menu.add(0, deletePopUpId, 0, v.context.getString(R.string.card_delete))
        popup.menu.add(0, editPopUpId, 0, v.context.getString(R.string.edit_delete))
        popup.setOnMenuItemClickListener {
            when(it.itemId){
                deletePopUpId -> onDelete(item)
                editPopUpId -> onEdit(item)
            }
            true
        }
        popup.show()
    }
}