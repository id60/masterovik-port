package noty_team.com.masterovik.ui.fragments.profile_setting.edit_personal_data

import android.arch.lifecycle.ViewModelProvider
import android.support.v7.widget.PopupMenu
import android.view.Gravity
import android.view.View
import kotlinx.android.synthetic.main.fragment_edit_personal_data.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.api.request.edit_profile.EditProfileRequest
import noty_team.com.masterovik.api.response.city.cities_list.ResponseItem
import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.ui.activities.main.UserProfileChangesViewModel
import noty_team.com.masterovik.utils.*
import noty_team.com.masterovik.utils.enums.UserRole
import noty_team.com.masterovik.utils.paper.PaperIO

class FragmentEditPersonalData : BaseFragment<ProfileFragmentViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): ProfileFragmentViewModel {
        return injectViewModel(viewModelFactory)
    }

    companion object {
        fun newInstance() = FragmentEditPersonalData()
    }

    override fun layout() = R.layout.fragment_edit_personal_data

    private lateinit var userProfileChangesViewModel: UserProfileChangesViewModel
    var citiesList = ArrayList<ResponseItem>()
    var cityId = -1

    override fun initialization(view: View, isFirstInit: Boolean) {
        initToolbar()

        if (isFirstInit) {
            userProfileChangesViewModel = baseActivity.injectViewModel(viewModelFactory)

            name_title_text_view.text = getString(R.string.name).fromHtml()
            last_name_title_text_view.text = getString(R.string.surname).fromHtml()
            city_title_text_view.text = getString(R.string.city).fromHtml()

            setUpWithRole()
            setupClicks()
            listenUpdates()

            setUpProfileData()
            getCities()
        }
    }

    private fun initToolbar(){
        baseActivity.getActionBarView()?.setupCenterText("Личные данные")
        baseActivity.getActionBarView()?.showLeftButton(true)
        baseActivity.getActionBarView()?.setBackButton(layoutInflater)?.getClick {
            onBackPressed()
        }
    }

    private fun getCities(){
        showProgress(true)
        viewModel.getCities()
    }

    private fun listenUpdates(){
        viewModel.citiesData.observe(this) { response ->
            showProgress(false)
            when(response){
                is Success -> {
                    response.value.response?.let {
                        citiesList = it
                        setUpCity()
                        userProfileChangesViewModel.getProfile()
                    }
                }
                is Failure -> {
                    showErrorSnack(response.errorMessage)
                }
            }
        }

        viewModel.editProfileData.observe(this) { response ->
            showProgress(false)
            when(response){
                is Success -> {
                    response.value.message?.let {
                        showSuccessSnack(it)
                        updateProfileData()
                        userProfileChangesViewModel.getProfile()
                    }
                }
                is Failure -> {
                    showErrorSnack(response.errorMessage)
                }
            }
        }
    }

    private fun updateProfileData(){
        val profileData = PaperIO.getUserProfile()
        profileData.firstName = name_edit_text.text.toString().trim()
        profileData.lastName = last_name_edit_text.text.toString().trim()
        profileData.cityId = cityId
        if (PaperIO.getRole() == UserRole.EXECUTOR) {
            profileData.companyName = company_name_edit_text.text.toString().trim()
            profileData.companyDescription = description_edit_text.text.toString().trim()
        }
    }

    private fun setUpWithRole(){
        val role = PaperIO.getRole()
        when (role){
            UserRole.EXECUTOR -> {
                ll_company_info.visible()
            }
            UserRole.EMPLOYER -> {
                ll_company_info.gone()
            }
        }
    }

    private fun setUpCity() {
        val cityId = PaperIO.getUserProfile().cityId
        citiesList.filter { it.id == cityId }.let {
            if (it.isNotEmpty()){
                city_edit_text.setText(it[0].city)
            }
        }
    }

    private fun setUpProfileData(){
        val profileData = PaperIO.getUserProfile()
        name_edit_text.setText(profileData.firstName)
        last_name_edit_text.setText(profileData.lastName)
        cityId = profileData.cityId
        company_name_edit_text.setText(profileData.companyName)
        description_edit_text.setText(profileData.companyDescription)
    }

    private fun setupClicks() {
        city_edit_text.getClick {
            showCitiesPopUp(it)
        }

        button_save.getClick {
            if (validate()) {
                val role = PaperIO.getRole()
                val request = EditProfileRequest(
                        accessToken = PaperIO.getAccessToken(),
                        firstName = name_edit_text.text.toString().trim(),
                        lastName = last_name_edit_text.text.toString().trim(),
                        cityId = cityId
//                        roleId = role.id
                )
                if (role == UserRole.EXECUTOR) {
                    company_name_edit_text.text.trim().apply {
                        if (isNotEmpty()) request.companyName = toString()
                    }
                    description_edit_text.text.trim().apply {
                        if (isNotEmpty()) request.companyDescription = toString()
                    }
                }

                showProgress(true)
                viewModel.editProfile(request)
            }
        }
    }

    private fun showCitiesPopUp(v: View){
        val popup = PopupMenu(baseActivity, v, Gravity.END)
        citiesList.forEachIndexed { index, item ->
            popup.menu.add(0, item.id, 0, item.city)
        }
        popup.setOnMenuItemClickListener {
            cityId = it.itemId

            val selectedCities = citiesList.filter { it.id == cityId}
            if (selectedCities.isNotEmpty()){
                city_edit_text.setText(selectedCities[0].city)
            }

            true
        }
        popup.show()
    }

    private fun validate(): Boolean {
        name_edit_text.setError(false)
        last_name_edit_text.setError(false)
        city_edit_text.setError(false)

        val name = name_edit_text.text.toString().trim()
        if (!name_edit_text.validate(
                        name.isNotEmpty(),
                        viewForShake = name_edit_text,
                        viewForScrollingTo = name_title_text_view,
                        container = scroll_layout)
        ) return false

        val lastName = last_name_edit_text.text.toString().trim()
        if (!last_name_edit_text.validate(
                        lastName.isNotEmpty(),
                        viewForShake = last_name_edit_text,
                        viewForScrollingTo = last_name_title_text_view,
                        container = scroll_layout)
        ) return false

        val city = city_edit_text.text.toString().trim()
        if (!city_edit_text.validate(
                        city.isNotEmpty(),
                        viewForShake = city_layout,
                        viewForScrollingTo = city_title_text_view,
                        container = scroll_layout)
        ) return false

        return true
    }
}