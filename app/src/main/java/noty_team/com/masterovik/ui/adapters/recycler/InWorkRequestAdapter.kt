package noty_team.com.masterovik.ui.adapters.recycler

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import noty_team.com.masterovik.R

import noty_team.com.masterovik.ui.adapters.items_adapter.InWorkRequestItem
import noty_team.com.masterovik.ui.fragments.request.in_work.request_in_detail.FragmentInDetail

class InWorkRequestAdapter(val items: ArrayList<InWorkRequestItem>,
                           val onClick:()->Unit) :
        RecyclerView.Adapter<InWorkRequestAdapter.ViewHolder>(), View.OnClickListener {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_in_work_request, parent, false))
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder?.nameOrder.text = items.get(position).nameOrder
        holder?.dateOrder.text = items.get(position).dateOrder
        holder?.price.text = items.get(position).price.toString() + " руб."
        holder?.description.text = items.get(position).description

        holder?.openRequestOrder.setOnClickListener(this)


    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.open_request_order -> {
                onClick()
            }
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var nameOrder = view.findViewById<TextView>(R.id.name_request_in_work)
        var dateOrder = view.findViewById<TextView>(R.id.date_order_request_in_work)
        var price = view.findViewById<TextView>(R.id.price_request_in_work)
        var description = view.findViewById<TextView>(R.id.description_request_in_work)

        var openRequestOrder = view.findViewById<View>(R.id.open_request_order)

    }
}