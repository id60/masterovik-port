package noty_team.com.masterovik.ui.activities.splash

import android.arch.lifecycle.ViewModelProvider
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import noty_team.com.masterovik.R
import noty_team.com.masterovik.api.request.auth.refresh_token.RefreshTokenRequest
import noty_team.com.masterovik.api.request.profile.ProfileRequest
import noty_team.com.masterovik.ui.activities.login.LoginActivity
import noty_team.com.masterovik.base.base_action_bar.ActionBarContract
import noty_team.com.masterovik.base.BaseActivity
import noty_team.com.masterovik.ui.activities.main.MainActivity
import noty_team.com.masterovik.ui.activities.main.UserProfileChangesViewModel
import noty_team.com.masterovik.utils.Failure
import noty_team.com.masterovik.utils.Success
import noty_team.com.masterovik.utils.injectViewModel
import java.util.concurrent.TimeUnit
import noty_team.com.masterovik.utils.*
import noty_team.com.masterovik.utils.paper.PaperIO

class SplashActivity : BaseActivity<SplashActivityViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): SplashActivityViewModel {
        return injectViewModel(viewModelFactory)
    }

    override fun layout(): Int = R.layout.activity_splash

    private lateinit var userProfileChangesViewModel: UserProfileChangesViewModel

    override fun initialization() {
        userProfileChangesViewModel = injectViewModel(viewModelFactory)

        startWaitTimer()
        listenUpdates()

        viewModel.refreshToken(RefreshTokenRequest(PaperIO.getRefreshToken()))
    }

    private fun listenUpdates(){

        viewModel.refreshTokenData.observe(this) {
            when(it){
                is Success -> {
                    it.value.accessToken?.apply {
                        PaperIO.setAccessToken(this)
                        userProfileChangesViewModel.getProfile()
                    }
                }
                is Failure -> {
                    LoginActivity.start(this)
                }
            }
        }

        userProfileChangesViewModel.profileData.observe(this) {
            when(it){
                is Success -> {
                    it.value.apply {
                        PaperIO.setUserProfile(this)
                        MainActivity.start(this@SplashActivity)
                    }
                }
                is Failure -> {
                    LoginActivity.start(this)
                }
            }
        }
    }


    private fun startWaitTimer() {
        compositeDisposable.add(Observable
                .interval(2000, TimeUnit.MILLISECONDS)
                .repeat()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({}, {}, {}))
    }

//    private fun openActivity(data: DataModel<RefreshTokenResponse>?) {
//        if (data?.isSuccess == true && data.dataModel?.accessToken?.isNotEmpty() == true){
//            MainActivity.start(this)
//        }
//        else {
//            LoginActivity.start(this)
//        }
//    }

    override fun getActionBarView(): ActionBarContract.View? = null

}