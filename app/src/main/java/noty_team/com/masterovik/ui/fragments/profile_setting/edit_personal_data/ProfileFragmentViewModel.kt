package noty_team.com.masterovik.ui.fragments.profile_setting.edit_personal_data

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import noty_team.com.masterovik.api.request.edit_profile.EditProfileRequest
import noty_team.com.masterovik.api.response.city.cities_list.CitiesResponse
import noty_team.com.masterovik.api.response.edit_profile.EditProfileResponse
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.utils.Result
import javax.inject.Inject

class ProfileFragmentViewModel @Inject constructor(app: Application) : BaseViewModel(app) {

    var citiesData = MutableLiveData<Result<CitiesResponse>>()
    var editProfileData = MutableLiveData<Result<EditProfileResponse>>()

    fun getCities(){
        api.cityApi.getCities { citiesData.postValue(it) }.call()
    }

    fun editProfile(data: EditProfileRequest){
        api.profileApi.editProfile(data) { editProfileData.postValue(it) }.call()
    }

    override fun onClear() {
        citiesData.value = null
        editProfileData.value = null
    }
}