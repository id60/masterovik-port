package noty_team.com.masterovik.ui.fragments.request.in_work

import android.arch.lifecycle.ViewModelProvider
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.fragment_in_work_request.*
import noty_team.com.masterovik.R

import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.ui.adapters.items_adapter.InWorkRequestItem
import noty_team.com.masterovik.ui.adapters.recycler.InWorkRequestAdapter
import noty_team.com.masterovik.ui.fragments.request.in_work.request_in_detail.FragmentInDetail
import noty_team.com.masterovik.utils.injectViewModel

class FragmentInWorkRequest : BaseFragment<BaseViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): BaseViewModel {
        return injectViewModel(viewModelFactory)
    }

    override fun layout() = R.layout.fragment_in_work_request

    override fun initialization(view: View, isFirstInit: Boolean) {

        recycler_in_work_request.layoutManager = LinearLayoutManager(baseActivity)
        recycler_in_work_request.adapter = InWorkRequestAdapter(createList()){
            fragmentManager.addFragment(FragmentInDetail.newInstance())
        }

    }

    fun createList(): ArrayList<InWorkRequestItem> {
        val list: ArrayList<InWorkRequestItem> = ArrayList()
        val item = InWorkRequestItem("Название объявления", "23 февраля 2019", 20000, getString(R.string.some_text))

        list.add(item)
        list.add(item)
        list.add(item)
        list.add(item)

        return list
    }

}