package noty_team.com.masterovik.ui.adapters.view_pager

import android.support.v4.app.FragmentManager
import noty_team.com.masterovik.R

import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.utils.adapters.CustomViewPager
import noty_team.com.masterovik.utils.getStringArray
import java.lang.Exception

class RequestPageAdapter(fm: FragmentManager,
                         pager: CustomViewPager,
                         fragments: ArrayList<BaseFragment<*>> = arrayListOf()) : BasePagerAdapter(fm, pager, fragments) {


    override fun getTitle(pageIndex: Int): String {
        var title = ""
        try {
            title = getStringArray(R.array.request_title, context)[pageIndex]
        } catch (e: Exception) {
        }
        return title
    }
}