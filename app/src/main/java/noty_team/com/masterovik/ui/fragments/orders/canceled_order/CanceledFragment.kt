package noty_team.com.masterovik.ui.fragments.orders.canceled_order

import android.arch.lifecycle.ViewModelProvider
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.fragment_canceled.*
import noty_team.com.masterovik.R

import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.ui.adapters.items_adapter.CanceledOrderItem
import noty_team.com.masterovik.ui.adapters.recycler.CanceledOrderAdapter
import noty_team.com.masterovik.utils.injectViewModel

class CanceledFragment : BaseFragment<BaseViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): BaseViewModel {
        return injectViewModel(viewModelFactory)
    }

    companion object {
        fun newInstance(): CanceledFragment {
            return CanceledFragment()
        }
    }

    override fun layout() = R.layout.fragment_canceled

    override fun initialization(view: View, isFirstInit: Boolean) {

        recycler_canceled_order.layoutManager = LinearLayoutManager(baseActivity)
        recycler_canceled_order.adapter = CanceledOrderAdapter(createList()){

        }
    }

    private fun createList(): ArrayList<CanceledOrderItem> {
        val list: ArrayList<CanceledOrderItem> = ArrayList()
        val item1 = CanceledOrderItem("Название Заказа", "Имя Фамилия", 15, "Отменено заказчиком")
        val item2 = CanceledOrderItem("Название Заказа", "Имя Фамилия", 22, "Отменено вами")

        list.add(item1)
        list.add(item2)
        return list
    }

}