package noty_team.com.masterovik.ui.fragments.login.selection_role

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.utils.enums.UserRole
import javax.inject.Inject

class SelectionRoleFragmentViewModel @Inject constructor(app: Application) : BaseViewModel(app) {

    val roleData = MutableLiveData<UserRole>()

    fun setRole(role: UserRole){
        roleData.postValue(role)
    }

    override fun onClear() {

    }
}