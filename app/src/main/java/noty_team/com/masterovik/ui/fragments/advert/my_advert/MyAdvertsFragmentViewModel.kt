package noty_team.com.masterovik.ui.fragments.advert.my_advert

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import noty_team.com.masterovik.api.request.services.AddServiceRequest
import noty_team.com.masterovik.api.response.BaseResponse
import noty_team.com.masterovik.api.response.adverts.my_adverts.MyAdvertsResponse
import noty_team.com.masterovik.api.response.services.get_services.GetServicesResponse
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.utils.Failure
import noty_team.com.masterovik.utils.Result
import noty_team.com.masterovik.utils.Success
import javax.inject.Inject

class MyAdvertsFragmentViewModel @Inject constructor(app: Application) : BaseViewModel(app) {

    val myAdvertsData = MutableLiveData<Result<MyAdvertsResponse>>()

    fun getAdverts(){
        api.advertsApi.getMyAdverts {
            myAdvertsData.postValue(it)
        }.call()
    }

    override fun onClear() {
        myAdvertsData.value = null
    }
}