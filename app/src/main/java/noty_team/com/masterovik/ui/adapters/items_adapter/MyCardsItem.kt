package noty_team.com.masterovik.ui.adapters.items_adapter

data class MyCardsItem (var id: Int = -1,
                        var cardNumber: String = "",
                        var isChecked: Boolean = false)