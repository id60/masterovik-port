package noty_team.com.masterovik.ui.fragments.faq.faq

import android.arch.lifecycle.ViewModelProvider
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.fragment_faq.*
import noty_team.com.masterovik.R

import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.ui.adapters.recycler.FAQAdapter
import noty_team.com.masterovik.ui.adapters.items_adapter.FAQItem
import noty_team.com.masterovik.utils.injectViewModel

class FragmentFAQ : BaseFragment<BaseViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): BaseViewModel {
        return injectViewModel(viewModelFactory)
    }

    companion object {
        fun newInstance(): FragmentFAQ {
            return FragmentFAQ()
        }
    }

    override fun layout() = R.layout.fragment_faq

    override fun initialization(view: View, isFirstInit: Boolean) {

        recycler_faq.layoutManager = LinearLayoutManager(baseActivity)
        recycler_faq.adapter = FAQAdapter(createList(), baseActivity)

    }

    private fun createList(): ArrayList<FAQItem> {
        var list: ArrayList<FAQItem> =  ArrayList()
        var item = FAQItem(getString(R.string.item_faq_title), getString(R.string.recycler_text_faq))

        list.add(item)
        list.add(item)

        return list
    }

}