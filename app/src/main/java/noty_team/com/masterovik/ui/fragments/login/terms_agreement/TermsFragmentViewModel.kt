package noty_team.com.masterovik.ui.fragments.login.terms_agreement

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import noty_team.com.masterovik.api.response.profile.ProfileResponse
import noty_team.com.masterovik.api.request.auth.register.RegistrationRequest
import noty_team.com.masterovik.api.request.profile.ProfileRequest
import noty_team.com.masterovik.api.response.auth.registration.RegistrationResponse
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.utils.Result
import javax.inject.Inject

class TermsFragmentViewModel @Inject constructor(app: Application) : BaseViewModel(app) {

    val registerData = MutableLiveData<Result<RegistrationResponse>>()

    fun register(request: RegistrationRequest){
        api.loginApi.register(request) {
            registerData.postValue(it)
        }.call()
    }

    override fun onClear() {
        registerData.value = null
    }
}