package noty_team.com.masterovik.ui.fragments.orders.current_order.order

import android.arch.lifecycle.ViewModelProvider
import android.view.View
import kotlinx.android.synthetic.main.fragment_order_item.*
import noty_team.com.masterovik.R

import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.ui.fragments.orders.complaint.FragmentComplaint
import noty_team.com.masterovik.ui.fragments.orders.current_order.order.change_amount_payment.FragmentChangeAmountPayment
import noty_team.com.masterovik.ui.fragments.orders.current_order.order.change_date_order.FragmentChangeDateOrder
import noty_team.com.masterovik.ui.fragments.orders.profile_order.FragmentProfileOrder
import noty_team.com.masterovik.utils.injectViewModel

class FragmentOrderItem : BaseFragment<BaseViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): BaseViewModel {
        return injectViewModel(viewModelFactory)
    }

    companion object {
        fun newInstance() = FragmentOrderItem()
    }

    override fun layout() = R.layout.fragment_order_item

    override fun initialization(view: View, isFirstInit: Boolean) {

        toolbarSizeListener.hideEmptyToolbarSpace(true)
        baseActivity.getActionBarView()?.showActionBar(false)
        toolbarSizeListener.resizeContainerSmall()

        cross_current_item.getClick {
            onBackPressed()
        }

        leave_complaint_current.getClick {
            fragmentManager.addFragment(FragmentComplaint.newInstance())
        }

        change_date_order.getClick {
            fragmentManager.addFragment(FragmentChangeDateOrder.newInstance())
        }

        change_amount_payment.getClick {
            fragmentManager.addFragment(FragmentChangeAmountPayment.newInstance())
        }

        user_name.getClick {
            fragmentManager.addFragment(FragmentProfileOrder.newInstance())
        }

    }
}