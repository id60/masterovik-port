package noty_team.com.masterovik.ui.fragments.wallet.add_card

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import noty_team.com.masterovik.api.request.services.AddServiceRequest
import noty_team.com.masterovik.api.request.wallet.add_card.AddCardRequest
import noty_team.com.masterovik.api.request.wallet.edit_card.EditCardRequest
import noty_team.com.masterovik.api.response.BaseResponse
import noty_team.com.masterovik.api.response.services.get_services.GetServicesResponse
import noty_team.com.masterovik.api.response.wallet.add_card.AddCardResponse
import noty_team.com.masterovik.api.response.wallet.edit_card.EditCardResponse
import noty_team.com.masterovik.api.response.wallet.my_cards.MyCardsResponse
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.utils.Failure
import noty_team.com.masterovik.utils.Result
import noty_team.com.masterovik.utils.Success
import javax.inject.Inject

class AddCardFragmentViewModel @Inject constructor(app: Application) : BaseViewModel(app) {

    val addCardData = MutableLiveData<Result<AddCardResponse>>()
    val editCardData = MutableLiveData<Result<EditCardResponse>>()

    fun addCard(data: AddCardRequest){
        api.walletApi.addCard(data) {
            addCardData.postValue(it)
        }.call()
    }

    fun editCard(data: EditCardRequest){
        api.walletApi.editCard(data) {
            editCardData.postValue(it)
        }.call()
    }

    override fun onClear() {
        addCardData.value = null
        editCardData.value = null
    }
}