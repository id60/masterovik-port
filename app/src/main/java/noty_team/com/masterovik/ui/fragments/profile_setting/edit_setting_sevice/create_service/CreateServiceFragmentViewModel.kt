package noty_team.com.masterovik.ui.fragments.profile_setting.edit_setting_sevice.create_service

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import noty_team.com.masterovik.api.request.change_phone.ChangePhoneRequest
import noty_team.com.masterovik.api.request.create_service.CreateServiceRequest
import noty_team.com.masterovik.api.request.services.AddServiceRequest
import noty_team.com.masterovik.api.response.BaseResponse
import noty_team.com.masterovik.api.response.change_phone.ChangePhoneResponse
import noty_team.com.masterovik.api.response.city.cities_list.CitiesResponse
import noty_team.com.masterovik.api.response.services.create_service.CreateServiceResponse
import noty_team.com.masterovik.api.response.services.get_service.GetServiceResponse
import noty_team.com.masterovik.api.response.services.get_services.GetServicesResponse
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.ui.fragments.login.entrance.CountDownTimerLiveData
import noty_team.com.masterovik.utils.Failure
import noty_team.com.masterovik.utils.Result
import noty_team.com.masterovik.utils.Success
import javax.inject.Inject

class CreateServiceFragmentViewModel @Inject constructor(app: Application) : BaseViewModel(app) {

    val createServiceData = MutableLiveData<Result<CreateServiceResponse>>()

    fun createService(data: CreateServiceRequest){
        api.servicesApi.createService(data) {
            createServiceData.postValue(it)
        }.call()
    }
    override fun onClear() {
        createServiceData.value = null
    }
}