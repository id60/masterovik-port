package noty_team.com.masterovik.ui.fragments.profile_setting.edit_email

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import noty_team.com.masterovik.api.request.change_email.ChangeEmailRequest
import noty_team.com.masterovik.api.request.change_phone.ChangePhoneRequest
import noty_team.com.masterovik.api.response.change_email.ChangeEmailResponse
import noty_team.com.masterovik.api.response.change_phone.ChangePhoneResponse
import noty_team.com.masterovik.api.response.city.cities_list.CitiesResponse
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.ui.fragments.login.entrance.CountDownTimerLiveData
import noty_team.com.masterovik.utils.Result
import javax.inject.Inject

class EditEmailFragmentViewModel @Inject constructor(app: Application) : BaseViewModel(app) {

    val changeEmailData = MutableLiveData<Result<ChangeEmailResponse>>()

    fun changeEmailData(data: ChangeEmailRequest){
        api.profileApi.changeEmail(data) {
            changeEmailData.postValue(it)
        }.call()
    }

    override fun onClear() {
        changeEmailData.value = null
    }
}