package noty_team.com.masterovik.ui.fragments.advert.filter_advert.price_filter

import android.arch.lifecycle.ViewModelProvider
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import kotlinx.android.synthetic.main.fragment_filter_price.*
import noty_team.com.masterovik.R

import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.utils.injectViewModel
import noty_team.com.masterovik.utils.setError
import noty_team.com.masterovik.utils.validate
import java.lang.NumberFormatException

class PriceFilterFragment : BaseFragment<BaseViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): BaseViewModel {
        return injectViewModel(viewModelFactory)
    }

    companion object {
        private const val PRICES_KEY = "PRICES_KEY"
        fun newInstance(prices: ArrayList<Int> = arrayListOf()): PriceFilterFragment = PriceFilterFragment().apply {
            arguments = Bundle().apply {
                putIntArray(PRICES_KEY, prices.toIntArray())
            }
        }
        fun getPrices(arguments: Bundle?): ArrayList<Int> =
                arguments?.getIntArray(PRICES_KEY)?.toCollection(arrayListOf())?: arrayListOf()
    }

    override fun layout() = R.layout.fragment_filter_price

    override fun initialization(view: View, isFirstInit: Boolean) {
        initToolbar()
        if (isFirstInit) {
            initPrices()
            initClicks()
        }
    }

    private fun initPrices(){
        getPrices(arguments)?.let {
            if (it.size >= 2){
                price_from_edit_text.setText(it[0].toString())
                price_to_edit_text.setText(it[1].toString())
            }
        }
    }

    private fun initClicks() {
        button_save.getClick {
            if (validate()){
                val priceFrom = price_from_edit_text.text.toString().trim()
                val priceTo = price_to_edit_text.text.toString().trim()
                sendBackResult(arrayListOf(priceFrom.toInt(), priceTo.toInt()))
                onBackPressed()
            }
        }
    }

    private fun validate(): Boolean {
        price_from_edit_text.setError(false)
        price_to_edit_text.setError(false)

        val priceFrom = price_from_edit_text.text.toString().trim()
        price_from_edit_text.validate(
                priceFrom.isNotEmpty() && TextUtils.isDigitsOnly(priceFrom),
                viewForShake = price_from_edit_text).let { if (!it) return it }

        val priceTo = price_to_edit_text.text.toString().trim()
        price_to_edit_text.validate(
                priceTo.isNotEmpty() && TextUtils.isDigitsOnly(priceTo),
                viewForShake = price_to_edit_text).let { if (!it) return it }

        try {
            if (priceFrom.toInt() > priceTo.toInt()) {
                showErrorSnack("Начальная цена не должна быть больше конечной")
                return false
            }
        }catch (e: NumberFormatException){
            showErrorSnack("Поля не должны быть пустыми")
            return false
        }

        return true
    }

    private fun initToolbar() {
        toolbarSizeListener.hideEmptyToolbarSpace(true)
        baseActivity.getActionBarView()?.showActionBar(false)

        back_button.getClick {
            onBackPressed()
        }
    }
}