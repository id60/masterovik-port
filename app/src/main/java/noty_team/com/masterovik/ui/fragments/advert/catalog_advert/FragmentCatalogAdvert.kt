package noty_team.com.masterovik.ui.fragments.advert.catalog_advert

import android.arch.lifecycle.ViewModelProvider
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.fragment_catalog_advert.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.api.request.advert.get_catalog_adverts.CatalogAdvertsRequest

import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.ui.adapters.items_adapter.CatalogAdvertItem
import noty_team.com.masterovik.ui.adapters.items_adapter.CityItem
import noty_team.com.masterovik.ui.adapters.items_adapter.ProvideServiceItem
import noty_team.com.masterovik.ui.adapters.recycler.AdvertAdapter
import noty_team.com.masterovik.ui.fragments.advert.advert_instance.FragmentInstanceAdvert
import noty_team.com.masterovik.ui.fragments.advert.filter_advert.AdvertFilterFragmentViewModel
import noty_team.com.masterovik.ui.fragments.cities_list.CitiesListFragment
import noty_team.com.masterovik.ui.fragments.profile_setting.edit_setting_sevice.EditServiceFragmentViewModel
import noty_team.com.masterovik.ui.fragments.profile_setting.edit_setting_sevice.services_list.ServicesListFragment
import noty_team.com.masterovik.utils.*

class FragmentCatalogAdvert : BaseFragment<CatalogAdvertsFragmentViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): CatalogAdvertsFragmentViewModel {
        return injectViewModel(viewModelFactory)
    }

    override fun layout() = R.layout.fragment_catalog_advert

    private lateinit var advertAdapter: AdvertAdapter
    private lateinit var editServiceViewModel: EditServiceFragmentViewModel
    private lateinit var advertFilterFragmentViewModel: AdvertFilterFragmentViewModel

    private var cityItem: CityItem? = null
    private var servicesList = arrayListOf<ProvideServiceItem>()

    override fun initialization(view: View, isFirstInit: Boolean) {
        initToolbar()

        if (isFirstInit) {
            editServiceViewModel = injectViewModel(viewModelFactory)
            advertFilterFragmentViewModel = baseActivity.injectViewModel(viewModelFactory)

            initAdvertAdapter()
            listenUpdates()

            getServices()
        }
    }

    private fun initClicks() {
        city_layout.getClick {
            fragmentManager.addFragment(CitiesListFragment.newInstance(cityItem?.id ?: -1))
        }

        service_layout.getClick {
            fragmentManager.addFragment(ServicesListFragment.newInstance(
                    servicesList.flatMap { arrayListOf(it.id) } as ArrayList<Int>, false))
        }
    }

    private fun getServices() {
        showProgress(true)
        editServiceViewModel.getServices()
    }

    private fun getMyAdverts(request: CatalogAdvertsRequest) {
        showProgress(true)
        viewModel.getCatalogAdverts(request)
    }

    private fun listenUpdates() {
        viewModel.catalogAdvertsData.observe(this) { response ->
            showProgress(false)
            when (response) {
                is Success -> {
                    advertAdapter.clear()
                    advertAdapter.addAll(response.value.map())
                }
                is Failure -> {
                    showErrorSnack(response.errorMessage)
                }
            }
            checkEmptyList()
        }

        editServiceViewModel.userServicesData.observe(this) { response ->
            showProgress(false)
            when (response) {
                is Success -> {
                    servicesList = response.value.map()
                    val serviceIds = servicesList.flatMap { arrayListOf(it.id) } as ArrayList<Int>
                    getMyAdverts(CatalogAdvertsRequest(
                            servicesId = serviceIds
                    ))

                    initClicks()
                }
                is Failure -> {
                    showErrorSnack(response.errorMessage)
                }
            }
        }

        advertFilterFragmentViewModel.advertSortingData.observe(this) {
            initFilters(it)
            getMyAdverts(it)
        }

        subscribeForResult {
            if (viewPagerListener.isCurrentPageSelected()) {
                when (it.classProvider) {
                    CitiesListFragment::class -> {
                        if (it.value is CityItem) {
                            cityItem = it.value
                            initCityFilter()

                            val sortingData = advertFilterFragmentViewModel.getSortingData()
                            sortingData.cityId = cityItem?.id
                            sortingData.cityItem = cityItem
                            advertFilterFragmentViewModel.setSortingData(sortingData)

//                            getMyAdverts(CatalogAdvertsRequest( cityId = cityItem?.id ))
                        }
                    }
                    ServicesListFragment::class -> {
                        if (it.value is ArrayList<*> && it.value.isNotEmpty()
                                && it.value[0] is ProvideServiceItem) {
                            servicesList = it.value as ArrayList<ProvideServiceItem>
                            val servicesId = servicesList.flatMap { service ->
                                arrayListOf(service.id)
                            } as ArrayList<Int>
                            initServicesFilter()

                            val sortingData = advertFilterFragmentViewModel.getSortingData()
                            sortingData.servicesId = servicesId
                            sortingData.servicesList = servicesList
                            advertFilterFragmentViewModel.setSortingData(sortingData)

//                            getMyAdverts(CatalogAdvertsRequest( servicesId = servicesId ))
                        }
                    }
                }
            }
        }
    }

    private fun checkEmptyList() {
        if (advertAdapter.itemCount > 0) empty_list_text_view.gone()
        else empty_list_text_view.visible()
    }

    private fun initFilters(request: CatalogAdvertsRequest) {
        cityItem = request.cityItem
        servicesList = request.servicesList ?: arrayListOf()
        initCityFilter()
        initServicesFilter()
    }

    private fun initCityFilter() {
        if (cityItem?.cityName?.isNotEmpty() == true)
            city_text_view.text = "(${cityItem?.cityName})"
        else
            city_text_view.text = ""
    }

    private fun initServicesFilter() {
        if (servicesList.isNotEmpty())
            service_text_view.text = "(${servicesList.joinToString { it.serviceName }})"
        else
            service_text_view.text = ""
    }

    private fun initAdvertAdapter() {
        recycler_catalog_advert.layoutManager = LinearLayoutManager(baseActivity)
        advertAdapter = AdvertAdapter(arrayListOf()) {
            fragmentManager.addFragment(FragmentInstanceAdvert.newInstance(it))
        }
        recycler_catalog_advert.adapter = advertAdapter
    }

    private fun initToolbar() {

    }
}