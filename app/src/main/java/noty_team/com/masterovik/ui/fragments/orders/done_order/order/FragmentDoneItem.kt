package noty_team.com.masterovik.ui.fragments.orders.done_order.order

import android.arch.lifecycle.ViewModelProvider
import android.view.View
import kotlinx.android.synthetic.main.fragment_done_item.*
import noty_team.com.masterovik.R

import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.ui.fragments.orders.complaint.FragmentComplaint
import noty_team.com.masterovik.utils.injectViewModel

class FragmentDoneItem : BaseFragment<BaseViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): BaseViewModel {
        return injectViewModel(viewModelFactory)
    }
    companion object {
        fun newInstance() = FragmentDoneItem()
    }

    override fun layout() = R.layout.fragment_done_item

    override fun initialization(view: View, isFirstInit: Boolean) {

        toolbarSizeListener.hideEmptyToolbarSpace(true)
        baseActivity.getActionBarView()?.showActionBar(false)
//        toolbarSizeListener.resizeContainerSmall()

        cross_done_item.setOnClickListener {
            onBackPressed()
        }

        leave_complaint_done.setOnClickListener {
            fragmentManager.addFragment(FragmentComplaint.newInstance())
        }

    }
}