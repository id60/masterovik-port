package noty_team.com.masterovik.ui.fragments.advert

import android.arch.lifecycle.ViewModelProvider
import android.support.design.widget.TabLayout
import android.view.LayoutInflater
import android.view.View
import kotlinx.android.synthetic.main.fragment_base_advert.*
import noty_team.com.masterovik.R

import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.ui.adapters.view_pager.AdvertPageAdapter
import noty_team.com.masterovik.ui.fragments.advert.catalog_advert.FragmentCatalogAdvert
import noty_team.com.masterovik.ui.fragments.advert.filter_advert.FragmentFilterAdvert
import noty_team.com.masterovik.ui.fragments.advert.my_advert.FragmentMyAdvert
import noty_team.com.masterovik.utils.injectViewModel
import noty_team.com.masterovik.utils.invisible
import noty_team.com.masterovik.utils.visible

class FragmentAdvert : BaseFragment<BaseViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): BaseViewModel {
        return injectViewModel(viewModelFactory)
    }

    companion object {
        fun newInstance(): FragmentAdvert {
            return FragmentAdvert()
        }
    }

    override fun layout() = R.layout.fragment_base_advert

    override fun initialization(view: View, isFirstInit: Boolean) {
        initToolbar()

        view_pager_advert.adapter = AdvertPageAdapter(childFragmentManager, view_pager_advert,
                arrayListOf(FragmentMyAdvert(), FragmentCatalogAdvert()))
        tabLayout_advert.setupWithViewPager(view_pager_advert)

        tabLayout_advert.addOnTabSelectedListener(object: TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                if (tab.position == 1) {
                    baseActivity.findViewById<View>(R.id.filter).visible()
                } else {
                    baseActivity.findViewById<View>(R.id.filter).invisible()
                }
            }
            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })
    }

    private fun initToolbar() {
        toolbarSizeListener.hideEmptyToolbarSpace(false)
        baseActivity.getActionBarView()?.showActionBar(true)
        baseActivity.getActionBarView()?.setSmallContainerWidth()
        baseActivity.getActionBarView()?.setLargeToolbarHeight()
        baseActivity.getActionBarView()?.setupCenterText("Объявления")
        baseActivity.getActionBarView()?.setupRightButton(LayoutInflater.from(baseActivity).inflate(R.layout.ab_right_filter, null))
        baseActivity.findViewById<View>(R.id.right_container).getClick {
        }
        baseActivity.getActionBarView()?.showLeftButton(true)
        baseActivity.getActionBarView()?.setDrawerMenu(layoutInflater)?.getClick {
            onDrawerPressedListener.onDrawerPressed()
        }

        toolbarSizeListener.resizeContainerLarge()

        baseActivity.findViewById<View>(R.id.filter).setOnClickListener {
            fragmentManager.addFragment(FragmentFilterAdvert.newInstance())
            toolbarSizeListener.resizeContainerSmall()
        }
        baseActivity.findViewById<View>(R.id.filter).invisible()
    }
}