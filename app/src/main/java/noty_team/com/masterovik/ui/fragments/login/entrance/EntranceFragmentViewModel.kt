package noty_team.com.masterovik.ui.fragments.login.entrance

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import noty_team.com.masterovik.api.request.auth.login.LoginRequest
import noty_team.com.masterovik.api.response.profile.ProfileResponse
import noty_team.com.masterovik.api.response.auth.login.LoginResponse
import noty_team.com.masterovik.api.request.profile.ProfileRequest
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.utils.Loading
import noty_team.com.masterovik.utils.Result
import javax.inject.Inject

class EntranceFragmentViewModel @Inject constructor(app: Application) : BaseViewModel(app) {

    val smsData = MutableLiveData<Result<LoginResponse>>()
    val loginData = MutableLiveData<Result<LoginResponse>>()
    val timerData = CountDownTimerLiveData(60000, 1000)

    fun login(request: LoginRequest){
        api.loginApi.login(request) {
            if (request.sms == null) smsData.postValue(it)
            else loginData.postValue(it)
        }.call()
    }

    override fun onClear() {
        smsData.value = null
        loginData.value = null
    }
}