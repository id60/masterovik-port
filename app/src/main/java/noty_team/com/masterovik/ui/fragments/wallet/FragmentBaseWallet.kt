package noty_team.com.masterovik.ui.fragments.wallet

import android.arch.lifecycle.ViewModelProvider
import android.support.design.widget.TabLayout
import android.view.LayoutInflater
import android.view.View
import kotlinx.android.synthetic.main.fragment_base_wallet.*
import noty_team.com.masterovik.R

import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.ui.adapters.view_pager.WalletPageAdapter
import noty_team.com.masterovik.ui.fragments.wallet.history.FragmentHistory
import noty_team.com.masterovik.ui.fragments.wallet.my_cards.FragmentMyCard
import noty_team.com.masterovik.ui.fragments.wallet.partners.FragmentPartners
import noty_team.com.masterovik.ui.fragments.wallet.sorting_history.FragmentSortingHistory
import noty_team.com.masterovik.utils.injectViewModel

class FragmentBaseWallet : BaseFragment<BaseViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): BaseViewModel {
        return injectViewModel(viewModelFactory)
    }

    companion object {
        fun newInstance(): FragmentBaseWallet {
            return FragmentBaseWallet()
        }
    }

    override fun layout() = R.layout.fragment_base_wallet

    override fun initialization(view: View, isFirstInit: Boolean) {
        initToolbar()

        if (isFirstInit) {
            initViewPager()
        }
    }

    private fun initViewPager(){
        view_pager_wallet.adapter = WalletPageAdapter(childFragmentManager, view_pager_wallet,
                arrayListOf(FragmentMyCard(), FragmentHistory(), FragmentPartners()))
        tabLayout_wallet.setupWithViewPager(view_pager_wallet)

        tabLayout_wallet.addOnTabSelectedListener(object :
                TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                if (tab.position == 1) {
                    baseActivity.getActionBarView()?.setupRightButton(
                            LayoutInflater.from(baseActivity).inflate(R.layout.ab_filter, null))
                    baseActivity.findViewById<View>(R.id.right_container).setOnClickListener {
                        fragmentManager.addFragment(FragmentSortingHistory.newInstance())
                    }
                } else {
                    baseActivity.getActionBarView()?.setupRightButton(
                            LayoutInflater.from(baseActivity).inflate(R.layout.ab_empty, null))
                    baseActivity.findViewById<View>(R.id.right_container).setOnClickListener {
                    }
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })
    }

    private fun initToolbar() {
        toolbarSizeListener.hideEmptyToolbarSpace(false)
        baseActivity.getActionBarView()?.showActionBar(true)
        baseActivity.getActionBarView()?.setLargeToolbarHeight()
        baseActivity.getActionBarView()?.setupCenterText("Платежные карты")
        baseActivity.getActionBarView()?.setSmallContainerWidth()
        baseActivity.getActionBarView()?.setupRightButton(LayoutInflater.from(baseActivity).inflate(R.layout.ab_empty, null))

        baseActivity.getActionBarView()?.showLeftButton(true)
        baseActivity.getActionBarView()?.setDrawerMenu(layoutInflater)?.getClick {
            onDrawerPressedListener.onDrawerPressed()
        }
    }
}