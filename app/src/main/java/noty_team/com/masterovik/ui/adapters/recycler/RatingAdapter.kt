package noty_team.com.masterovik.ui.adapters.recycler

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_rating.view.*
import noty_team.com.masterovik.R

import noty_team.com.masterovik.ui.adapters.items_adapter.RatingItem
import noty_team.com.masterovik.ui.fragments.rating.rating_review_detail.FragmentReviewDetail

class RatingAdapter(val items: ArrayList<RatingItem>,
                    val onClick:()->Unit) :
        RecyclerView.Adapter<RatingAdapter.ViewHolder>(), View.OnClickListener {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_rating, parent, false))
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder?.nameCustomer?.text = items.get(position).namecustomer
        holder?.date?.text = items.get(position).date
        holder?.rating?.text = "Рейтинг: " + items.get(position).rating.toString()


        holder?.ratingBar.rating = items.get(position).rating.toFloat()

        holder?.ratingbtn.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.rating_item -> {
                onClick()
                /*  baseActivity.navigator.navigateToFragment(FragmentOrderItem.newInstance(), null, true)



                  baseActivity.getActionBarView()?.showActionBar(false)
                  var lpconteriner = baseActivity.findViewById<View>(R.id.main_fragment_container).layoutParams as ConstraintLayout.LayoutParams
                  lpconteriner.setMargins(0, 0, 0, 0)
                  baseActivity.findViewById<View>(R.id.main_fragment_container).layoutParams = lpconteriner*/
            }
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var nameCustomer = view.name_customer_rating
        var date = view.date_rating
        var rating = view.rating
        var ratingBar = view.rating_bar

        var ratingbtn = view.rating_item

        //var feedbackItem = view.feedback_item

    }
}