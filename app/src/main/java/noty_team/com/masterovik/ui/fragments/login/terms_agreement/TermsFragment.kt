package noty_team.com.masterovik.ui.fragments.login.terms_agreement

import android.arch.lifecycle.ViewModelProvider
import android.view.View
import kotlinx.android.synthetic.main.fragment_terms_agreement.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.api.request.auth.register.RegistrationRequest
import noty_team.com.masterovik.api.request.profile.ProfileRequest
import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.ui.activities.login.LoginActivityViewModel
import noty_team.com.masterovik.ui.activities.main.MainActivity
import noty_team.com.masterovik.ui.activities.main.UserProfileChangesViewModel
import noty_team.com.masterovik.utils.Failure
import noty_team.com.masterovik.utils.Success
import noty_team.com.masterovik.utils.injectViewModel
import noty_team.com.masterovik.utils.observe
import noty_team.com.masterovik.utils.paper.PaperIO

class TermsFragment : BaseFragment<TermsFragmentViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): TermsFragmentViewModel {
        return injectViewModel(viewModelFactory)
    }

    companion object {
        fun newInstance(): TermsFragment {
            return TermsFragment()
        }
    }

    private lateinit var userProfileChangesViewModel: UserProfileChangesViewModel
    private lateinit var loginActivityViewModel: LoginActivityViewModel

    private var registrationRequest = RegistrationRequest()
    var phone = ""

    override fun layout() = R.layout.fragment_terms_agreement

    override fun initialization(view: View, isFirstInit: Boolean) {
        baseActivity.getActionBarView()?.setSmallContainerWidth()

        if (isFirstInit) {
            userProfileChangesViewModel = injectViewModel(viewModelFactory)
            loginActivityViewModel = baseActivity.injectViewModel(viewModelFactory)

            setupClicks()
            listenUpdates()
        }
    }

    private fun listenUpdates() {
        loginActivityViewModel.mediatorLiveData.observe(this) { request ->
            if (request.phone.isNotEmpty()) phone = request.phone
            else registrationRequest = request
        }

        viewModel.registerData.observe(this) { response ->
            when(response){
                is Success -> {
                    response.value.apply {
                        PaperIO.setAccessToken(accessToken)
                        PaperIO.setRefreshToken(refreshToken)

                        userProfileChangesViewModel.getProfile()
                    }
                }
                is Failure -> {
                    showProgress(false)
                    showErrorSnack(response.errorMessage)
                }
            }
        }

        userProfileChangesViewModel.profileData.observe(this) {
            showProgress(false)
            when(it){
                is Success -> {
                    it.value.apply {
                        PaperIO.setUserProfile(this)
                        MainActivity.start(baseActivity)
                    }
                }
                is Failure -> {
                    showErrorSnack(it.errorMessage)
                }
            }
        }
    }

    private fun setupClicks() {
        button_continue.getClick {
            if (phone.isNotEmpty()) {
                showProgress(true)
                registrationRequest.phone = phone
                viewModel.register(registrationRequest)
            }
            else showErrorSnack("Телефон не введен")
        }
    }
}
