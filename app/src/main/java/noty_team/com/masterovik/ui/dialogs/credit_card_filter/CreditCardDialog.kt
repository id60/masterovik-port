package noty_team.com.masterovik.ui.dialogs.credit_card_filter

import android.arch.lifecycle.ViewModelProvider
import android.text.TextUtils
import android.view.View
import android.widget.EditText
import com.github.vacxe.phonemask.PhoneMaskManager
import kotlinx.android.synthetic.main.dialog_set_card.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.SnackBarListener
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.ui.dialogs.BaseFragmentDialog
import noty_team.com.masterovik.utils.injectViewModel
import noty_team.com.masterovik.utils.setError
import noty_team.com.masterovik.utils.validate

class CreditCardDialog: BaseFragmentDialog<BaseViewModel>() {

    companion object {
        fun newInstance(snackBarListener: SnackBarListener) = CreditCardDialog().apply {
            this.snackBarListener = snackBarListener
        }
    }

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): BaseViewModel {
        return injectViewModel(viewModelFactory)
    }

    override fun layout() = R.layout.dialog_set_card

    val cardFormatter = PhoneMaskManager().withMask("#### #### #### ####")

    override fun initialization(view: View, isFirstInit: Boolean) {
        if (isFirstInit){
            cardFormatter.bindTo(card_number_edit_text as EditText)
            initClicks()
        }
    }

    private fun getCardNumber(): String {
        val number = cardFormatter.phone.toString()
        return number.replace("+", "")
    }

    private fun initClicks(){
        button_save.getClick {
            if (validateCardNumber()) {
                sendBackResult(getCardNumber())
                dismiss()
            }
            else snackBarListener.showErrorSnack("Неверные данные карты")
        }
    }

    private fun validateCardNumber(): Boolean {
        card_number_edit_text.setError(false)
        val cardNumber = getCardNumber()
        return card_number_edit_text.validate(
                validator.isCardNumberValid(cardNumber),
                viewForShake = card_number_edit_text
        )
    }
}