package noty_team.com.masterovik.ui.activities.main

import android.app.Application
import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.support.annotation.NonNull
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.ui.adapters.items_adapter.ProvideServiceItem
import noty_team.com.masterovik.utils.observe
import javax.inject.Inject
import kotlin.reflect.KClass

class BackResultViewModel @Inject constructor(app: Application) : BaseViewModel(app) {

    val returnData = MutableLiveData<BackResult<*, *>>()

    data class BackResult<out T: Any, C: Any>(val value: T, val classProvider: KClass<out C>){
        fun getDataClass() = value::class
    }

    fun observe(onResult:(result: BackResultViewModel.BackResult<*, *>)->Unit, @NonNull owner: LifecycleOwner){
        returnData.value = null
        returnData.observe(owner) {
            onResult(it)
        }
    }

    fun<T: Any, C: Any> addResult(data: T, classProvider: KClass<C>) {
        returnData.value = BackResult(data, classProvider)
    }

    override fun onClear() {
        returnData.value = null
    }
}