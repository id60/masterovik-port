package noty_team.com.masterovik.ui.activities.login

import android.app.Activity
import android.arch.lifecycle.ViewModelProvider
import android.content.Intent
import kotlinx.android.synthetic.main.activity_login.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.base_action_bar.ActionBarContract
import noty_team.com.masterovik.base.base_action_bar.BaseActionBarPresenter
import noty_team.com.masterovik.base.base_action_bar.BaseActionBarView
import noty_team.com.masterovik.base.BaseActivity
import noty_team.com.masterovik.ui.fragments.login.entrance.EntranceFragment
import noty_team.com.masterovik.utils.injectViewModel

class LoginActivity : BaseActivity<LoginActivityViewModel>() {
    private lateinit var actionBarView: ActionBarContract.View
    private lateinit var actionBarPresenter: ActionBarContract.Presenter

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): LoginActivityViewModel {
        return injectViewModel(viewModelFactory)
    }

    companion object {

        fun start(baseActivity: Activity) {
            baseActivity.startActivity(
                    Intent(baseActivity, LoginActivity::class.java)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            )
        }
    }

    override fun layout(): Int = R.layout.activity_login

    override fun initialization() {
        fragmentManager.replaceFragment(EntranceFragment.newInstance())

        actionBarView = BaseActionBarView(toolbar_login, activity_login)
        actionBarPresenter = BaseActionBarPresenter(actionBarView)
    }

//    override fun onStart() {
//        super.onStart()
//        actionBarPresenter.start()
//    }
//
//    override fun onStop() {
//        super.onStop()
//        actionBarPresenter.stop()
//    }

    override fun getActionBarView(): ActionBarContract.View? = actionBarView
}
