package noty_team.com.masterovik.ui.fragments.wallet.cards_list

import android.arch.lifecycle.ViewModelProvider
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.fragment_cards_list.*
import noty_team.com.masterovik.R

import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.ui.adapters.items_adapter.MyCardsItem
import noty_team.com.masterovik.ui.adapters.recycler.CardsListSelectingAdapter
import noty_team.com.masterovik.utils.*

class CardsListFragment: BaseFragment<CardsListFragmentViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): CardsListFragmentViewModel {
        return baseActivity.injectViewModel(viewModelFactory)
    }

    companion object {
        private const val CHECKED_CARD_ID_KEY = "CHECKED_CARD_ID_KEY"
        fun newInstance(id: Int): CardsListFragment {
            return CardsListFragment().apply {
                arguments = Bundle().apply {
                    putInt(CHECKED_CARD_ID_KEY, id)
                }
            }
        }
        fun getUserCardId(arguments: Bundle?): Int =
                arguments?.getInt(CHECKED_CARD_ID_KEY, -1)?:-1
    }

    override fun layout() = R.layout.fragment_cards_list

    private lateinit var cardsAdapter: CardsListSelectingAdapter

    override fun initialization(view: View, isFirstInit: Boolean) {
        initToolbar()

        if (isFirstInit){
            initAdapter()
            initClicks()
            listenUpdates()

            getMyCards()
        }
    }

    private fun getMyCards(){
        showProgress(true)
        viewModel.getMyCards()
    }

    private fun initClicks(){
        button_save.getClick {
            cardsAdapter.getCheckedItem()?.let {
                sendBackResult(it)
                onBackPressed()
            }
        }
    }

    private fun listenUpdates() {
        viewModel.myCardsData.observe(this) { response ->
            showProgress(false)
            when (response) {
                is Success -> {
                    cardsAdapter.clear()
                    cardsAdapter.addCards(response.value.map().apply { selectCardItem(this) })
                }
                is Failure -> {
                    showErrorSnack(response.errorMessage)
                }
            }
            checkEmptyList()
        }
    }

    private fun selectCardItem(data: ArrayList<MyCardsItem>): ArrayList<MyCardsItem>{
        if (data.isNotEmpty()) {
            getUserCardId(arguments).let { checkedCardId ->
                if (checkedCardId >= 0) {
                    data.find { it.id == checkedCardId }?.let {
                        foundItem -> foundItem.isChecked = true
                        return data
                    }
                }
                data[0].isChecked = true
            }
        }
        return data
    }

    private fun checkEmptyList(){
        if (cardsAdapter.itemCount > 0) empty_list_text_view.gone()
        else empty_list_text_view.visible()
    }

    private fun initAdapter(){
        my_cards_list.layoutManager = LinearLayoutManager(baseContext)
        cardsAdapter = CardsListSelectingAdapter(arrayListOf()){

        }
        my_cards_list.adapter = cardsAdapter
    }

    private fun initToolbar(){
        toolbarSizeListener.hideEmptyToolbarSpace(true)
        baseActivity.getActionBarView()?.showActionBar(false)

        back_button.getClick {
            onBackPressed()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        baseActivity.getActionBarView()?.setLargeToolbarHeight()
    }

}