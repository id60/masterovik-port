package noty_team.com.masterovik.ui.fragments.webview

import android.arch.lifecycle.ViewModelProvider
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.webkit.WebView
import android.webkit.WebViewClient
import kotlinx.android.synthetic.main.fragment_webview.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.utils.injectViewModel


class WebViewFragment: BaseFragment<BaseViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): BaseViewModel {
        return injectViewModel(viewModelFactory)
    }

    companion object {
        private const val WEB_VIEW_NAME_KEY = "WEB_VIEW_NAME_KEY"
        private const val WEB_VIEW_LINK_KEY = "WEB_VIEW_LINK_KEY"
        fun newInstance(name: String, link: String): WebViewFragment {
            return WebViewFragment().apply {
                arguments = Bundle().apply {
                    putString(WEB_VIEW_NAME_KEY, name)
                    putString(WEB_VIEW_LINK_KEY, link)
                }
            }
        }

        fun getName(bundle: Bundle?) = bundle?.getString(WEB_VIEW_NAME_KEY) ?: ""
        fun getLink(bundle: Bundle?) = bundle?.getString(WEB_VIEW_LINK_KEY) ?: ""

    }

    override fun layout(): Int = R.layout.fragment_webview


    override fun initialization(view: View, isFirstInit: Boolean) {
        baseActivity.getActionBarView()?.showActionBar(true)
        baseActivity.getActionBarView()?.setupCenterText(getName(arguments))


        if (isFirstInit) {
//            showProgressDialog()
            webview.settings.javaScriptEnabled = true

            webview.webViewClient = object: WebViewClient(){
                override fun onPageFinished(view: WebView, url: String ) {
//                    if (!isViewDestroyed())
//                        hideProgressDialog()
                }
            }
            baseActivity.window.setFlags(
                    WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
                    WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED)
            webview.setLayerType(View.LAYER_TYPE_SOFTWARE, null)

            webview.loadUrl(getLink(arguments))
        }
    }
}