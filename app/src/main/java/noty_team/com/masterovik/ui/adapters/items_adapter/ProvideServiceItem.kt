package noty_team.com.masterovik.ui.adapters.items_adapter

data class ProvideServiceItem (var id: Int = -1,
                               var serviceName: String = "",
                               val serviceUrl: String = "",
                               var isChecked: Boolean = false)