package noty_team.com.masterovik.ui.adapters.items_adapter

data class CatalogAdvertItem(var advertId: Int = -1,
                             var advertName: String = "",
                             var dateCreated: String = "",
                             var price: String = "",
                             var shortDescription: String = "",
                             var fullDescription: String = "",
                             var name: String = "",
                             var phone: String = "",
                             var email: String = "",
                             var dateEnded: String = "",
                             var cityId: Int = -1,
                             var address: String = "")
