package noty_team.com.masterovik.ui.fragments.wallet.top_up_balance

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import noty_team.com.masterovik.api.request.services.AddServiceRequest
import noty_team.com.masterovik.api.request.wallet.add_money.AddMoneyRequest
import noty_team.com.masterovik.api.response.BaseResponse
import noty_team.com.masterovik.api.response.services.get_services.GetServicesResponse
import noty_team.com.masterovik.api.response.wallet.add_money.AddMoneyResponse
import noty_team.com.masterovik.api.response.wallet.my_cards.MyCardsResponse
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.utils.Failure
import noty_team.com.masterovik.utils.Result
import noty_team.com.masterovik.utils.Success
import javax.inject.Inject

class TopUpBalanceFragmentViewModel @Inject constructor(app: Application) : BaseViewModel(app) {

    val addMoneyData = MutableLiveData<Result<AddMoneyResponse>>()

    fun addMoney(data: AddMoneyRequest){
        api.walletApi.addMoney(data) {
            addMoneyData.postValue(it)
        }.call()
    }

    override fun onClear() {
        addMoneyData.value = null
    }
}