package noty_team.com.masterovik.ui.fragments.profile_setting.edit_phone_number

import android.arch.lifecycle.ViewModelProvider
import android.view.View
import kotlinx.android.synthetic.main.fragment_edit_phone_number.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.api.request.change_phone.ChangePhoneRequest
import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.utils.*
import noty_team.com.masterovik.utils.paper.PaperIO
import java.util.concurrent.TimeUnit

class FragmentEditPhoneNumber : BaseFragment<EditPhoneFragmentViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): EditPhoneFragmentViewModel {
        return injectViewModel(viewModelFactory)
    }

    companion object {
        fun newInstance() = FragmentEditPhoneNumber()
    }
    override fun layout() = R.layout.fragment_edit_phone_number

    override fun initialization(view: View, isFirstInit: Boolean) {
        initToolbar()

        if (isFirstInit){
            phoneManager.bindTo(phone_edit_text)

            initSmsButton()
            initLoginButton()
            listenUpdates()
        }
    }

    private fun initToolbar(){
        baseActivity.getActionBarView()?.changeSizeActionBar(false)
        baseActivity.getActionBarView()?.setupCenterText("Изменить номер телефона")
        baseActivity.getActionBarView()?.showLeftButton(true)
        baseActivity.getActionBarView()?.setBackButton(layoutInflater)?.getClick {
            onBackPressed()
        }
    }

    private fun listenUpdates(){
        viewModel.changePhoneData.observe(this) { response ->
            showProgress(false)
            when(response){
                is Success -> {
                    response.value.message?.let {
                        updateDataBase(getPhone())
                        showSuccessSnack(it)
                    }
                }
                is Failure -> {
                    showErrorSnack(response.errorMessage)
                }
            }
        }

        viewModel.smsData.observe(this) { response ->
            showProgress(false)
            when(response){
                is Success -> {
                    response.value.message?.let {
                        showSuccessSnack(it)
                        showSmsFieldWithTimer()
                    }
                }
                is Failure -> {
                    showErrorSnack(response.errorMessage)
                }
            }
        }

        viewModel.timerData.observe(this) { tick ->
            if (tick.isFinished) enableSmsButton(true)
            else setTime(tick.time)
            timerLayoutVisibility(!tick.isFinished)
        }
    }

    private fun updateDataBase(phone: String){
        val userProfile = PaperIO.getUserProfile()
        userProfile.phone = phone
        PaperIO.setUserProfile(userProfile)
    }

    private fun getPhone() = phoneManager.phone.toString().replace("+", "")

    private fun initLoginButton(){
        enableLoginButton(false)
        save_phone_button.getClick {
            if (validate()) {
                showProgress(true)
                val phone = getPhone()
                val smsCode = sms_code_edit_text.text.toString().trim()
                val request = ChangePhoneRequest(
                        phone = phone,
                        sms = smsCode,
                        accessToken = PaperIO.getAccessToken()
                )
                viewModel.changePhone(request)
            }
        }
    }

    private fun initSmsButton(){
        get_sms_button.getClick {
            if (validatePhone()) {
                showProgress(true)
                viewModel.changePhone(
                        ChangePhoneRequest(
                                phone = getPhone(),
                                accessToken = PaperIO.getAccessToken()
                        )
                )
            }
        }
    }

    private fun showSmsFieldWithTimer(){
        enableLoginButton(true)
        enableSmsButton(false)
        showSmsLayout(true)
        timerLayoutVisibility(true)
        viewModel.timerData.start()
    }

    private fun setTime(timeMillis: Long) {
        timer_text_view.text = TimeUnit.MILLISECONDS.toSeconds(timeMillis).toString()
        progress_bar.progress = 100f * timeMillis / viewModel.timerData.totalTime
    }

    private fun timerLayoutVisibility(show: Boolean) {
        if (show) timer_layout.visible()
        else timer_layout.gone()
    }

    private fun enableSmsButton(enable: Boolean) {
        if (enable) get_sms_button.enable()
        else get_sms_button.disable()
    }

    private fun showSmsLayout(show: Boolean) {
        if (show) {
            get_sms_button.text = getString(R.string.send_code_again)
            sms_code_layout.visible()
        } else {
            get_sms_button.text = getString(R.string.get_code)
            sms_code_layout.gone()
        }
    }

    private fun enableLoginButton(enable: Boolean) {
        if (enable) save_phone_button.enable()
        else save_phone_button.disable()
    }

    private fun validate(): Boolean {
        return validatePhone() && validateSms()
    }

    private fun validatePhone(): Boolean {
        phone_edit_text.setError(false)
        val phone = phone_edit_text.text.toString().trim()
        return phone_edit_text.validate(
                validator.isPhoneValid(phone),
                viewForShake = phone_edit_text
        )
    }

    private fun validateSms(): Boolean {
        val smsCode = sms_code_edit_text.text.toString().trim()
        validator.isSmsValid(smsCode).let {
            if (!it) showErrorSnack("Короткий смс-код")
            return it
        }
    }

}