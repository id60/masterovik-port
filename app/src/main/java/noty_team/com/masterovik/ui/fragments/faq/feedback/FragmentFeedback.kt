package noty_team.com.masterovik.ui.fragments.faq.feedback

import android.arch.lifecycle.ViewModelProvider
import android.view.View
import noty_team.com.masterovik.R

import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.utils.injectViewModel

class FragmentFeedback : BaseFragment<BaseViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): BaseViewModel {
        return injectViewModel(viewModelFactory)
    }

    companion object {
        fun newInstance(): FragmentFeedback {
            return FragmentFeedback()
        }
    }

    override fun layout() = R.layout.fragment_feedback

    override fun initialization(view: View, isFirstInit: Boolean) {

    }

}