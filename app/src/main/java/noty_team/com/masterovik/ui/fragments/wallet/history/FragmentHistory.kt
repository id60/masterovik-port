package noty_team.com.masterovik.ui.fragments.wallet.history

import android.arch.lifecycle.ViewModelProvider
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.fragment_histoty.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.api.request.wallet.history.PaymentHistoryRequest
import noty_team.com.masterovik.api.response.wallet.add_money.AddMoneyResponse

import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.ui.adapters.items_adapter.HistoryPaymentItem
import noty_team.com.masterovik.ui.adapters.items_adapter.MyCardsItem
import noty_team.com.masterovik.ui.adapters.recycler.HistoryPaymentAdapter
import noty_team.com.masterovik.ui.fragments.wallet.cards_list.CardsListFragmentViewModel
import noty_team.com.masterovik.ui.fragments.wallet.sorting_history.FragmentSortingHistory
import noty_team.com.masterovik.ui.fragments.wallet.sorting_history.HistorySortingFragmentViewModel
import noty_team.com.masterovik.ui.fragments.wallet.top_up_balance.FragmentTopUpBalance
import noty_team.com.masterovik.utils.*

class FragmentHistory : BaseFragment<HistoryFragmentViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): HistoryFragmentViewModel {
        return injectViewModel(viewModelFactory)
    }

    override fun layout() = R.layout.fragment_histoty

    private lateinit var paymentHistoryAdapter: HistoryPaymentAdapter
    private var myCards = arrayListOf<MyCardsItem>()

    private lateinit var historySortingFragmentViewModel: HistorySortingFragmentViewModel
    private lateinit var cardsListFragmentViewModel: CardsListFragmentViewModel

    override fun initialization(view: View, isFirstInit: Boolean) {

        if (isFirstInit){
            historySortingFragmentViewModel = baseActivity.injectViewModel(viewModelFactory)
            cardsListFragmentViewModel = baseActivity.injectViewModel(viewModelFactory)

            initAdapter()
            listenUpdates()

            getMyCards()
        }
    }

    private fun getMyCards(){
        showProgress(true)
        cardsListFragmentViewModel.getMyCards()
    }

    private fun getPaymentHistory(request: PaymentHistoryRequest = PaymentHistoryRequest()){
        showProgress(true)
        viewModel.getPaymentHistory(request)
    }

    private fun listenUpdates() {
        cardsListFragmentViewModel.myCardsData.observe(this) { response ->
            showProgress(false)
            when (response) {
                is Success -> {
                    myCards = response.value.map()
                    getPaymentHistory()
                }
                is Failure -> {
                    showErrorSnack(response.errorMessage)
                }
            }
        }

        viewModel.paymentHistoryData.observe(this) { response ->
            showProgress(false)
            when (response) {
                is Success -> {
                    paymentHistoryAdapter.clear()
                    paymentHistoryAdapter.addAll(response.value.map(myCards))
                }
                is Failure -> {
                    showErrorSnack(response.errorMessage)
                }
            }
            checkEmptyList()
        }

        historySortingFragmentViewModel.historySortingData.observe(this) { response ->
            getPaymentHistory(response)
        }

        subscribeForResult {
            when(it.classProvider){
                FragmentTopUpBalance::class -> {
                    if (it.value::class == AddMoneyResponse::class)
                        getPaymentHistory()
                }
            }
        }
    }

    private fun checkEmptyList(){
        if (paymentHistoryAdapter.itemCount > 0) empty_list_text_view.gone()
        else empty_list_text_view.visible()
    }

    private fun initAdapter(){
        recycler_history.layoutManager = LinearLayoutManager(baseActivity)
        paymentHistoryAdapter = HistoryPaymentAdapter(arrayListOf())
        recycler_history.adapter = paymentHistoryAdapter
    }

}