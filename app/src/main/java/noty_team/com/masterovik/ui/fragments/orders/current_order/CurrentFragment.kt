package noty_team.com.masterovik.ui.fragments.orders.current_order

import android.arch.lifecycle.ViewModelProvider
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.fragment_current.*

import noty_team.com.masterovik.R

import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.ui.adapters.items_adapter.CurrentOrderItem
import noty_team.com.masterovik.ui.adapters.recycler.CurrentOrderAdapter
import noty_team.com.masterovik.ui.fragments.orders.current_order.order.FragmentOrderItem
import noty_team.com.masterovik.utils.injectViewModel

class CurrentFragment : BaseFragment<BaseViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): BaseViewModel {
        return injectViewModel(viewModelFactory)
    }

    companion object {
        fun newInstance(): CurrentFragment {
            return CurrentFragment()
        }
    }

    override fun layout() = R.layout.fragment_current

    override fun initialization(view: View, isFirstInit: Boolean) {

        recycler_current_order.layoutManager = LinearLayoutManager(baseActivity)
        recycler_current_order.adapter = CurrentOrderAdapter(createList()){
            fragmentManager.addFragment(FragmentOrderItem.newInstance())
        }
    }

    private fun createList(): ArrayList<CurrentOrderItem> {
        val list: ArrayList<CurrentOrderItem> = ArrayList()
        val item1 = CurrentOrderItem("Название Заказа", "Имя Фамилия", 15)
        val item2 = CurrentOrderItem("Название Заказа", "Имя Фамилия", 22)
        val item3 = CurrentOrderItem("Название Заказа", "Имя Фамилия", 6)

        list.add(item1)
        list.add(item2)
        list.add(item3)

        return list
    }

}