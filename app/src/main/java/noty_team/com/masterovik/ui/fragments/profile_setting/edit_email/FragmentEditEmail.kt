package noty_team.com.masterovik.ui.fragments.profile_setting.edit_email

import android.arch.lifecycle.ViewModelProvider
import android.view.View
import kotlinx.android.synthetic.main.fragment_edit_email.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.api.request.change_email.ChangeEmailRequest

import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.utils.*
import noty_team.com.masterovik.utils.paper.PaperIO

class FragmentEditEmail : BaseFragment<EditEmailFragmentViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): EditEmailFragmentViewModel {
        return injectViewModel(viewModelFactory)
    }

    companion object {
        fun newInstance() = FragmentEditEmail()
    }
    override fun layout() = R.layout.fragment_edit_email

    override fun initialization(view: View, isFirstInit: Boolean) {
        initToolbar()
        if (isFirstInit){
            initClicks()
            listenUpdates()
        }
    }

    private fun initClicks(){
        leave_complaint_done.getClick {
            if (validate()){
                showProgress(true)
                val request = ChangeEmailRequest(
                        email = new_email_edit_text.text.toString().trim(),
                        accessToken = PaperIO.getAccessToken()
                )
                viewModel.changeEmailData(request)
            }
        }
    }

    private fun initToolbar(){
        baseActivity.getActionBarView()?.changeSizeActionBar(false)
        baseActivity.getActionBarView()?.setupCenterText("Изменить электронную почту")
        baseActivity.getActionBarView()?.showLeftButton(true)
        baseActivity.getActionBarView()?.setBackButton(layoutInflater)?.getClick {
            onBackPressed()
        }
    }

    private fun listenUpdates() {
        viewModel.changeEmailData.observe(this) { response ->
            showProgress(false)
            when (response) {
                is Success -> {
                    response.value.message?.let {
                        updateDataBase(new_email_edit_text.text.toString().trim())
                        showSuccessSnack(it)
                    }
                }
                is Failure -> {
                    showErrorSnack(response.errorMessage)
                }
            }
        }
    }

    private fun updateDataBase(email: String){
        val userProfile = PaperIO.getUserProfile()
        userProfile.email = email
        PaperIO.setUserProfile(userProfile)
    }

    private fun validate(): Boolean {
        new_email_edit_text.setError(false)
        repeat_email_edit_text.setError(false)

        val newEmail = new_email_edit_text.text.toString().trim()
        new_email_edit_text.validate(
                validator.isEmailValid(newEmail),
                viewForShake = new_email_edit_text).let { if (!it) return it }

        val repeatEmail = repeat_email_edit_text.text.toString().trim()
        repeat_email_edit_text.validate(
                validator.isEmailValid(repeatEmail),
                viewForShake = repeat_email_edit_text).let { if (!it) return it }

        if (newEmail != repeatEmail) {
            showErrorSnack("Email не совпадает")
            return false
        }

        return true
    }
}