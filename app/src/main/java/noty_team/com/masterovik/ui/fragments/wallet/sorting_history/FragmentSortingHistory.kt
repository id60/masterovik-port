package noty_team.com.masterovik.ui.fragments.wallet.sorting_history

import android.arch.lifecycle.ViewModelProvider
import android.view.View
import android.widget.RadioGroup
import kotlinx.android.synthetic.main.fragment_sorting_history.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.api.request.wallet.history.PaymentHistoryRequest

import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.ui.adapters.items_adapter.HistoryPaymentItem
import noty_team.com.masterovik.ui.adapters.items_adapter.MyCardsItem
import noty_team.com.masterovik.ui.dialogs.credit_card_filter.CreditCardDialog
import noty_team.com.masterovik.ui.fragments.wallet.cards_list.CardsListFragment
import noty_team.com.masterovik.utils.injectViewModel
import noty_team.com.masterovik.utils.listenForDrawn
import noty_team.com.masterovik.utils.observe

class FragmentSortingHistory : BaseFragment<HistorySortingFragmentViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): HistorySortingFragmentViewModel {
        return baseActivity.injectViewModel(viewModelFactory)
    }

    companion object {
        fun newInstance() = FragmentSortingHistory()
    }

    override fun layout() = R.layout.fragment_sorting_history

    private var selectedCard = MyCardsItem()

    override fun initialization(view: View, isFirstInit: Boolean) {
        initToolbar()
        if (isFirstInit){
            initClicks()
            listenUpdates()
        }
    }

    private fun initClicks(){
        reset_filters_button.getClick {
            time_history_radio_group.clearCheck()
            payment_type_history_radio_group.clearCheck()
            selectedCard = MyCardsItem()
            card_number_text_view.text = ""
            viewModel.setSortingData(null)
        }

        set_card_layout.getClick {
            fragmentManager.addFragment(CardsListFragment.newInstance(selectedCard.id))
        }

        search_button.getClick {
            val jsonMemberNew: Boolean? = when(time_history_radio_group.checkedRadioButtonId){
                R.id.new_payments_radio_button -> true
                R.id.old_payments_radio_button -> false
                else -> null
            }
            val operation: String? = when(payment_type_history_radio_group.checkedRadioButtonId){
                R.id.refill_payments_radio_button -> HistoryPaymentItem.OperationType.REFILL.typeString
                R.id.withdrawal_payments_radio_button -> HistoryPaymentItem.OperationType.WITHDRAWAL.typeString
                else -> null
            }
            val cardId = if (selectedCard.id >= 0) selectedCard.id else null
            val card = if (selectedCard.id >= 0) selectedCard else null
            val searchRequest = PaymentHistoryRequest(
                    jsonMemberNew = jsonMemberNew,
                    operation = operation,
                    cardId = cardId,
                    card = card
            )
            viewModel.setSortingData(searchRequest)
            onBackPressed()
        }
    }

    private fun initFilters(data: PaymentHistoryRequest){
        time_history_radio_group.listenForDrawn{
            (it as RadioGroup).check(
                when (data.jsonMemberNew) {
                    true -> R.id.new_payments_radio_button
                    false -> R.id.old_payments_radio_button
                    else -> 0
                })
        }
        payment_type_history_radio_group.listenForDrawn {
            (it as RadioGroup).check(
                    when (data.operation) {
                        HistoryPaymentItem.OperationType.REFILL.typeString -> R.id.refill_payments_radio_button
                        HistoryPaymentItem.OperationType.WITHDRAWAL.typeString -> R.id.withdrawal_payments_radio_button
                        else -> 0
                    }
            )
        }
        data.card?.let { selectedCard = it }
        card_number_text_view.text = selectedCard.cardNumber
    }

    private fun listenUpdates() {
        viewModel.historySortingData.observe(this) { response ->
            initFilters(response)
        }

        subscribeForResult {
            when(it.classProvider){
                CardsListFragment::class -> {
                    selectedCard = it.value as MyCardsItem
                    card_number_text_view.text = selectedCard.cardNumber
                }
            }
        }
    }

    private fun initToolbar(){
        toolbarSizeListener.resizeContainerSmall()
        toolbarSizeListener.hideEmptyToolbarSpace(true)
        baseActivity.getActionBarView()?.showActionBar(false)

        cross_sorting_history.getClick{
            onBackPressed()
        }

    }

    override fun onDestroyView() {
        baseActivity.getActionBarView()?.setLargeToolbarHeight()
        super.onDestroyView()
    }
}