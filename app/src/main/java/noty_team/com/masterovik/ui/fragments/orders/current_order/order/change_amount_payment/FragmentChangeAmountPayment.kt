package noty_team.com.masterovik.ui.fragments.orders.current_order.order.change_amount_payment

import android.arch.lifecycle.ViewModelProvider
import android.view.View
import kotlinx.android.synthetic.main.fragment_change_amount_payment.*
import noty_team.com.masterovik.R

import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.utils.injectViewModel

class FragmentChangeAmountPayment : BaseFragment<BaseViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): BaseViewModel {
        return injectViewModel(viewModelFactory)
    }

    companion object {
        fun newInstance() = FragmentChangeAmountPayment()
    }
    override fun layout() = R.layout.fragment_change_amount_payment

    override fun initialization(view: View, isFirstInit: Boolean) {

        cross_change_amount_payment.getClick {
            onBackPressed()
        }
    }

}