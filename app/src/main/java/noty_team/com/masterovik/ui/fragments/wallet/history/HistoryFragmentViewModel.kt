package noty_team.com.masterovik.ui.fragments.wallet.history

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import noty_team.com.masterovik.api.request.services.AddServiceRequest
import noty_team.com.masterovik.api.request.wallet.add_card.AddCardRequest
import noty_team.com.masterovik.api.request.wallet.history.PaymentHistoryRequest
import noty_team.com.masterovik.api.response.BaseResponse
import noty_team.com.masterovik.api.response.services.get_services.GetServicesResponse
import noty_team.com.masterovik.api.response.wallet.add_card.AddCardResponse
import noty_team.com.masterovik.api.response.wallet.history.PaymentHistoryResponse
import noty_team.com.masterovik.api.response.wallet.my_cards.MyCardsResponse
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.utils.Failure
import noty_team.com.masterovik.utils.Result
import noty_team.com.masterovik.utils.Success
import javax.inject.Inject

class HistoryFragmentViewModel @Inject constructor(app: Application) : BaseViewModel(app) {

    val paymentHistoryData = MutableLiveData<Result<PaymentHistoryResponse>>()

    fun getPaymentHistory(data: PaymentHistoryRequest){
        api.walletApi.getPaymentHistory(data) {
            paymentHistoryData.postValue(it)
        }.call()
    }

    override fun onClear() {
        paymentHistoryData.value = null
    }
}