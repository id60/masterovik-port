package noty_team.com.masterovik.ui.fragments.faq

import android.arch.lifecycle.ViewModelProvider
import android.view.LayoutInflater
import android.view.View
import kotlinx.android.synthetic.main.fragment_faq_base.*
import noty_team.com.masterovik.R

import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.ui.adapters.view_pager.FAQPagerAdapter
import noty_team.com.masterovik.ui.fragments.faq.faq.FragmentFAQ
import noty_team.com.masterovik.ui.fragments.faq.feedback.FragmentFeedback
import noty_team.com.masterovik.utils.injectViewModel

class FragmentFAQBase : BaseFragment<BaseViewModel>() {


    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): BaseViewModel {
        return injectViewModel(viewModelFactory)
    }

    companion object {
        fun newInstance(): FragmentFAQBase {
            return FragmentFAQBase()
        }
    }

    override fun layout() = R.layout.fragment_faq_base

    override fun initialization(view: View, isFirstInit: Boolean) {


        initTopBar()

    }

    private fun initTopBar() {

        baseActivity.getActionBarView()?.setLargeToolbarHeight()

        view_pager_faq.adapter = FAQPagerAdapter(childFragmentManager, view_pager_faq, arrayListOf(FragmentFAQ(), FragmentFeedback()))
        tabLayout_faq.setupWithViewPager(view_pager_faq)

        baseActivity.getActionBarView()?.setupCenterText("FAQ")

        var rightView = LayoutInflater.from(baseActivity).inflate(R.layout.ab_empty, null)
        baseActivity.getActionBarView()?.setupRightButton(rightView)

        baseActivity.getActionBarView()?.showLeftButton(true)
        baseActivity.getActionBarView()?.setDrawerMenu(layoutInflater)?.getClick {
            onDrawerPressedListener.onDrawerPressed()
        }
    }

}