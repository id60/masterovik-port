package noty_team.com.masterovik.ui.fragments.cities_list

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import noty_team.com.masterovik.api.request.edit_profile.EditProfileRequest
import noty_team.com.masterovik.api.response.city.cities_list.CitiesResponse
import noty_team.com.masterovik.api.response.edit_profile.EditProfileResponse
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.utils.Result
import javax.inject.Inject

class CitiesListFragmentViewModel @Inject constructor(app: Application) : BaseViewModel(app) {

    var citiesData = MutableLiveData<Result<CitiesResponse>>()

    fun getCities(){
        api.cityApi.getCities { citiesData.postValue(it) }.call()
    }

    override fun onClear() {
        citiesData.value = null
    }
}