package noty_team.com.masterovik.ui.dialogs

import android.app.Dialog
import android.arch.lifecycle.ViewModelProvider
import android.content.Context
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.design.widget.Snackbar
import android.support.v4.app.DialogFragment
import android.support.v4.content.ContextCompat
import android.view.*
import android.widget.FrameLayout
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.dialog_progress.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.BaseActivity
import noty_team.com.masterovik.base.SnackBarListener
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.ui.activities.main.BackResultViewModel
import noty_team.com.masterovik.utils.injectViewModel
import noty_team.com.masterovik.utils.validator.FieldsValidationInterface
import java.util.concurrent.TimeUnit
import javax.inject.Inject

abstract class BaseFragmentDialog<V : BaseViewModel> : DialogFragment() {

    var snackBarListener: SnackBarListener = SnackBarListener.empty

    protected lateinit var viewModel: V

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var validator: FieldsValidationInterface

    @Inject
    protected lateinit var baseContext: Context

    @Inject
    lateinit var baseActivity: BaseActivity<*>

    @Inject
    lateinit var compositeDisposable: CompositeDisposable

    protected var rootView: View? = null
    @LayoutRes
    protected abstract fun layout(): Int

    protected abstract fun initialization(view: View, isFirstInit: Boolean)
    protected abstract fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): V

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try {
            (context as BaseActivity<*>).fragmentComponent.inject(this as BaseFragmentDialog<BaseViewModel>)
        } catch (e: UninitializedPropertyAccessException) {
            (context as BaseActivity<*>).initFragmentComponent()
            context.fragmentComponent.inject(this as BaseFragmentDialog<BaseViewModel>)
//            fragmentManager = context.fragmentManager
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = provideViewModel(viewModelFactory)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = if (layout() != 0)
            addProgressView(inflater.inflate(layout(), container, false))
        else
            super.onCreateView(inflater, container, savedInstanceState)
        return if (rootView == null) view else rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setStyle(STYLE_NORMAL, R.style.AppTheme)
        initialization(view, rootView == null)
        rootView = view
        super.onViewCreated(view, savedInstanceState)
    }

    private fun addProgressView(rootView: View): View {
        val rootLayout = FrameLayout(baseContext)
        val layoutParams = FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, Gravity.CENTER)
        rootLayout.layoutParams = layoutParams
        rootLayout.addView(rootView)

        val progressLayout = baseActivity.layoutInflater.inflate(R.layout.dialog_progress, null)
        rootLayout.addView(progressLayout)
        return rootLayout
    }

    fun showProgress(show: Boolean) {
        progress_layout?.visibility = if (show) View.VISIBLE else View.GONE
    }

    override fun onStart() {
        super.onStart()
        dialog.window?.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
    }

    override fun onDestroyView() {
        viewModel.onClear()
        super.onDestroyView()
    }

    override fun onDestroy() {
        baseActivity.toggleKeyboard(false)
        compositeDisposable.clear()
        super.onDestroy()
    }

    fun showErrorSnack(message: String) {
        val snackbar = view?.let {
            Snackbar.make(it, message, Snackbar.LENGTH_LONG)
        }
        context?.let {
            ContextCompat.getColor(it, R.color.colorRed)
        }?.let {
            snackbar?.view?.setBackgroundColor(it)
        }
        snackbar?.show()
    }

    fun showSuccessSnack(message: String) {
        val snackbar = view?.let {
            Snackbar.make(it, message, Snackbar.LENGTH_LONG)
        }
        context?.let {
            ContextCompat.getColor(it, R.color.colorGreen)
        }?.let {
            snackbar?.view?.setBackgroundColor(it)
        }
        snackbar?.show()
    }

    fun View.getClick(durationMillis: Long = 500, onClick: (view: View) -> Unit) {
        RxView.clicks(this)
                .throttleFirst(durationMillis, TimeUnit.MILLISECONDS)
                .subscribe({ _ ->
                    onClick(this)
                }, {

                })
                .also { compositeDisposable.add(it) }
    }

    fun subscribeForResult(onResult: (result: BackResultViewModel.BackResult<*, *>) -> Unit) {
        (baseActivity.injectViewModel(viewModelFactory) as BackResultViewModel?)?.observe(onResult, this)
    }

    fun <T : Any> sendBackResult(data: T) {
        (baseActivity.injectViewModel(viewModelFactory) as BackResultViewModel?)?.addResult(data, this::class)
    }
}