package noty_team.com.masterovik.ui.fragments.request.in_work.request_in_detail

import android.arch.lifecycle.ViewModelProvider
import android.view.View
import kotlinx.android.synthetic.main.fragment_request_order.*
import noty_team.com.masterovik.R

import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.ui.fragments.orders.current_order.order.change_amount_payment.FragmentChangeAmountPayment
import noty_team.com.masterovik.ui.fragments.request.in_work.request_in_detail.change_deadlines.FragmentChangeDeadlines
import noty_team.com.masterovik.utils.injectViewModel

class FragmentInDetail : BaseFragment<BaseViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): BaseViewModel {
        return injectViewModel(viewModelFactory)
    }

    companion object {

        fun newInstance() = FragmentInDetail()
    }

    override fun layout() = R.layout.fragment_request_order

    override fun initialization(view: View, isFirstInit: Boolean) {
        toolbarSizeListener.hideEmptyToolbarSpace(true)
        baseActivity.getActionBarView()?.showActionBar(false)
        toolbarSizeListener.resizeContainerSmall()

        cross_fragment_in_details.getClick {
            onBackPressed()
        }

        change_deadline.getClick {
            fragmentManager.addFragment(FragmentChangeDeadlines.newInstance())

        }

        change_sum_request.getClick {
            fragmentManager.addFragment(FragmentChangeAmountPayment.newInstance())
        }
    }
}