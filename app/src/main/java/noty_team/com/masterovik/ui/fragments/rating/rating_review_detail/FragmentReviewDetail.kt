package noty_team.com.masterovik.ui.fragments.rating.rating_review_detail

import android.arch.lifecycle.ViewModelProvider
import android.view.View
import noty_team.com.masterovik.R

import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.utils.injectViewModel

class FragmentReviewDetail : BaseFragment<BaseViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): BaseViewModel {
        return injectViewModel(viewModelFactory)
    }
    companion object {
        fun newInstance() = FragmentReviewDetail()
    }

    override fun layout() = R.layout.fragment_review_detail

    override fun initialization(view: View, isFirstInit: Boolean) {

        toolbarSizeListener.hideEmptyToolbarSpace(true)
        baseActivity.getActionBarView()?.showActionBar(false)

        baseActivity.findViewById<View>(R.id.cross_rating).setOnClickListener {
            onBackPressed()
        }

    }
}