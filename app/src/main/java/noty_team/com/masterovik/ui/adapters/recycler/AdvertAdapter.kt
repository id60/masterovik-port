package noty_team.com.masterovik.ui.adapters.recycler

import android.view.View
import kotlinx.android.synthetic.main.item_catalog_advert.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.ui.adapters.items_adapter.CatalogAdvertItem
import java.lang.Exception

class AdvertAdapter(list: ArrayList<CatalogAdvertItem>,
                    val onClick:(item: CatalogAdvertItem)->Unit) :
        BaseAdapter<CatalogAdvertItem, AdvertAdapter.ViewHolder>(list) {

    override fun createViewHolder(view: View, layoutId: Int) = ViewHolder(view)

    override fun getItemViewType(position: Int) = R.layout.item_catalog_advert

    inner class ViewHolder(view: View) : BaseAdapter.BaseViewHolder(view) {

        init {
            try {
                itemView.getClick {
                    onClick(list[adapterPosition])
                }
            }catch (e: Exception){}
        }
        override fun bind(pos: Int) {
            name_catalog_advert.text = list[pos].name
            date_catalog_advert.text = list[pos].dateCreated
            price_catalog_advert.text = list[pos].price
            description_catalog_advert.text = list[pos].shortDescription
        }
    }

}