package noty_team.com.masterovik.ui.adapters.items_adapter

 data class DoneOrderItem (var nameOrder: String = "Название Заказа", var nameSurname: String = "Имя Фамилия", var date: Int)