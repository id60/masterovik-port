package noty_team.com.masterovik.ui.fragments.profile_setting

import android.arch.lifecycle.ViewModelProvider
import android.view.View
import kotlinx.android.synthetic.main.fragment_profile_setting.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.ui.fragments.profile_setting.edit_email.FragmentEditEmail
import noty_team.com.masterovik.ui.fragments.profile_setting.edit_notification.FragmentEditNotification
import noty_team.com.masterovik.ui.fragments.profile_setting.edit_personal_data.FragmentEditPersonalData
import noty_team.com.masterovik.ui.fragments.profile_setting.edit_phone_number.FragmentEditPhoneNumber
import noty_team.com.masterovik.ui.fragments.profile_setting.edit_setting_sevice.FragmentEditSettingService
import noty_team.com.masterovik.utils.injectViewModel

class FragmentProfileSetting : BaseFragment<BaseViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): BaseViewModel {
        return injectViewModel(viewModelFactory)
    }

    companion object {
        fun newInstance() = FragmentProfileSetting()
    }

    override fun layout() = R.layout.fragment_profile_setting

    override fun initialization(view: View, isFirstInit: Boolean) {
        initToolbar()

        edit_profile_data.getClick {
            fragmentManager.addFragment(FragmentEditPersonalData.newInstance())
        }

        edit_phone_number.getClick {
            fragmentManager.addFragment(FragmentEditPhoneNumber.newInstance())
        }

        edit_email.getClick {
            fragmentManager.addFragment(FragmentEditEmail.newInstance())
        }

        edit_notification.getClick {
            fragmentManager.addFragment(FragmentEditNotification.newInstance())
        }
        setting_service.getClick {
            fragmentManager.addFragment(FragmentEditSettingService.newInstance())
        }


    }

    private fun initToolbar(){
//        toolbarSizeListener.resizeContainerSmall()
        toolbarSizeListener.hideEmptyToolbarSpace(false)
        baseActivity.getActionBarView()?.changeSizeActionBar(false)
        baseActivity.getActionBarView()?.setupCenterText("Настройки")
        baseActivity.getActionBarView()?.setupRightButton(layoutInflater.inflate(R.layout.ab_empty, null))
        baseActivity.getActionBarView()?.setSmallToolbarHeight()

        baseActivity.getActionBarView()?.showLeftButton(true)
        baseActivity.getActionBarView()?.setDrawerMenu(layoutInflater)?.getClick {
            onDrawerPressedListener.onDrawerPressed()
        }
    }
}