package noty_team.com.masterovik.ui.activities.login

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import noty_team.com.masterovik.api.request.auth.register.RegistrationRequest
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import javax.inject.Inject
import android.arch.lifecycle.MediatorLiveData


class LoginActivityViewModel @Inject constructor(app: Application) : BaseViewModel(app) {

    val phoneData = MutableLiveData<String>()
    val registrationRequestData = MutableLiveData<RegistrationRequest>()
    val mediatorLiveData = MediatorLiveData<RegistrationRequest>()

    init {
        mediatorLiveData.addSource(phoneData) { phone ->
            phone?.let {
                mediatorLiveData.setValue(RegistrationRequest(phone = it))
            }
        }
        mediatorLiveData.addSource(registrationRequestData) { registrationData ->
            registrationData?.let {
                mediatorLiveData.setValue(it)
            }
        }
    }

    override fun onClear() {

    }
}