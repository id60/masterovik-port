package noty_team.com.masterovik.ui.fragments.advert.filter_advert.time_filter

import android.arch.lifecycle.ViewModelProvider
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.fragment_filter_time.*
import noty_team.com.masterovik.R

import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import java.lang.NumberFormatException
import java.util.*
import kotlin.collections.ArrayList
import noty_team.com.masterovik.utils.*

class TimeFilterFragment : BaseFragment<BaseViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): BaseViewModel {
        return injectViewModel(viewModelFactory)
    }

    companion object {
        private const val TIMES_KEY = "TIMES_KEY"
        fun newInstance(dates: ArrayList<Long> = arrayListOf()): TimeFilterFragment = TimeFilterFragment().apply {
            arguments = Bundle().apply {
                putLongArray(TIMES_KEY, dates.toLongArray())
            }
        }

        fun getDates(arguments: Bundle?): ArrayList<Long> =
                arguments?.getLongArray(TIMES_KEY)?.toCollection(arrayListOf()) ?: arrayListOf()
    }

    override fun layout() = R.layout.fragment_filter_time

    private val calendarFrom = Calendar.getInstance(getLocale())
    private val calendarTo = Calendar.getInstance(getLocale())

    override fun initialization(view: View, isFirstInit: Boolean) {
        initToolbar()
        if (isFirstInit) {
            initCalendars()
            initDates()
            initClicks()
        }
    }

    private fun initCalendars() {
        calendarFrom.set(Calendar.MILLISECOND, 0)
        calendarFrom.set(Calendar.SECOND, 0)
        calendarFrom.set(Calendar.MINUTE, 0)
        calendarFrom.set(Calendar.HOUR_OF_DAY, 0)

        calendarTo.set(Calendar.MILLISECOND, 0)
        calendarTo.set(Calendar.SECOND, 0)
        calendarTo.set(Calendar.MINUTE, 0)
        calendarTo.set(Calendar.HOUR_OF_DAY, 0)
    }

    private fun initDates() {
        getDates(arguments).let {
            if (it.size >= 2) {
                val timeFrom = CustomTimePicker.getFormattedTimeString(it[0])
                val timeTo = CustomTimePicker.getFormattedTimeString(it[1])
                time_from_edit_text.setText(timeFrom)
                time_to_edit_text.setText(timeTo)
            }
        }
        time_from_edit_text.getClick {
            CustomTimePicker.show(baseActivity, calendarFrom) {
                time_from_edit_text.setText(it)
            }
        }
        time_to_edit_text.getClick {
            CustomTimePicker.show(baseActivity, calendarTo) {
                time_to_edit_text.setText(it)
            }
        }
    }

    private fun initClicks() {
        button_save.getClick {
            if (validate()) {
                val timeFromTimestamp = calendarFrom.timeInMillis
                val timeToTimestamp = calendarTo.timeInMillis
                sendBackResult(arrayListOf(timeFromTimestamp, timeToTimestamp))
                onBackPressed()
            }
        }
    }

    private fun validate(): Boolean {
        time_from_edit_text.setError(false)
        time_to_edit_text.setError(false)

        val timeFrom = time_from_edit_text.text.toString().trim()
        time_from_edit_text.validate(
                timeFrom.isNotEmpty(),
                viewForShake = time_from_edit_text).let { if (!it) return it }

        val timeTo = time_to_edit_text.text.toString().trim()
        time_to_edit_text.validate(
                timeTo.isNotEmpty(),
                viewForShake = time_to_edit_text).let { if (!it) return it }

        if (!calendarFrom.before(calendarTo)) {
            showErrorSnack("Начальное время не должно быть больше конечного")
            return false
        }

        return true
    }

    private fun initToolbar() {
        toolbarSizeListener.hideEmptyToolbarSpace(true)
        baseActivity.getActionBarView()?.showActionBar(false)

        back_button.getClick {
            onBackPressed()
        }
    }
}