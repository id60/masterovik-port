package noty_team.com.masterovik.ui.adapters.recycler

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import noty_team.com.masterovik.R
import noty_team.com.masterovik.ui.adapters.items_adapter.CanceledRequestItem

class CanceledRequestAdapter(val items: ArrayList<CanceledRequestItem>,
                             val onClick:()->Unit) :
        RecyclerView.Adapter<CanceledRequestAdapter.ViewHolder>(), View.OnClickListener {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_request_canceled, parent, false))
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder?.nameOrder.text = items.get(position).nameOrder
        holder?.dateOrder.text = items.get(position).dateOrder
        holder?.price.text = items.get(position).price.toString() + " руб."
        holder?.description.text = items.get(position).description


    }

    override fun onClick(v: View?) {
        when (v?.id) {
//            onClick()
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var nameOrder = view.findViewById<TextView>(R.id.name_request_canceled)
        var dateOrder = view.findViewById<TextView>(R.id.date_order_request_canceled)
        var price = view.findViewById<TextView>(R.id.price_request_canceled)
        var description = view.findViewById<TextView>(R.id.description_request_canceled)
    }
}