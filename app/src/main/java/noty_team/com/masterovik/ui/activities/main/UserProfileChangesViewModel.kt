package noty_team.com.masterovik.ui.activities.main

import android.app.Application
import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import noty_team.com.masterovik.api.request.profile.ProfileRequest
import noty_team.com.masterovik.api.response.profile.ProfileResponse
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.ui.adapters.items_adapter.ProvideServiceItem
import noty_team.com.masterovik.utils.Result
import noty_team.com.masterovik.utils.Success
import noty_team.com.masterovik.utils.paper.PaperIO
import javax.inject.Inject
import kotlin.reflect.KClass

class UserProfileChangesViewModel @Inject constructor(app: Application) : BaseViewModel(app) {

    val profileData = MutableLiveData<Result<ProfileResponse>>()

    fun getProfile(){
        api.profileApi.getProfile {
            if (it is Success) {
                PaperIO.setUserProfile(it.value)
            }
            profileData.postValue(it)
        }.call()
    }

    override fun onClear() {
        profileData.value = null
    }
}