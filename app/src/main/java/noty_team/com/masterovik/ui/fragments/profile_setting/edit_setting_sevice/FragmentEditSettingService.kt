package noty_team.com.masterovik.ui.fragments.profile_setting.edit_setting_sevice

import android.arch.lifecycle.ViewModelProvider
import android.support.v7.widget.GridLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.fragment_setting_service.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.api.request.services.AddServiceRequest
import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.ui.adapters.items_adapter.ProvideServiceItem
import noty_team.com.masterovik.ui.adapters.recycler.ProvideSeviceAdapter
import noty_team.com.masterovik.ui.fragments.profile_setting.edit_setting_sevice.create_service.CreateServiceFragment
import noty_team.com.masterovik.ui.fragments.profile_setting.edit_setting_sevice.services_list.ServicesListFragment
import noty_team.com.masterovik.utils.*
import noty_team.com.masterovik.utils.adapters.SpacingItemDecoration

class FragmentEditSettingService : BaseFragment<EditServiceFragmentViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): EditServiceFragmentViewModel {
        return injectViewModel(viewModelFactory)
    }

    companion object {
        fun newInstance() = FragmentEditSettingService()
    }

    override fun layout() = R.layout.fragment_setting_service

    private lateinit var servicesAdapter: ProvideSeviceAdapter

    override fun initialization(view: View, isFirstInit: Boolean) {
        initToolbar()

        if (isFirstInit){
            initClicks()
            initServicesAdapter()
            listenUpdates()

            getServices()
        }
    }

    private fun initClicks(){
        create_service.getClick {
            fragmentManager.addFragment(CreateServiceFragment.newInstance())
        }

        add_service_button.getClick {
            val serviceIds = servicesAdapter.getItemsList().flatMap { arrayListOf(it.id) } as ArrayList
            fragmentManager.addFragment(ServicesListFragment.newInstance(serviceIds))
        }
    }

    private fun initServicesAdapter() {
        recycler_setting_provide_service.layoutManager = GridLayoutManager(baseContext, 3)
        recycler_setting_provide_service.addItemDecoration(
                SpacingItemDecoration(3, convertDpToPixel(8f, baseContext).toInt(), true))
        servicesAdapter = ProvideSeviceAdapter(arrayListOf()){
            deleteService(it.id)
        }
        recycler_setting_provide_service.adapter = servicesAdapter
    }

    private fun getServices(){
        showProgress(true)
        viewModel.getServices()
    }

    private fun deleteService(id: Int){
        showProgress(true)
        viewModel.deleteService(AddServiceRequest( services = arrayListOf(id) ))
    }

    private fun listenUpdates() {
        viewModel.userServicesData.observe(this) { response ->
            showProgress(false)
            when (response) {
                is Success -> {
                    servicesAdapter.clear()
                    servicesAdapter.addAll(response.value.map())
                }
                is Failure -> {
                    showErrorSnack(response.errorMessage)
                }
            }
        }

        viewModel.deleteServiceData.observe(this) { response ->
            showProgress(false)
            when (response) {
                is Success -> {
                    response.value.request.services.let {
                        if (it.isNotEmpty()){
                            servicesAdapter.deleteItem(it[0])
                        }
                    }
                }
                is Failure -> {
                    showErrorSnack(response.errorMessage)
                }
            }
        }

        subscribeForResult {
            if (it.classProvider == ServicesListFragment::class) {
                servicesAdapter.addWithSorting(it.value as ArrayList<ProvideServiceItem>)
            }
        }

//        (injectViewModel(viewModelFactory) as MainActivityViewModel).returnData.apply {
//            observe(this@FragmentEditSettingService) {
//                if (it.classProvider == ServicesListFragment::class) {
//                    servicesAdapter.clear()
//                    servicesAdapter.addAll(it.value as ArrayList<ProvideServiceItem>)
//                }
//            }
//        }

    }

    private fun initToolbar(){
        baseActivity.getActionBarView()?.setupCenterText("Мои услуги")
        baseActivity.getActionBarView()?.showLeftButton(true)
        baseActivity.getActionBarView()?.setBackButton(layoutInflater)?.getClick {
            onBackPressed()
        }
    }

}