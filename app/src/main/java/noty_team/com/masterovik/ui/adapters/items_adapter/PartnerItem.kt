package noty_team.com.masterovik.ui.adapters.items_adapter

data class PartnerItem(var partnerName: String,
                       var date: String,
                       var activationType: PartnerActivationType){

    enum class PartnerActivationType(val typeString: String) {
        ACTIVATED("Активировал учетную запись"),
        NOT_ACTIVATED("Не активировал учетную запись")
    }

}