package noty_team.com.masterovik.ui.activities.splash

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import noty_team.com.masterovik.api.response.profile.ProfileResponse
import noty_team.com.masterovik.api.request.auth.refresh_token.RefreshTokenRequest
import noty_team.com.masterovik.api.request.profile.ProfileRequest
import noty_team.com.masterovik.api.response.auth.refresh_token.RefreshTokenResponse
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.utils.Result
import javax.inject.Inject

class SplashActivityViewModel @Inject constructor(app: Application) : BaseViewModel(app) {

    val refreshTokenData = MutableLiveData<Result<RefreshTokenResponse>>()

    fun refreshToken(request: RefreshTokenRequest){
        api.loginApi.refreshIoken(request) {
            refreshTokenData.postValue(it)
        }.call()
    }

    override fun onClear() {
        refreshTokenData.value = null
    }
}