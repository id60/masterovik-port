package noty_team.com.masterovik.ui.fragments.orders

import android.arch.lifecycle.ViewModelProvider
import android.view.LayoutInflater
import android.view.View
import kotlinx.android.synthetic.main.fragment_current_orders.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.ui.adapters.view_pager.OrderPagerAdapter
import noty_team.com.masterovik.ui.fragments.orders.canceled_order.CanceledFragment
import noty_team.com.masterovik.ui.fragments.orders.current_order.CurrentFragment
import noty_team.com.masterovik.ui.fragments.orders.done_order.DoneFragment
import noty_team.com.masterovik.ui.fragments.orders.filter_order.FilterFragment
import noty_team.com.masterovik.utils.injectViewModel

class OrderFragment : BaseFragment<BaseViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): BaseViewModel {
        return injectViewModel(viewModelFactory)
    }

    companion object {
        fun newInstance(): OrderFragment {
            return OrderFragment()
        }
    }

    override fun layout() = R.layout.fragment_current_orders

    override fun initialization(view: View, isFirstInit: Boolean) {
        initToolbar()

        if (isFirstInit) {
            initViewPager()
        }
    }

    private fun initViewPager() {
        view_pager_order.adapter = OrderPagerAdapter(childFragmentManager, view_pager_order,
                arrayListOf(CurrentFragment(), DoneFragment(), CanceledFragment()))
        tabLayout_order.setupWithViewPager(view_pager_order)
    }

    private fun initToolbar() {
        toolbarSizeListener.hideEmptyToolbarSpace(false)
        baseActivity.getActionBarView()?.showActionBar(true)
        baseActivity.getActionBarView()?.setLargeToolbarHeight()
        baseActivity.getActionBarView()?.setupCenterText("Заказы")
        baseActivity.getActionBarView()?.setSmallContainerWidth()
        baseActivity.getActionBarView()?.setupRightButton(LayoutInflater.from(baseActivity).inflate(R.layout.ab_right_menu, null))
        baseActivity.findViewById<View>(R.id.filter)?.getClick {
            fragmentManager.addFragment(FilterFragment.newInstance())

        }
        baseActivity.getActionBarView()?.showLeftButton(true)
        baseActivity.getActionBarView()?.setDrawerMenu(layoutInflater)?.getClick {
            onDrawerPressedListener.onDrawerPressed()
        }
    }
}
