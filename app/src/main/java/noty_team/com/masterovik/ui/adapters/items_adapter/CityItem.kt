package noty_team.com.masterovik.ui.adapters.items_adapter

data class CityItem (var id: Int = -1,
                     var cityName: String = "",
                     var isChecked: Boolean = false)