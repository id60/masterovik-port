package noty_team.com.masterovik.ui.adapters.recycler

import android.view.View
import kotlinx.android.synthetic.main.item_provide_service.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.ui.adapters.items_adapter.ProvideServiceItem
import noty_team.com.masterovik.utils.loadImage

class ProvideSeviceAdapter (list: ArrayList<ProvideServiceItem>,
                            val onDelete:(item: ProvideServiceItem)->Unit) :
        BaseAdapter<ProvideServiceItem, ProvideSeviceAdapter.ViewHolder>(list) {

    override fun createViewHolder(view: View, layoutId: Int) = ViewHolder(view)

    override fun getItemViewType(position: Int) = R.layout.item_provide_service

    inner class ViewHolder(view: View) : BaseAdapter.BaseViewHolder(view) {

        init {
            service_delete_item.getClick {
                onDelete(list[adapterPosition])
            }
        }

        override fun bind(pos: Int) {
            name_provide_service.text = list[pos].serviceName
            container_service.loadImage(list[pos].serviceUrl, loading_spinner)
        }
    }

    fun deleteItem(itemId: Int){
        var indexForRemove = -1
        list.forEachIndexed { index, item ->
            if (item.id == itemId){
                indexForRemove = index
                return@forEachIndexed
            }
        }
        if (indexForRemove >= 0) {
            list.removeAt(indexForRemove)
            notifyItemRemoved(indexForRemove)
        }
    }

    fun addWithSorting(data: ArrayList<ProvideServiceItem>){
        list.addAll(data)
        list.sortBy { it.id }
        notifyDataSetChanged()
    }
}