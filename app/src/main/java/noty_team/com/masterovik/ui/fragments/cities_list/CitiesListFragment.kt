package noty_team.com.masterovik.ui.fragments.cities_list

import android.arch.lifecycle.ViewModelProvider
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.fragment_cities_list.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.ui.adapters.recycler.CitiesListAdapter
import noty_team.com.masterovik.utils.*
import android.support.v7.widget.DividerItemDecoration

class CitiesListFragment : BaseFragment<CitiesListFragmentViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): CitiesListFragmentViewModel {
        return injectViewModel(viewModelFactory)
    }

    companion object {
        private const val CITIES_IDS_KEY = "CITIES_IDS_KEY"
        fun newInstance(cityId: Int = -1) = CitiesListFragment().apply {
            arguments = Bundle().apply {
                putInt(CITIES_IDS_KEY, cityId)
            }
        }
        fun getCityId(arguments: Bundle?): Int =
                arguments?.getInt(CITIES_IDS_KEY, -1)?:-1
    }

    override fun layout() = R.layout.fragment_cities_list

    private lateinit var citiesAdapter: CitiesListAdapter

    override fun initialization(view: View, isFirstInit: Boolean) {
        initToolbar()

        if (isFirstInit){
            initServicesAdapter()
            listenUpdates()

            getCities()
        }
    }

    private fun initServicesAdapter() {
        cities_list.layoutManager = LinearLayoutManager(baseContext)
        cities_list.addItemDecoration(DividerItemDecoration(baseActivity,
                LinearLayoutManager.VERTICAL))
        citiesAdapter = CitiesListAdapter(arrayListOf()){
            sendBackResult(it)
            onBackPressed()
        }
        cities_list.adapter = citiesAdapter
    }

    private fun getCities(){
        showProgress(true)
        viewModel.getCities()
    }

    private fun listenUpdates() {
        viewModel.citiesData.observe(this) { response ->
            showProgress(false)
            when (response) {
                is Success -> {
                    citiesAdapter.clear()
                    citiesAdapter.addAll(response.value.map().apply {
                        this.find { it.id == getCityId(arguments) }?.let {
                            it.isChecked = true
                        }
                    })
                }
                is Failure -> {
                    showErrorSnack(response.errorMessage)
                }
            }
        }
    }

    private fun initToolbar(){
        baseActivity.getActionBarView()?.showActionBar(true)
        toolbarSizeListener.hideEmptyToolbarSpace(false)
        baseActivity.getActionBarView()?.setupCenterText("Все города")
        baseActivity.getActionBarView()?.setSmallToolbarHeight()
        baseActivity.getActionBarView()?.showLeftButton(true)
        baseActivity.getActionBarView()?.setupLeftButton(layoutInflater.inflate(R.layout.ab_back, null))
        baseActivity.getActionBarView()?.getLeftButton()?.getClick {
            onBackPressed()
        }
        baseActivity.getActionBarView()?.setupRightButton(layoutInflater.inflate(R.layout.ab_empty, null))
    }
}