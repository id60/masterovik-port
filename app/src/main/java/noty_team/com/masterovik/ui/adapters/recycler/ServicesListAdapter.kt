package noty_team.com.masterovik.ui.adapters.recycler

import android.view.View
import kotlinx.android.synthetic.main.services_checked_item.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.ui.adapters.items_adapter.ProvideServiceItem
import noty_team.com.masterovik.utils.loadImage
import java.lang.Exception

class ServicesListAdapter (list: ArrayList<ProvideServiceItem>,
                           val onCheck:(item: ProvideServiceItem)->Unit) :
        BaseAdapter<ProvideServiceItem, ServicesListAdapter.ViewHolder>(list) {

    override fun createViewHolder(view: View, layoutId: Int) = ViewHolder(view)

    override fun getItemViewType(position: Int) = R.layout.services_checked_item

    inner class ViewHolder(view: View) : BaseAdapter.BaseViewHolder(view) {

        init {
            try {
                service_check_box.setOnCheckedChangeListener { buttonView, isChecked ->
                    list[adapterPosition].isChecked = isChecked
                    onCheck(list[adapterPosition])
                }
                itemView.getClick {
                    service_check_box.isChecked = !service_check_box.isChecked
                }
            }catch(e: Exception){}
        }

        override fun bind(pos: Int) {
            name_provide_service.text = list[pos].serviceName
            container_service.loadImage(list[pos].serviceUrl)
            service_check_box.isChecked = list[pos].isChecked
        }
    }

    fun getCheckedItems(): ArrayList<ProvideServiceItem> {
        return list.filter{ it.isChecked } as ArrayList<ProvideServiceItem>
    }

    fun addUniqueItems(list: ArrayList<ProvideServiceItem>, existingIds: ArrayList<Int>) {
        val unique = list.filter { it.id !in existingIds } as ArrayList
        addAll(unique)
    }
}