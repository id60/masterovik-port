package noty_team.com.masterovik.ui.fragments.wallet.partners

import android.arch.lifecycle.ViewModelProvider
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.fragmnet_parthner.*
import noty_team.com.masterovik.R

import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.ui.adapters.recycler.PartnerAdapter
import noty_team.com.masterovik.utils.*
import android.content.ClipData
import android.content.Context.CLIPBOARD_SERVICE
import android.content.ClipboardManager

class FragmentPartners : BaseFragment<PartnersFragmentViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): PartnersFragmentViewModel {
        return injectViewModel(viewModelFactory)
    }

    override fun layout() = R.layout.fragmnet_parthner

    private lateinit var partnerAdapter: PartnerAdapter

    override fun initialization(view: View, isFirstInit: Boolean) {
        if (isFirstInit){
            initAdapter()
            listenUpdates()

            getPartners()
        }
    }

    private fun getPartners(){
        showProgress(true)
        viewModel.getPartners()
    }

    private fun listenUpdates() {
        viewModel.partnersData.observe(this) { response ->
            showProgress(false)
            when (response) {
                is Success -> {
                    initReferralCode(response.value.referalCode)
                    partnerAdapter.clear()
                    partnerAdapter.addAll(response.value.map())
                }
                is Failure -> {
                    showErrorSnack(response.errorMessage)
                }
            }
            checkEmptyList()
        }
    }

    private fun initReferralCode(code: String){
        user_referral_code_text_view.getClick {
            (baseContext.getSystemService(CLIPBOARD_SERVICE) as ClipboardManager?)?.let { clipboard ->
                val clip = ClipData.newPlainText("ReferralCode", code)
                clipboard.primaryClip = clip
                showSuccessSnack("Код скопирован")
            }
        }
        user_referral_code_text_view.text = code
    }

    private fun checkEmptyList(){
        if (partnerAdapter.itemCount > 0) empty_list_text_view.gone()
        else empty_list_text_view.visible()
    }

    private fun initAdapter(){
        recycler_partner.layoutManager = LinearLayoutManager(baseActivity)
        partnerAdapter = PartnerAdapter(arrayListOf())
        recycler_partner.adapter = partnerAdapter
    }

}