package noty_team.com.masterovik.ui.fragments.orders.current_order.order.change_date_order

import android.arch.lifecycle.ViewModelProvider
import android.view.View
import noty_team.com.masterovik.R

import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.utils.injectViewModel

class FragmentChangeDateOrder : BaseFragment<BaseViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): BaseViewModel {
        return injectViewModel(viewModelFactory)
    }

    companion object {
        fun newInstance() = FragmentChangeDateOrder()
    }

    override fun layout() = R.layout.fragment_change_date_order

    override fun initialization(view: View, isFirstInit: Boolean) {
        baseActivity.findViewById<View>(R.id.cross_change_date_order).setOnClickListener {
//            baseActivity.navigationBackManager.navigateBack()
            onBackPressed()
        }
    }
}