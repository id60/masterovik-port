package noty_team.com.masterovik.ui.adapters.recycler

import android.annotation.SuppressLint
import android.support.v4.content.ContextCompat
import android.view.View
import kotlinx.android.synthetic.main.item_history_wallet.*
import noty_team.com.masterovik.R

import noty_team.com.masterovik.ui.adapters.items_adapter.HistoryPaymentItem
import java.lang.Exception

class HistoryPaymentAdapter(list: ArrayList<HistoryPaymentItem>) :
        BaseAdapter<HistoryPaymentItem, HistoryPaymentAdapter.ViewHolder>(list) {

    override fun createViewHolder(view: View, layoutId: Int) = ViewHolder(view)

    override fun getItemViewType(position: Int) = R.layout.item_history_wallet

    inner class ViewHolder(view: View) : BaseAdapter.BaseViewHolder(view) {

        init {
            try {

            } catch (e: Exception) {
            }
        }

        override fun bind(pos: Int) {
            val context = itemView.context
            if (list[pos].type == HistoryPaymentItem.OperationType.REFILL) {
                image_card_history.setImageResource(R.drawable.ic_023_card)
                parameter_history.text = "Пополнение баланса"
                card_name_history.visibility = View.VISIBLE
                card_name_history.text = list[pos].cardNumber
                sum_history.text = list[pos].amount.toString() + " руб."
                sum_history.setTextColor(ContextCompat.getColor(context, R.color.colorGreen))
            } else {
                image_card_history.setImageResource(R.drawable.ic_022_withdraw)
                parameter_history.text = "Списание с \nвнутреннего баланса"
                card_name_history.visibility = View.GONE

                sum_history.text = "-" + list[pos].amount.toString() + " руб."
                sum_history.setTextColor(ContextCompat.getColor(context, R.color.colorOrange))
            }
            balance_history.text = "Баланс: " + list[pos].balance.toString() + " руб."
            date_history.text = list[pos].date
        }
    }

}