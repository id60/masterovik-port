package noty_team.com.masterovik.ui.adapters.recycler

import android.support.v4.content.ContextCompat
import android.view.View
import kotlinx.android.synthetic.main.item_parthner.*
import noty_team.com.masterovik.R

import noty_team.com.masterovik.ui.adapters.items_adapter.PartnerItem
import java.lang.Exception

class PartnerAdapter(list: ArrayList<PartnerItem>):
        BaseAdapter<PartnerItem, PartnerAdapter.ViewHolder>(list) {

    override fun createViewHolder(view: View, layoutId: Int) = ViewHolder(view)

    override fun getItemViewType(position: Int) = R.layout.item_parthner

    inner class ViewHolder(view: View) : BaseAdapter.BaseViewHolder(view) {

        init {
            try {

            } catch (e: Exception) {
            }
        }

        override fun bind(pos: Int) {
            val context = itemView.context
            partner_user_name.text = list[pos].partnerName
            partner_date.text = list[pos].date

            if (list[pos].activationType == PartnerItem.PartnerActivationType.ACTIVATED) {
                bottom_hint.background = context.getDrawable(R.drawable.background_items_in_work)
                activated_info.text = context.getString(R.string.partner_activated)
                activated_info.setTextColor(ContextCompat.getColor(context, R.color.colorGreen))
            } else {

                bottom_hint.background = context.getDrawable(R.drawable.background_gray_bottom_partner)
                activated_info.text = context.getString(R.string.partner_not_activated)
                activated_info.setTextColor(ContextCompat.getColor(context, R.color.colorWhite))
            }

        }
    }

}