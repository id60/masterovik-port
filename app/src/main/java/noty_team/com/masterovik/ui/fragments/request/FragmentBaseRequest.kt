package noty_team.com.masterovik.ui.fragments.request

import android.arch.lifecycle.ViewModelProvider
import android.support.design.widget.TabLayout
import android.view.LayoutInflater
import android.view.View
import kotlinx.android.synthetic.main.fragment_base_request.*
import noty_team.com.masterovik.R

import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.ui.adapters.view_pager.RequestPageAdapter
import noty_team.com.masterovik.ui.fragments.orders.filter_order.FilterFragment
import noty_team.com.masterovik.ui.fragments.request.canceled.FragmentCanceledRequest
import noty_team.com.masterovik.ui.fragments.request.in_work.FragmentInWorkRequest
import noty_team.com.masterovik.utils.injectViewModel

class FragmentBaseRequest : BaseFragment<BaseViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): BaseViewModel {
        return injectViewModel(viewModelFactory)
    }

    companion object {
        fun newInstance() = FragmentBaseRequest()
    }


    override fun layout() = R.layout.fragment_base_request

    override fun initialization(view: View, isFirstInit: Boolean) {
        initToolbar()

        view_pager_request.adapter = RequestPageAdapter(childFragmentManager, view_pager_request,
                arrayListOf(FragmentInWorkRequest(), FragmentCanceledRequest()))
        tabLayout_request.setupWithViewPager(view_pager_request)

        tabLayout_request.addOnTabSelectedListener(object :
                TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                if (tab.position == 0) {

                    baseActivity.findViewById<View>(R.id.filter).visibility = View.VISIBLE

                } else {
                    baseActivity.findViewById<View>(R.id.filter).visibility = View.GONE
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })


    }

    private fun initToolbar() {
        toolbarSizeListener.hideEmptyToolbarSpace(false)
        baseActivity.getActionBarView()?.showActionBar(true)
        baseActivity.getActionBarView()?.setupCenterText("Заявки")

        baseActivity.getActionBarView()?.setLargeToolbarHeight()
        baseActivity.getActionBarView()?.setSmallContainerWidth()
        toolbarSizeListener.resizeContainerLarge()

        baseActivity.getActionBarView()?.setupRightButton(LayoutInflater.from(baseActivity).inflate(R.layout.ab_right_filter, null))

        baseActivity.findViewById<View>(R.id.filter).setOnClickListener {
            fragmentManager.addFragment(FilterFragment.newInstance())
            toolbarSizeListener.resizeContainerSmall()
        }

        baseActivity.getActionBarView()?.showLeftButton(true)
        baseActivity.getActionBarView()?.setDrawerMenu(layoutInflater)?.getClick {
            onDrawerPressedListener.onDrawerPressed()
        }
    }

}