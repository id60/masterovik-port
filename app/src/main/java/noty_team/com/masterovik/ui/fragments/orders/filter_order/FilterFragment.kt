package noty_team.com.masterovik.ui.fragments.orders.filter_order

import android.arch.lifecycle.ViewModelProvider
import android.view.View
import kotlinx.android.synthetic.main.fragment_filter_order.*
import noty_team.com.masterovik.R

import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.utils.injectViewModel

class FilterFragment : BaseFragment<BaseViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): BaseViewModel {
        return injectViewModel(viewModelFactory)
    }

    companion object {
        fun newInstance(): FilterFragment {
            return FilterFragment()
        }
    }

    override fun layout() = R.layout.fragment_filter_order

    override fun initialization(view: View, isFirstInit: Boolean) {

        toolbarSizeListener.hideEmptyToolbarSpace(true)
        baseActivity.getActionBarView()?.showActionBar(false)
        baseActivity.getActionBarView()?.setSmallToolbarHeight()

        cross_filter_order.setOnClickListener {
            onBackPressed()
        }

    }

}