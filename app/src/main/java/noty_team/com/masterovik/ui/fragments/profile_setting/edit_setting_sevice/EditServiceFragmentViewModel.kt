package noty_team.com.masterovik.ui.fragments.profile_setting.edit_setting_sevice

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import noty_team.com.masterovik.api.request.services.AddServiceRequest
import noty_team.com.masterovik.api.response.BaseResponse
import noty_team.com.masterovik.api.response.services.get_services.GetServicesResponse
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.utils.Failure
import noty_team.com.masterovik.utils.Result
import noty_team.com.masterovik.utils.Success
import javax.inject.Inject

class EditServiceFragmentViewModel @Inject constructor(app: Application) : BaseViewModel(app) {

    data class ServiceDeleteResponse(val request: AddServiceRequest, val baseResponse: BaseResponse)

    val userServicesData = MutableLiveData<Result<GetServicesResponse>>()
    val deleteServiceData = MutableLiveData<Result<ServiceDeleteResponse>>()

    fun getServices(){
        api.servicesApi.getUserServices() {
            userServicesData.postValue(it)
        }.call()
    }

    fun deleteService(data: AddServiceRequest){
        api.profileApi.deleteService(data) {
            if (it is Success) deleteServiceData.postValue(Success(ServiceDeleteResponse(data, it.value)))
            else if (it is Failure) deleteServiceData.postValue(Failure(it.errorMessage, it.errorCode))
        }.call()
    }

    override fun onClear() {
        userServicesData.value = null
        deleteServiceData.value = null
    }
}