package noty_team.com.masterovik.ui.fragments.wallet.partners

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import noty_team.com.masterovik.api.request.services.AddServiceRequest
import noty_team.com.masterovik.api.request.wallet.add_card.AddCardRequest
import noty_team.com.masterovik.api.request.wallet.history.PaymentHistoryRequest
import noty_team.com.masterovik.api.response.BaseResponse
import noty_team.com.masterovik.api.response.services.get_services.GetServicesResponse
import noty_team.com.masterovik.api.response.wallet.add_card.AddCardResponse
import noty_team.com.masterovik.api.response.wallet.history.PaymentHistoryResponse
import noty_team.com.masterovik.api.response.wallet.my_cards.MyCardsResponse
import noty_team.com.masterovik.api.response.wallet.partners.PartnersResponse
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.utils.Failure
import noty_team.com.masterovik.utils.Result
import noty_team.com.masterovik.utils.Success
import javax.inject.Inject

class PartnersFragmentViewModel @Inject constructor(app: Application) : BaseViewModel(app) {

    val partnersData = MutableLiveData<Result<PartnersResponse>>()

    fun getPartners(){
        api.walletApi.getPartners {
            partnersData.postValue(it)
        }.call()
    }

    override fun onClear() {
        partnersData.value = null
    }
}