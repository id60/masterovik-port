package noty_team.com.masterovik.ui.fragments.wallet.my_cards

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import noty_team.com.masterovik.api.request.wallet.delete_card.DeleteCardRequest
import noty_team.com.masterovik.api.response.BaseResponse
import noty_team.com.masterovik.api.response.wallet.delete_card.DeleteCardResponse
import noty_team.com.masterovik.api.response.wallet.my_cards.MyCardsResponse
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.utils.Failure
import noty_team.com.masterovik.utils.Result
import noty_team.com.masterovik.utils.Success
import javax.inject.Inject

class MyCardsFragmentViewModel @Inject constructor(app: Application) : BaseViewModel(app) {

    data class CardDeleteResponse(val request: DeleteCardRequest, val baseResponse: BaseResponse)

    val deleteCardData = MutableLiveData<Result<CardDeleteResponse>>()

    fun deleteCard(data: DeleteCardRequest){
        api.walletApi.deleteCard(data) {
            if (it is Success) deleteCardData.postValue(Success(CardDeleteResponse(data, it.value)))
            else if (it is Failure) deleteCardData.postValue(Failure(it.errorMessage, it.errorCode))
        }.call()
    }

    override fun onClear() {
        deleteCardData.value = null
    }
}