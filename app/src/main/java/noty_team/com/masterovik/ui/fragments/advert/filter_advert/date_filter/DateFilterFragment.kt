package noty_team.com.masterovik.ui.fragments.advert.filter_advert.date_filter

import android.arch.lifecycle.ViewModelProvider
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.fragment_filter_date.*
import noty_team.com.masterovik.R

import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import java.lang.NumberFormatException
import java.util.*
import kotlin.collections.ArrayList
import android.app.DatePickerDialog
import android.app.Activity
import noty_team.com.masterovik.utils.*
import java.text.SimpleDateFormat
import java.text.ParseException

class DateFilterFragment : BaseFragment<BaseViewModel>() {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): BaseViewModel {
        return injectViewModel(viewModelFactory)
    }

    companion object {
        private const val DATES_KEY = "DATES_KEY"
        fun newInstance(dates: ArrayList<Long> = arrayListOf()): DateFilterFragment = DateFilterFragment().apply {
            arguments = Bundle().apply {
                putLongArray(DATES_KEY, dates.toLongArray())
            }
        }

        fun getDates(arguments: Bundle?): ArrayList<Long> =
                arguments?.getLongArray(DATES_KEY)?.toCollection(arrayListOf()) ?: arrayListOf()
    }

    override fun layout() = R.layout.fragment_filter_date

    private val calendarFrom = Calendar.getInstance(getLocale())
    private val calendarTo = Calendar.getInstance(getLocale())

    override fun initialization(view: View, isFirstInit: Boolean) {
        initToolbar()
        if (isFirstInit) {
            initCalendars()
            initDates()
            initClicks()
        }
    }

    private fun initCalendars() {
        calendarFrom.set(Calendar.MILLISECOND, 0)
        calendarFrom.set(Calendar.SECOND, 0)
        calendarFrom.set(Calendar.MINUTE, 0)
        calendarFrom.set(Calendar.HOUR_OF_DAY, 0)

        calendarTo.set(Calendar.MILLISECOND, 0)
        calendarTo.set(Calendar.SECOND, 0)
        calendarTo.set(Calendar.MINUTE, 0)
        calendarTo.set(Calendar.HOUR_OF_DAY, 0)
    }

    private fun initDates() {
        getDates(arguments).let {
            if (it.size >= 2) {
                val dateFrom = CustomDatePicker.getFormattedDateString(it[0])
                val dateTo = CustomDatePicker.getFormattedDateString(it[1])
                date_from_edit_text.setText(dateFrom)
                date_to_edit_text.setText(dateTo)
            }
        }
        date_from_edit_text.getClick {
            CustomDatePicker.show(baseActivity, calendarFrom) {
                date_from_edit_text.setText(it)
            }
        }
        date_to_edit_text.getClick {
            CustomDatePicker.show(baseActivity, calendarTo) {
                date_to_edit_text.setText(it)
            }
        }
    }

    private fun initClicks() {
        button_save.getClick {
            if (validate()) {
                val dateFromTimestamp = calendarFrom.timeInMillis
                val dateToTimestamp = calendarTo.timeInMillis
                sendBackResult(arrayListOf(dateFromTimestamp, dateToTimestamp))
                onBackPressed()
            }
        }
    }

    private fun validate(): Boolean {
        date_from_edit_text.setError(false)
        date_to_edit_text.setError(false)

        val dateFrom = date_from_edit_text.text.toString().trim()
        date_from_edit_text.validate(
                dateFrom.isNotEmpty(),
                viewForShake = date_from_edit_text).let { if (!it) return it }

        val dateTo = date_to_edit_text.text.toString().trim()
        date_to_edit_text.validate(
                dateTo.isNotEmpty(),
                viewForShake = date_to_edit_text).let { if (!it) return it }

        if (!calendarFrom.before(calendarTo)) {
            showErrorSnack("Начальная дата не должна быть больше конечной")
            return false
        }

        return true
    }

    private fun initToolbar() {
        toolbarSizeListener.hideEmptyToolbarSpace(true)
        baseActivity.getActionBarView()?.showActionBar(false)

        back_button.getClick {
            onBackPressed()
        }
    }
}