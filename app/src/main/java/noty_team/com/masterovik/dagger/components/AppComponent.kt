package noty_team.com.masterovik.dagger.components

import android.app.Application
import dagger.Component
import noty_team.com.masterovik.api.retrofit.BaseRepository
import noty_team.com.masterovik.api.retrofit.CallbackWrapper
import noty_team.com.masterovik.api.retrofit.apis.MasterovikApi
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.dagger.modules.*
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AppModule::class,
    RetrofitApiModule::class,
    ViewModelFactoryModule::class
])
interface AppComponent {
    fun plus(fragmentModule: FragmentModule): FragmentComponent

    fun inject(app: Application)
    fun inject(target: CallbackWrapper<Any, Any>)
    fun inject(target: MasterovikApi)
    fun inject(target: BaseRepository)
    fun inject(target: BaseViewModel)
}
