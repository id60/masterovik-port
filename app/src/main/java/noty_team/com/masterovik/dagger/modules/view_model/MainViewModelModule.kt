package noty_team.com.masterovik.dagger.modules.view_model

import android.arch.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import noty_team.com.masterovik.dagger.map_key.ViewModelKey
import noty_team.com.masterovik.dagger.scopes.FragmentScope
import noty_team.com.masterovik.ui.activities.login.LoginActivityViewModel
import noty_team.com.masterovik.ui.activities.main.BackResultViewModel
import noty_team.com.masterovik.ui.activities.main.MainActivityViewModel
import noty_team.com.masterovik.ui.activities.main.UserProfileChangesViewModel
import noty_team.com.masterovik.ui.activities.splash.SplashActivityViewModel
import noty_team.com.masterovik.ui.fragments.login.entrance.EntranceFragmentViewModel
import noty_team.com.masterovik.ui.fragments.login.registration.RegistrationFragmentViewModel
import noty_team.com.masterovik.ui.fragments.login.selection_role.SelectionRoleFragmentViewModel
import noty_team.com.masterovik.ui.fragments.login.terms_agreement.TermsFragmentViewModel

@Module
abstract class MainViewModelModule {

    @Binds
    @FragmentScope
    @IntoMap
    @ViewModelKey(MainActivityViewModel::class)
    abstract fun bindMainActivityViewModel(model: MainActivityViewModel): ViewModel

    @Binds
    @FragmentScope
    @IntoMap
    @ViewModelKey(BackResultViewModel::class)
    abstract fun bindBackResultViewModel(model: BackResultViewModel): ViewModel

    @Binds
    @FragmentScope
    @IntoMap
    @ViewModelKey(UserProfileChangesViewModel::class)
    abstract fun bindUserProfileChangesViewModel(model: UserProfileChangesViewModel): ViewModel
}