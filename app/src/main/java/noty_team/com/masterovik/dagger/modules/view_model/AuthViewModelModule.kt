package noty_team.com.masterovik.dagger.modules.view_model

import android.arch.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import noty_team.com.masterovik.dagger.map_key.ViewModelKey
import noty_team.com.masterovik.dagger.scopes.FragmentScope
import noty_team.com.masterovik.ui.activities.login.LoginActivityViewModel
import noty_team.com.masterovik.ui.activities.main.MainActivityViewModel
import noty_team.com.masterovik.ui.activities.splash.SplashActivityViewModel
import noty_team.com.masterovik.ui.fragments.login.entrance.EntranceFragmentViewModel
import noty_team.com.masterovik.ui.fragments.login.registration.RegistrationFragmentViewModel
import noty_team.com.masterovik.ui.fragments.login.selection_role.SelectionRoleFragmentViewModel
import noty_team.com.masterovik.ui.fragments.login.terms_agreement.TermsFragmentViewModel

@Module
abstract class AuthViewModelModule {

    @Binds
    @FragmentScope
    @IntoMap
    @ViewModelKey(SplashActivityViewModel::class)
    abstract fun bindSplashActivityViewModel(model: SplashActivityViewModel): ViewModel

    @Binds
    @FragmentScope
    @IntoMap
    @ViewModelKey(LoginActivityViewModel::class)
    abstract fun bindLoginActivityViewModel(model: LoginActivityViewModel): ViewModel

    @Binds
    @FragmentScope
    @IntoMap
    @ViewModelKey(EntranceFragmentViewModel::class)
    abstract fun bindEntranceFragmentViewModel(model: EntranceFragmentViewModel): ViewModel

    @Binds
    @FragmentScope
    @IntoMap
    @ViewModelKey(SelectionRoleFragmentViewModel::class)
    abstract fun bindSelectionRoleFragmentViewModel(model: SelectionRoleFragmentViewModel): ViewModel

    @Binds
    @FragmentScope
    @IntoMap
    @ViewModelKey(RegistrationFragmentViewModel::class)
    abstract fun bindRegistrationFragmentViewModel(model: RegistrationFragmentViewModel): ViewModel

    @Binds
    @FragmentScope
    @IntoMap
    @ViewModelKey(TermsFragmentViewModel::class)
    abstract fun bindTermsFragmentViewModel(model: TermsFragmentViewModel): ViewModel
}