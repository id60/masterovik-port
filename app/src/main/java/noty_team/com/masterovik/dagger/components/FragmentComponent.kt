package noty_team.com.masterovik.dagger.components

import android.arch.lifecycle.ViewModelProvider
import dagger.Subcomponent
import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.base.BaseActivity
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.dagger.modules.FragmentModule
import noty_team.com.masterovik.dagger.modules.view_model.*
import noty_team.com.masterovik.dagger.scopes.FragmentScope
import noty_team.com.masterovik.ui.dialogs.BaseFragmentDialog

@FragmentScope
@Subcomponent(modules = [FragmentModule::class,
    AuthViewModelModule::class,
    MainViewModelModule::class,
    ProfileViewModelModule::class,
    AdvertsViewModelModule::class,
    WalletViewModelModule::class])
interface FragmentComponent {
    fun inject(target: BaseActivity<BaseViewModel>)
    fun inject(target: BaseFragment<BaseViewModel>)
    fun inject(target: BaseFragmentDialog<BaseViewModel>)

    fun daggerViewModelFactory(): ViewModelProvider.Factory
}