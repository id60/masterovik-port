package noty_team.com.masterovik.dagger.modules.view_model

import android.arch.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import noty_team.com.masterovik.dagger.map_key.ViewModelKey
import noty_team.com.masterovik.dagger.scopes.FragmentScope
import noty_team.com.masterovik.ui.fragments.wallet.add_card.AddCardFragmentViewModel
import noty_team.com.masterovik.ui.fragments.wallet.cards_list.CardsListFragmentViewModel
import noty_team.com.masterovik.ui.fragments.wallet.history.HistoryFragmentViewModel
import noty_team.com.masterovik.ui.fragments.wallet.my_cards.MyCardsFragmentViewModel
import noty_team.com.masterovik.ui.fragments.wallet.partners.PartnersFragmentViewModel
import noty_team.com.masterovik.ui.fragments.wallet.sorting_history.HistorySortingFragmentViewModel
import noty_team.com.masterovik.ui.fragments.wallet.top_up_balance.TopUpBalanceFragmentViewModel

@Module
abstract class WalletViewModelModule {

    @Binds
    @FragmentScope
    @IntoMap
    @ViewModelKey(MyCardsFragmentViewModel::class)
    abstract fun bindMyCardsFragmentViewModel(model: MyCardsFragmentViewModel): ViewModel

    @Binds
    @FragmentScope
    @IntoMap
    @ViewModelKey(TopUpBalanceFragmentViewModel::class)
    abstract fun bindTopUpBalanceFragmentViewModel(model: TopUpBalanceFragmentViewModel): ViewModel

    @Binds
    @FragmentScope
    @IntoMap
    @ViewModelKey(AddCardFragmentViewModel::class)
    abstract fun bindAddCardFragmentViewModel(model: AddCardFragmentViewModel): ViewModel

    @Binds
    @FragmentScope
    @IntoMap
    @ViewModelKey(HistoryFragmentViewModel::class)
    abstract fun bindHistoryFragmentViewModel(model: HistoryFragmentViewModel): ViewModel

    @Binds
    @FragmentScope
    @IntoMap
    @ViewModelKey(CardsListFragmentViewModel::class)
    abstract fun bindCardsListFragmentViewModel(model: CardsListFragmentViewModel): ViewModel

    @Binds
    @FragmentScope
    @IntoMap
    @ViewModelKey(PartnersFragmentViewModel::class)
    abstract fun bindPartnersFragmentViewModel(model: PartnersFragmentViewModel): ViewModel

    @Binds
    @FragmentScope
    @IntoMap
    @ViewModelKey(HistorySortingFragmentViewModel::class)
    abstract fun bindHistorySortingFragmentViewModel(model: HistorySortingFragmentViewModel): ViewModel
}