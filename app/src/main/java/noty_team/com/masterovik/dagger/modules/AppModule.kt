package noty_team.com.masterovik.dagger.modules

import android.app.Application
import android.content.Context
import android.widget.Toast
import com.github.vacxe.phonemask.PhoneMaskManager
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import noty_team.com.masterovik.api.retrofit.api_interface.*
import noty_team.com.masterovik.api.retrofit.apis.*
import noty_team.com.masterovik.utils.validator.FieldValidator
import noty_team.com.masterovik.utils.validator.FieldsValidationInterface
import retrofit2.Retrofit
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import javax.inject.Singleton

@Module
class AppModule(private val app: Application) {

    @Provides
    @Singleton
    fun provideApplication(): Application = app

    @Provides
    @Singleton
    fun provideContext(): Context = app.applicationContext

    @Provides
    @Singleton
    fun provideToast(context: Context): Toast = Toast.makeText(context, "", Toast.LENGTH_LONG)

    @Provides
    fun provideRequestsDisposable() = CompositeDisposable()

    @Provides
    @Singleton
    fun provideMasterovikApi(context: Context): MasterovikApi = MasterovikApi(context)
    @Provides
    @Singleton
    fun provideServicesApi(context: Context, retrofit: Retrofit, api: ServicesApiInterface):
            ServicesApi = ServicesApi(context, retrofit, api)
    @Provides
    @Singleton
    fun provideCityApi(context: Context, retrofit: Retrofit, api: CityApiInterface):
            CityApi = CityApi(context, retrofit, api)
    @Provides
    @Singleton
    fun provideProfileApi(context: Context, retrofit: Retrofit, api: ProfileApiInterface):
            ProfileApi = ProfileApi(context, retrofit, api)
    @Provides
    @Singleton
    fun provideLoginApi(context: Context, retrofit: Retrofit, api: LoginApiInterface):
            LoginApi = LoginApi(context, retrofit, api)
    @Provides
    @Singleton
    fun provideAdvertsApi(context: Context, retrofit: Retrofit, api: AdvertsApiInterface):
            AdvertsApi = AdvertsApi(context, retrofit, api)
    @Provides
    @Singleton
    fun provideWalletApi(context: Context, retrofit: Retrofit, api: WalletApiInterface):
            WalletApi = WalletApi(context, retrofit, api)

    @Provides
    @Singleton
    fun provideCicerone(): Cicerone<Router> = Cicerone.create()

    @Provides
    @Singleton
    fun provideCiceroneRouter(cicerone: Cicerone<Router>): Router = cicerone.router

    @Provides
    @Singleton
    fun provideCiceroneNavigator(cicerone: Cicerone<Router>): NavigatorHolder = cicerone.navigatorHolder

    @Provides
    @Singleton
    fun providePhoneMaskManager(): PhoneMaskManager =
            PhoneMaskManager()
                    .withMask("# ### #### ## ##")
                    .withRegion("+")

    @Provides
    @Singleton
    fun provideFieldValidator(): FieldsValidationInterface = FieldValidator()

}