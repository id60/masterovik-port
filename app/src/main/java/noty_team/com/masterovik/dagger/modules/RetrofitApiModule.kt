package noty_team.com.masterovik.dagger.modules

import android.app.Application
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import noty_team.com.masterovik.BuildConfig
import noty_team.com.masterovik.api.retrofit.api_interface.*
import noty_team.com.masterovik.utils.paper.PaperIO
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import okio.Buffer
import org.json.JSONObject
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.lang.Exception
import java.net.CookieManager
import java.net.CookiePolicy
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class RetrofitApiModule {
    companion object {
        private const val CONNECTION_TIMEOUT_SEC = 30L
        private const val CACHE_SIZE = 10_000_000L
        private const val CACHE_FILE_NAME = "http_cache"
    }

    @Provides
    @Singleton
    fun provideHttpCache(application: Application): Cache {
        val cacheSize = 10L * 1024 * 1024
        return Cache(application.cacheDir, cacheSize)
    }

    @Provides
    @Singleton
    fun provideGson(): Gson {
        val gsonBuilder = GsonBuilder()
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        return gsonBuilder.create()
    }

    @Provides
    @Singleton
    fun provideOkhttpClient(cache: Cache): OkHttpClient {
        return OkHttpClient.Builder().apply {
            cache(cache)

            val cookieManager = CookieManager()
            cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL)
            cookieJar(JavaNetCookieJar(cookieManager))

            if (BuildConfig.DEBUG) {
                val httpLoggingInterceptor = HttpLoggingInterceptor()
                httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
                addInterceptor(httpLoggingInterceptor)
            }

            connectTimeout(CONNECTION_TIMEOUT_SEC, TimeUnit.SECONDS)
            readTimeout(CONNECTION_TIMEOUT_SEC, TimeUnit.SECONDS)
            writeTimeout(CONNECTION_TIMEOUT_SEC, TimeUnit.SECONDS)
            retryOnConnectionFailure(true)

            addInterceptor { chain ->
                var request = chain.request()
                val token = PaperIO.getAccessToken()

                var requestBody = request.body()
                try {
                    val customReq = bodyToString(requestBody)
                    val obj = JSONObject(customReq)

                    if (obj.has("access_token")) {
                        obj.put("access_token", token)

                        requestBody = RequestBody.create(requestBody?.contentType(), obj.toString());
                        if (requestBody != null) {
                            val requestBuilder = request.newBuilder()
                            request = requestBuilder
                                    .post(requestBody)
                                    .build()
                        }
                    }
                }catch (e: Exception){}

                val builder = request.newBuilder()
                if (token.isNotEmpty()) {
                    if (request.url().toString().contains(BuildConfig.SERVER_URL))
                        builder.header("Authorization", "Bearer $token")
                }
                builder.header("Content-Type", "application/json")

                val response = chain.proceed(builder.build())

//            val allHeaders = response.headers()
//            response.body()?.let { responseBody ->
//                try {
//                    val bodyJson = JSONObject(responseBody.string())
//                    if (bodyJson.has("data") && bodyJson.has("response")){
//                        if (!bodyJson.getBoolean("response")){
//                            bodyJson.remove("data")
//                        }
//                        else {
//                            allHeaders["X-Items-Total"]?.let { total ->
//                                allHeaders["X-Items-Count"]?.let { count ->
//                                    bodyJson.put("total_items", total)
//                                    bodyJson.put("items_count", count)
//                                }
//                            }
//                        }
//                    }
//                    val newBody = ResponseBody.create(responseBody.contentType(), bodyJson.toString())
//                    response = response.newBuilder()
//                            .headers(allHeaders)
//                            .body(newBody)
//                            .build()
//                } catch (e: Exception) {
//                }
//            }

                response
            }
        }.build()
    }

    private fun bodyToString(request: RequestBody?): String {
        try {
            val buffer = Buffer()
            if (request != null)
                request.writeTo(buffer)
            else
                return ""
            return buffer.readUtf8()
        } catch (e: IOException) {
            return "did not work"
        }
    }

    @Provides
    @Singleton
    fun provideRetrofit(gson: Gson, okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
                .baseUrl(BuildConfig.SERVER_URL)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
    }

    @Provides
    @Singleton
    fun provideLoginApiImplementation(retrofit: Retrofit): LoginApiInterface {
        return retrofit.create(LoginApiInterface::class.java)
    }

    @Provides
    @Singleton
    fun provideCityApiImplementation(retrofit: Retrofit): CityApiInterface {
        return retrofit.create(CityApiInterface::class.java)
    }

    @Provides
    @Singleton
    fun provideProfileApiImplementation(retrofit: Retrofit): ProfileApiInterface {
        return retrofit.create(ProfileApiInterface::class.java)
    }

    @Provides
    @Singleton
    fun provideServicesApiImplementation(retrofit: Retrofit): ServicesApiInterface {
        return retrofit.create(ServicesApiInterface::class.java)
    }

    @Provides
    @Singleton
    fun provideAdvertsApiImplementation(retrofit: Retrofit): AdvertsApiInterface {
        return retrofit.create(AdvertsApiInterface::class.java)
    }

    @Provides
    @Singleton
    fun provideWalletApiImplementation(retrofit: Retrofit): WalletApiInterface {
        return retrofit.create(WalletApiInterface::class.java)
    }
}