package noty_team.com.masterovik.dagger.modules.view_model

import android.arch.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import noty_team.com.masterovik.dagger.map_key.ViewModelKey
import noty_team.com.masterovik.dagger.scopes.FragmentScope
import noty_team.com.masterovik.ui.fragments.profile_setting.edit_email.EditEmailFragmentViewModel
import noty_team.com.masterovik.ui.fragments.profile_setting.edit_personal_data.ProfileFragmentViewModel
import noty_team.com.masterovik.ui.fragments.profile_setting.edit_phone_number.EditPhoneFragmentViewModel
import noty_team.com.masterovik.ui.fragments.profile_setting.edit_setting_sevice.EditServiceFragmentViewModel
import noty_team.com.masterovik.ui.fragments.profile_setting.edit_setting_sevice.create_service.CreateServiceFragmentViewModel
import noty_team.com.masterovik.ui.fragments.profile_setting.edit_setting_sevice.services_list.ServicesListFragmentViewModel

@Module
abstract class ProfileViewModelModule {

    @Binds
    @FragmentScope
    @IntoMap
    @ViewModelKey(ProfileFragmentViewModel::class)
    abstract fun bindProfileFragmentViewModel(model: ProfileFragmentViewModel): ViewModel

    @Binds
    @FragmentScope
    @IntoMap
    @ViewModelKey(EditPhoneFragmentViewModel::class)
    abstract fun bindEditPhoneFragmentViewModel(model: EditPhoneFragmentViewModel): ViewModel

    @Binds
    @FragmentScope
    @IntoMap
    @ViewModelKey(EditEmailFragmentViewModel::class)
    abstract fun bindEditEmailFragmentViewModel(model: EditEmailFragmentViewModel): ViewModel

    @Binds
    @FragmentScope
    @IntoMap
    @ViewModelKey(EditServiceFragmentViewModel::class)
    abstract fun bindEditServiceFragmentViewModel(model: EditServiceFragmentViewModel): ViewModel

    @Binds
    @FragmentScope
    @IntoMap
    @ViewModelKey(ServicesListFragmentViewModel::class)
    abstract fun bindServicesListFragmentViewModel(model: ServicesListFragmentViewModel): ViewModel

    @Binds
    @FragmentScope
    @IntoMap
    @ViewModelKey(CreateServiceFragmentViewModel::class)
    abstract fun bindCreateServiceFragmentViewModel(model: CreateServiceFragmentViewModel): ViewModel
}