package noty_team.com.masterovik.dagger.modules

import android.arch.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import noty_team.com.masterovik.base.mvvm.DaggerViewModelFactory
import noty_team.com.masterovik.dagger.scopes.FragmentScope

@Module
abstract class ViewModelFactoryModule {
    @Binds
    @FragmentScope
    abstract fun bindViewModelFactory(viewModelFactory: DaggerViewModelFactory): ViewModelProvider.Factory
}