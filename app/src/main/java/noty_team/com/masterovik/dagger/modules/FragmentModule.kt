package noty_team.com.masterovik.dagger.modules

import dagger.Module
import dagger.Provides
import noty_team.com.masterovik.base.BaseActivity
import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.dagger.scopes.FragmentScope
import noty_team.com.masterovik.utils.ciceron.CiceroneFragmentManager
import noty_team.com.masterovik.utils.ciceron.Screens
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.commands.*

@Module
class FragmentModule(private val containerId: Int, private val baseActivity: BaseActivity<*>) {

    @Provides
    @FragmentScope
    fun provideBaseActivity(): BaseActivity<*> = baseActivity

    @Provides
    @FragmentScope
    fun provideCiceroneAppNavigator(): SupportAppNavigator {
        return object : SupportAppNavigator(baseActivity, containerId) {
            override fun applyCommands(commands: Array<Command>) {
                super.applyCommands(commands)
                baseActivity.supportFragmentManager.executePendingTransactions()
            }
        }
    }

    @Provides
    @FragmentScope
    fun provideFragmentManager2(ciceroneNavigator: SupportAppNavigator) = object: CiceroneFragmentManager {

        override fun<P: BaseViewModel> replaceFragment(fragment: BaseFragment<P>){
            ciceroneNavigator.applyCommands(arrayOf(Replace(Screens.FragmentScreen2(fragment))))
        }

        override fun<P: BaseViewModel> addFragment(fragment: BaseFragment<P>){
            ciceroneNavigator.applyCommands(arrayOf(Forward(Screens.FragmentScreen2(fragment))))
        }

        override fun<P: BaseViewModel> newRootFragment(fragment: BaseFragment<P>){
            ciceroneNavigator.applyCommands(arrayOf(BackTo(null), Replace(Screens.FragmentScreen2(fragment))))
        }

        override fun exit(){
            ciceroneNavigator.applyCommands(arrayOf(Back()))
        }
    }
}