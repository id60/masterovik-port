package noty_team.com.masterovik.dagger.components

import com.notissimus.stroyland.android.dagger.scopes.RetrofitScope
import dagger.Subcomponent
import noty_team.com.masterovik.dagger.modules.RetrofitApiModule

@RetrofitScope
@Subcomponent(modules = [RetrofitApiModule::class])
interface RetrofitComponent {

//    fun inject(target: StroylandApi)
}