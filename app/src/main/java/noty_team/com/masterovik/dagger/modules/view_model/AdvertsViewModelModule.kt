package noty_team.com.masterovik.dagger.modules.view_model

import android.arch.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import noty_team.com.masterovik.dagger.map_key.ViewModelKey
import noty_team.com.masterovik.dagger.scopes.FragmentScope
import noty_team.com.masterovik.ui.fragments.cities_list.CitiesListFragmentViewModel
import noty_team.com.masterovik.ui.fragments.advert.catalog_advert.CatalogAdvertsFragmentViewModel
import noty_team.com.masterovik.ui.fragments.advert.filter_advert.AdvertFilterFragmentViewModel
import noty_team.com.masterovik.ui.fragments.advert.my_advert.MyAdvertsFragmentViewModel

@Module
abstract class AdvertsViewModelModule {

    @Binds
    @FragmentScope
    @IntoMap
    @ViewModelKey(MyAdvertsFragmentViewModel::class)
    abstract fun bindMyAdvertsFragmentViewModel(model: MyAdvertsFragmentViewModel): ViewModel

    @Binds
    @FragmentScope
    @IntoMap
    @ViewModelKey(CitiesListFragmentViewModel::class)
    abstract fun bindInstanceAdvertFragmentViewModel(model: CitiesListFragmentViewModel): ViewModel

    @Binds
    @FragmentScope
    @IntoMap
    @ViewModelKey(CatalogAdvertsFragmentViewModel::class)
    abstract fun bindCatalogAdvertsFragmentViewModel(model: CatalogAdvertsFragmentViewModel): ViewModel

    @Binds
    @FragmentScope
    @IntoMap
    @ViewModelKey(AdvertFilterFragmentViewModel::class)
    abstract fun bindAdvertFilterFragmentViewModel(model: AdvertFilterFragmentViewModel): ViewModel
}