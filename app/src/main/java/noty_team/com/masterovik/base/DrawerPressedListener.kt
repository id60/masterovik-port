package noty_team.com.masterovik.base

interface DrawerPressedListener {
    fun onDrawerPressed()
    companion object {
        val empty = object: DrawerPressedListener {
            override fun onDrawerPressed() {}
        }
    }
}