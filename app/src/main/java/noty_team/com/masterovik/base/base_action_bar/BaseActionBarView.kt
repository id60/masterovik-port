package noty_team.com.masterovik.base.base_action_bar

import android.support.annotation.Nullable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.Observable
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.toolbar.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.utils.convertDpToPixel

class BaseActionBarView(private var root: View?,
                        override val containerView: View?) : LayoutContainer, ActionBarContract.View {

    override fun showActionBar(show: Boolean) {
        root?.visibility = if (show) View.VISIBLE else View.GONE
    }

    override fun showLeftButton(show: Boolean) {
        left_container.visibility = if (show) View.VISIBLE else View.INVISIBLE
        left_container.isEnabled = show

    }

    override fun setupLeftButton(view: View) {
        addViewSafe(left_container, view)
    }

    override fun showRightButton(show: Boolean) {
        right_container.visibility = if (show) View.VISIBLE else View.INVISIBLE
        right_container.isEnabled = show
    }


    override fun getRightContainer(): ViewGroup = right_container


    override fun setupRightButton(view: View) {
        addViewSafe(right_container, view)
    }

    override fun showCenterText(show: Boolean) {
        center_text.visibility = if (show) View.VISIBLE else View.GONE
    }

    override fun setupCenterText(res: Int) {
        center_text.setText(res)
    }

    override fun setupCenterText(string: String) {
        center_text.text = string
    }

    override fun leftButtonAction(): Observable<Any> {
        return RxView.clicks(left_container)
    }

    override fun getLeftButton(): View = left_container

    override fun rightButtonAction(): Observable<Any> {
        return RxView.clicks(right_container)
    }


    override fun changeSizeActionBar(boolean: Boolean) {
        val context = left_container.context
        if (boolean) {
            val width120Dp = convertDpToPixel(120f, context).toInt()
            left_container.layoutParams.width = width120Dp
            right_container.layoutParams.width = width120Dp

        } else {
            val width60Dp = convertDpToPixel(60f, context).toInt()
            left_container.layoutParams.width = width60Dp
            right_container.layoutParams.width = width60Dp
        }

    }

    override fun setLargeToolbarHeight() {
        val dp = convertDpToPixel(80.0f, containerView?.context)

        root?.layoutParams?.height = dp.toInt()
    }

    override fun setSmallToolbarHeight() {
        /* val density =  as Int*/
        val dp = convertDpToPixel(56.0f, containerView?.context)

        root?.layoutParams?.height = dp.toInt()
    }

//    private fun addViewSafe(@Nullable parentNew: ViewGroup?, @Nullable view: View?, @Nullable index: Int?, @Nullable layoutParams: ViewGroup.LayoutParams?) {
//        if (parentNew == null || view == null)
//            return
//
//        val parent = view.parent as? ViewGroup
//        if (parent != null) {
//            val transition = parent.layoutTransition
//
//            parent.layoutTransition = null
//            parent.removeView(view)
//            parent.layoutTransition = transition
//        }
//
//        if (index != null) {
//            if (layoutParams != null) {
//                parentNew.addView(view, index, layoutParams)
//            } else {
//                parentNew.addView(view, index)
//            }
//        } else {
//            if (layoutParams != null) {
//                parentNew.addView(view, layoutParams)
//            } else {
//                parentNew.addView(view)
//            }
//        }
//    }

    private fun addViewSafe(@Nullable parentNew: ViewGroup?, @Nullable view: View?) {
        parentNew?.let {
            it.removeAllViews()
            parentNew.addView(view)
        }
    }

    override fun resetView(boolean: Boolean) {
        if (boolean) {
            left_container.removeView(left_container.rootView)
        }
    }

    override fun setLargeContainerWidth() {
        val dp = convertDpToPixel(120.0f, containerView?.context)

        left_container.layoutParams.width = dp.toInt()
        right_container.layoutParams.width = dp.toInt()
    }

    override fun setSmallContainerWidth() {
        val dp = convertDpToPixel(60.0f, containerView?.context)
        left_container.layoutParams.width = dp.toInt()
        right_container.layoutParams.width = dp.toInt()
    }


    override fun setDrawerMenu(inflater: LayoutInflater): View {
        setupLeftButton(inflater.inflate(R.layout.ab_menu, null))
        return getLeftButton()
    }

    override fun setBackButton(inflater: LayoutInflater): View {
        setupLeftButton(inflater.inflate(R.layout.ab_back, null))
        return getLeftButton()
    }
}