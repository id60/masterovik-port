package noty_team.com.masterovik.base

import android.arch.lifecycle.ViewModelProvider
import android.content.Context
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.Toast
import com.github.vacxe.phonemask.PhoneMaskManager
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.dialog_progress.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.ui.activities.main.BackResultViewModel
import noty_team.com.masterovik.utils.ciceron.BackButtonListener
import noty_team.com.masterovik.utils.ciceron.CiceroneFragmentManager
import noty_team.com.masterovik.utils.injectViewModel
import noty_team.com.masterovik.utils.validator.FieldsValidationInterface
import java.util.concurrent.TimeUnit
import javax.inject.Inject

abstract class BaseFragment<V : BaseViewModel>: Fragment(), BackButtonListener, SnackBarListener {
    var onDrawerPressedListener = DrawerPressedListener.empty
    var toolbarSizeListener = ToolbarSizeListener.empty
    var viewPagerListener = ViewPagerListener.empty

    protected lateinit var viewModel: V

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    protected lateinit var baseContext: Context

    @Inject
    lateinit var baseActivity: BaseActivity<*>

    @Inject
    lateinit var compositeDisposable: CompositeDisposable

    @Inject
    lateinit var fragmentManager: CiceroneFragmentManager

    @Inject
    lateinit var validator: FieldsValidationInterface

    @Inject
    lateinit var phoneManager: PhoneMaskManager

    @Inject
    lateinit var toast: Toast

    protected var rootView: View? = null
    var isVisible: (fragment: Fragment) -> Boolean = { true }

//    open var presenter: P? = null

    @LayoutRes
    protected abstract fun layout(): Int
    protected abstract fun initialization(view: View, isFirstInit: Boolean)
    protected abstract fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): V

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try {
            (context as BaseActivity<*>).fragmentComponent.inject(this as BaseFragment<BaseViewModel>)
//            fragmentManager = context.fragmentManager
            if (context is DrawerPressedListener)
                onDrawerPressedListener = context
            if (context is ToolbarSizeListener)
                toolbarSizeListener = context
        }
        catch (e: UninitializedPropertyAccessException){
            (context as BaseActivity<*>).initFragmentComponent()
            context.fragmentComponent.inject(this as BaseFragment<BaseViewModel>)
//            fragmentManager = context.fragmentManager
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = provideViewModel(viewModelFactory)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = if (layout() != 0)
            addProgressView(inflater.inflate(layout(), container, false))
        else
            super.onCreateView(inflater, container, savedInstanceState)
        return if (rootView == null) view else rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initialization(view, rootView == null)
        rootView = view
        super.onViewCreated(view, savedInstanceState)
    }

    private fun addProgressView(rootView: View): View {
        val rootLayout = FrameLayout(baseContext)
        val layoutParams = FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, Gravity.CENTER)
        rootLayout.layoutParams = layoutParams
        rootLayout.addView(rootView)

        val progressLayout = baseActivity.layoutInflater.inflate(R.layout.dialog_progress, null)
        rootLayout.addView(progressLayout)
        return rootLayout
    }

    fun showProgress(show: Boolean){
        progress_layout?.visibility = if (show) View.VISIBLE else View.GONE
    }

    override fun onDestroyView() {
        viewModel.onClear()
        baseActivity.toggleKeyboard(false)
        super.onDestroyView()
    }

    override fun onDestroy() {
        baseActivity.toggleKeyboard(false)
        compositeDisposable.clear()
        super.onDestroy()
    }

    override fun onBackPressed(): Boolean {
        baseActivity.toggleKeyboard(false)
        fragmentManager.exit()
        return true
    }

    fun View.getClick(durationMillis: Long = 500, onClick:(view: View)->Unit){
        RxView.clicks(this)
                .throttleFirst(durationMillis, TimeUnit.MILLISECONDS)
                .subscribe ({ _ ->
                    onClick(this)
                }, {

                })
                .also { compositeDisposable.add(it) }
    }

    override fun showErrorSnack(message: String) {
        val snackbar = view?.let {
            Snackbar.make(it, message, Snackbar.LENGTH_LONG)
        }
        context?.let {
            ContextCompat.getColor(it, R.color.colorRed)
        }?.let {
            snackbar?.view?.setBackgroundColor(it)
        }
        snackbar?.addCallback(object: Snackbar.Callback(){
            override fun onShown(sb: Snackbar?) {

            }

            override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {

            }
        })

        /*snackbar?.let {
            val layout = it.view as Snackbar.SnackbarLayout
            val snackView = layoutInflater.inflate(R.layout.snackbar_custom, null, false)
            layout.setPadding(0,0,0,0)
            layout.addView(snackView, 0)
            layout.listenForDrawn{
                val screenWidth = Resources.getSystem().displayMetrics.widthPixels
                val dotsCount = Random.nextInt(100)
                var childIndex = 1

                for(i in 0..100+dotsCount){
                    val randomWidth = Random.nextInt(screenWidth)
                    val randomHeight = Random.nextInt(layout.height)
                    val dot = layoutInflater.inflate(R.layout.snackbar_dot, null, false)
                    val lp = RelativeLayout.LayoutParams(
                            5,
                            5
                    )
                    lp.setMargins(randomWidth, randomHeight, 0, 0)
                    dot.setLayoutParams(lp)

                    val randomColor = Random.nextInt(3)
                    dot.background.setColorFilter(
                            ContextCompat.getColor(baseContext, when(randomColor){
                                0 -> R.color.colorWhite
                                1 -> R.color.colorNails1
                                2 -> R.color.colorNails2
                                else -> R.color.colorWhite
                            }), PorterDuff.Mode.SRC_ATOP)
                    layout.addView(dot,childIndex++)
                }
            }
        }*/

        snackbar?.show()
    }

    override fun showSuccessSnack(message: String) {
        val snackbar = view?.let {
            Snackbar.make(it, message, Snackbar.LENGTH_LONG)
        }
        context?.let {
            ContextCompat.getColor(it, R.color.colorGreen)
        }?.let {
            snackbar?.view?.setBackgroundColor(it)
        }
        snackbar?.show()
    }

    fun subscribeForResult(onResult:(result: BackResultViewModel.BackResult<*, *>)->Unit){
        (injectViewModel(viewModelFactory) as BackResultViewModel?)?.observe(onResult,this)
    }

    fun<T: Any> sendBackResult(data: T){
        (baseActivity.injectViewModel(viewModelFactory) as BackResultViewModel?)?.addResult(data, this::class)
    }
}