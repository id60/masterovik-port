package noty_team.com.masterovik.base.base_action_bar

import android.view.LayoutInflater
import android.view.ViewGroup
import io.reactivex.Observable

interface ActionBarContract {
    interface View {
        fun showActionBar(show: Boolean)

        fun showLeftButton(show: Boolean)
        fun setupLeftButton(view: android.view.View)


        fun showRightButton(show: Boolean)
        fun setupRightButton(view: android.view.View)

        fun showCenterText(show: Boolean)
        fun setupCenterText(res: Int)
        fun setupCenterText(string: String)

        fun leftButtonAction(): Observable<Any>
        fun rightButtonAction(): Observable<Any>
        fun getLeftButton(): android.view.View

        fun changeSizeActionBar(boolean: Boolean)

        fun resetView(boolean: Boolean)
        fun getRightContainer(): ViewGroup

        fun setLargeToolbarHeight()
        fun setSmallToolbarHeight()

        fun setLargeContainerWidth()
        fun setSmallContainerWidth()

        fun setDrawerMenu(inflater: LayoutInflater): android.view.View
        fun setBackButton(inflater: LayoutInflater): android.view.View
    }

    interface Presenter {

        fun setupView()
        fun setupActions()
        fun leftButtonAction()
        fun rightButtonAction()
        fun dispose()
        fun start()
        fun stop()
    }
}