package noty_team.com.masterovik.base

interface ToolbarSizeListener {
    fun resizeContainerLarge()
    fun resizeContainerSmall()
    fun hideEmptyToolbarSpace(enable: Boolean)
    fun enableSnackBarBehaviour(enable: Boolean)
    companion object {
        val empty = object: ToolbarSizeListener {
            override fun resizeContainerLarge() {}
            override fun resizeContainerSmall() {}
            override fun hideEmptyToolbarSpace(enable: Boolean) {}
            override fun enableSnackBarBehaviour(enable: Boolean) {}
        }
    }
}