package noty_team.com.masterovik.base.dialog

import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.view.Window
import kotlinx.android.synthetic.main.button_continue.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.AppMasterovik

class InviteDialog(var context: AppMasterovik) : Dialog(context), View.OnClickListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_user_invite)
        continue_btn.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.continue_btn -> dismiss()
        }
    }
}