package noty_team.com.masterovik.base

import android.app.Activity
import android.app.Application
import android.content.Context
import android.support.v7.app.AppCompatDelegate
import io.paperdb.Paper
import noty_team.com.masterovik.dagger.components.AppComponent
import noty_team.com.masterovik.dagger.components.DaggerAppComponent
import noty_team.com.masterovik.dagger.modules.AppModule
import noty_team.com.masterovik.dagger.modules.RetrofitApiModule

class AppMasterovik : Application() {
    companion object {
        operator fun get(activity: Activity): AppMasterovik {
            return activity.application as AppMasterovik
        }

        operator fun get(context: Context): AppMasterovik {
            return context as AppMasterovik
        }
    }

    val appComponent: AppComponent by lazy {
        initDagger()
    }

    private fun initDagger(): AppComponent =
            DaggerAppComponent.builder()
                    .appModule(AppModule(this))
                    .retrofitApiModule(RetrofitApiModule())
                    .build()

    override fun onCreate() {
        super.onCreate()
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        Paper.init(applicationContext)
    }

}