package noty_team.com.masterovik.base

import android.arch.lifecycle.ViewModelProvider
import android.content.Context
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.disposables.CompositeDisposable
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.base_action_bar.ActionBarContract
import noty_team.com.masterovik.base.mvvm.BaseViewModel
import noty_team.com.masterovik.dagger.components.FragmentComponent
import noty_team.com.masterovik.dagger.modules.FragmentModule
import noty_team.com.masterovik.utils.ciceron.BackButtonListener
import noty_team.com.masterovik.utils.ciceron.CiceroneFragmentManager
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import java.util.concurrent.TimeUnit
import javax.inject.Inject

abstract class BaseActivity<V : BaseViewModel>: AppCompatActivity() {

    protected lateinit var viewModel: V

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var fragmentComponent: FragmentComponent

    @Inject
    lateinit var compositeDisposable: CompositeDisposable

    @Inject
    lateinit var cicerone: Cicerone<Router>

    @Inject
    lateinit var navigatorHolder: NavigatorHolder

    @Inject
    lateinit var ciceroneNavigator: SupportAppNavigator

    @Inject
    lateinit var fragmentManager: CiceroneFragmentManager

    @LayoutRes
    abstract fun layout(): Int
    abstract fun initialization()
    abstract fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): V

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initFragmentComponent()

        viewModel = provideViewModel(viewModelFactory)

        if (layout() != 0) {
            setContentView(layout())
            initialization()
        }
    }

    fun initFragmentComponent() {
        if (!::fragmentComponent.isInitialized) {
            fragmentComponent = AppMasterovik[this].appComponent.plus(
                    FragmentModule(R.id.main_fragment_container, this as BaseActivity<BaseViewModel>)
            )
            fragmentComponent.inject(this)
        }
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    override fun onResume() {
        super.onResume()
        navigatorHolder.setNavigator(ciceroneNavigator)
    }

    override fun onPause() {
        super.onPause()
        navigatorHolder.removeNavigator()
    }

    override fun onBackPressed() {
        val fm = supportFragmentManager
        var fragment: Fragment? = null
        val fragments = fm.fragments
        for (f in fragments) {
            if (f.isVisible) {
                fragment = f
                break
            }
        }

        if (fragment != null
                && fragment is BackButtonListener
                && (fragment as BackButtonListener).onBackPressed()) {
        } else {
            fragmentManager.exit()
        }
    }

    fun toggleKeyboard(show: Boolean) {
        val inputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (!show)
            inputMethodManager.hideSoftInputFromWindow(window.decorView.windowToken, 0)
        else
            inputMethodManager.toggleSoftInputFromWindow(window.decorView.windowToken,
                    InputMethodManager.SHOW_FORCED, 0)
    }

    fun freeMemory() {
        System.runFinalization()
        Runtime.getRuntime().gc()
        System.gc()
    }

    fun View.getClick(durationMillis: Long = 500, onClick:(view: View)->Unit){
        RxView.clicks(this)
                .throttleFirst(durationMillis, TimeUnit.MILLISECONDS)
                .subscribe ({ _ ->
                    onClick(this)
                }, {

                })
                .also { compositeDisposable.add(it) }
    }

    abstract fun getActionBarView(): ActionBarContract.View?

}