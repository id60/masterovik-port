package noty_team.com.masterovik.base.mvvm

import android.app.Application
import android.arch.lifecycle.*
import android.widget.Toast
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import noty_team.com.masterovik.api.retrofit.apis.MasterovikApi
import noty_team.com.masterovik.base.AppMasterovik
import javax.inject.Inject

abstract class BaseViewModel(val app: Application): AndroidViewModel(app) {

    @Inject
    lateinit var api: MasterovikApi

    @Inject
    lateinit var compositeDisposable: CompositeDisposable

    @Inject
    lateinit var toast: Toast

    init {
        AppMasterovik[app].appComponent.inject(this)
    }

    abstract fun onClear()

    override fun onCleared() {
        super.onCleared()
        onClear()
        compositeDisposable.clear()
    }

    private fun Disposable.bindToLifeCycle() {
        compositeDisposable.add(this)
    }

    protected fun<T> Single<T>.call() {
        this.subscribe({
        }, {

        }).also { it.bindToLifeCycle() }
    }

//    protected fun<T> Single<T>.call(liveData: MutableLiveData<Result<T>>) {
//        liveData.postValue(Loading(true))
//        this
//                .doAfterSuccess {
//                    liveData.postValue(Loading(false))
//                }
//                .subscribe({
//
//                }, {
//            liveData.postValue(Loading(false))
//        }).also { it.bindToLifeCycle() }
//    }

//    protected fun<T> Single<T>.call2(onResult:(Result<T>)->Unit) {
//        val callback = api.getCallback(this)
//        this.subscribe({
//            onResult(callback.onSuccess(it))
//        }, {
//            onResult(callback.onError(it))
//        }).also { it.bindToLifeCycle() }
//    }

    fun showToast(message: String){
        noty_team.com.masterovik.utils.showToast(toast, message)
    }
}