package noty_team.com.masterovik.base

interface ViewPagerListener {
    fun getPageIndex(): Int
    fun isCurrentPageSelected(): Boolean

    companion object {
        val empty = object: ViewPagerListener {
            override fun getPageIndex() = 0
            override fun isCurrentPageSelected() = false
        }
    }
}