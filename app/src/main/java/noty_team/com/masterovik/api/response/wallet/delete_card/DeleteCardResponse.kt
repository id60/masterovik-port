package noty_team.com.masterovik.api.response.wallet.delete_card

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.masterovik.api.response.BaseResponse

@Generated("com.robohorse.robopojogenerator")
data class DeleteCardResponse(

	@field:SerializedName("response")
	val response: String = "",

	var intCardId: Int = -1

): BaseResponse()