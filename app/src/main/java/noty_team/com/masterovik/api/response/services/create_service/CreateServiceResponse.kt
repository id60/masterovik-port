package noty_team.com.masterovik.api.response.services.create_service

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.masterovik.api.response.BaseResponse

@Generated("com.robohorse.robopojogenerator")
data class CreateServiceResponse(

	@field:SerializedName("reason")
	val reason: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("id")
	val id: Int = -1

): BaseResponse()