package noty_team.com.masterovik.api.response.auth.refresh_token

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.masterovik.api.response.BaseResponse

@Generated("com.robohorse.robopojogenerator")
data class RefreshTokenResponse(

	@field:SerializedName("access_token")
	val accessToken: String? = null

): BaseResponse()