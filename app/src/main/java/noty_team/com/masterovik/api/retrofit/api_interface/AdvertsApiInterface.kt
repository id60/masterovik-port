package noty_team.com.masterovik.api.retrofit.api_interface

import io.reactivex.Single
import noty_team.com.masterovik.api.request.advert.get_catalog_adverts.CatalogAdvertsRequest
import noty_team.com.masterovik.api.request.change_email.ChangeEmailRequest
import noty_team.com.masterovik.api.request.change_phone.ChangePhoneRequest
import noty_team.com.masterovik.api.request.edit_profile.EditProfileRequest
import noty_team.com.masterovik.api.response.profile.ProfileResponse
import noty_team.com.masterovik.api.request.profile.ProfileRequest
import noty_team.com.masterovik.api.request.services.AddServiceRequest
import noty_team.com.masterovik.api.request.simple_request.SimpleAccessTokenRequest
import noty_team.com.masterovik.api.response.BaseResponse
import noty_team.com.masterovik.api.response.adverts.my_adverts.MyAdvertsResponse
import noty_team.com.masterovik.api.response.change_email.ChangeEmailResponse
import noty_team.com.masterovik.api.response.change_phone.ChangePhoneResponse
import noty_team.com.masterovik.api.response.edit_profile.EditProfileResponse
import retrofit2.http.Body
import retrofit2.http.POST

interface AdvertsApiInterface {

    @POST("announcement/get-my-announcements")
    fun getMyAdverts(@Body data: SimpleAccessTokenRequest): Single<MyAdvertsResponse>

    @POST("announcement/get-catalog-announcements")
    fun getCatalogAdverts(@Body data: CatalogAdvertsRequest): Single<MyAdvertsResponse>
}