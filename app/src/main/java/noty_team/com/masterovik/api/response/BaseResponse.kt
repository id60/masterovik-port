package noty_team.com.masterovik.api.response

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
open class BaseResponse(

	@field:SerializedName("error_message")
	val errorMessage: String? = null,

	@field:SerializedName("status_code")
	val statusCode: String? = null
)