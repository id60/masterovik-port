package noty_team.com.masterovik.api.request.auth.refresh_token

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class RefreshTokenRequest(

	@field:SerializedName("refresh_token")
	var refreshToken: String? = null
)