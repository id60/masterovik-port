package noty_team.com.masterovik.api.response.wallet.add_card

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.masterovik.api.response.BaseResponse

@Generated("com.robohorse.robopojogenerator")
data class AddCardResponse(

	@field:SerializedName("response")
	val response: Response? = null

): BaseResponse()