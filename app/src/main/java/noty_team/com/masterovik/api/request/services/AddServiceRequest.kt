package noty_team.com.masterovik.api.request.services

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.masterovik.utils.paper.PaperIO

@Generated("com.robohorse.robopojogenerator")
data class AddServiceRequest(

		@field:SerializedName("access_token")
	val accessToken: String = PaperIO.getAccessToken(),

		@field:SerializedName("services")
	val services: List<Int> = arrayListOf()
)