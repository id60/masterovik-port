package noty_team.com.masterovik.api.retrofit.api_interface

import io.reactivex.Single
import noty_team.com.masterovik.api.request.create_service.CreateServiceRequest
import noty_team.com.masterovik.api.request.simple_request.SimpleAccessTokenRequest
import noty_team.com.masterovik.api.response.services.create_service.CreateServiceResponse
import noty_team.com.masterovik.api.response.services.get_service.GetServiceResponse
import noty_team.com.masterovik.api.response.services.get_services.GetServicesResponse
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface ServicesApiInterface {

    @GET("service/get-services")
    fun getServices(): Single<GetServicesResponse>

    @GET("service/{serviceId}")
    fun getService(@Path("serviceId") serviceId: Int): Single<GetServiceResponse>

    @POST("service-offered/create")
    fun createService(@Body body: CreateServiceRequest): Single<CreateServiceResponse>

    @POST("service/get-user-services")
    fun getUserServices(@Body body: SimpleAccessTokenRequest): Single<GetServicesResponse>
}