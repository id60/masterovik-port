package noty_team.com.masterovik.api.response.wallet.edit_card

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.masterovik.api.response.BaseResponse

@Generated("com.robohorse.robopojogenerator")
data class EditCardResponse(

	@field:SerializedName("response")
	val response: String? = null

): BaseResponse()