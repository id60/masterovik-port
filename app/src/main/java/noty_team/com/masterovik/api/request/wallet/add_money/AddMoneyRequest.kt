package noty_team.com.masterovik.api.request.wallet.add_money

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.masterovik.api.request.simple_request.SimpleAccessTokenRequest

@Generated("com.robohorse.robopojogenerator")
data class AddMoneyRequest(

	@field:SerializedName("sum")
	val sum: Int = 0,

	@field:SerializedName("card_id")
	val cardId: Int = -1

): SimpleAccessTokenRequest()