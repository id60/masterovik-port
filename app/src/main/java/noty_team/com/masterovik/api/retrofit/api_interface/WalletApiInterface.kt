package noty_team.com.masterovik.api.retrofit.api_interface

import io.reactivex.Single
import noty_team.com.masterovik.api.request.simple_request.SimpleAccessTokenRequest
import noty_team.com.masterovik.api.request.wallet.add_card.AddCardRequest
import noty_team.com.masterovik.api.request.wallet.add_money.AddMoneyRequest
import noty_team.com.masterovik.api.request.wallet.delete_card.DeleteCardRequest
import noty_team.com.masterovik.api.request.wallet.edit_card.EditCardRequest
import noty_team.com.masterovik.api.response.BaseResponse
import noty_team.com.masterovik.api.response.wallet.add_card.AddCardResponse
import noty_team.com.masterovik.api.response.wallet.delete_card.DeleteCardResponse
import noty_team.com.masterovik.api.request.wallet.history.PaymentHistoryRequest
import noty_team.com.masterovik.api.response.wallet.add_money.AddMoneyResponse
import noty_team.com.masterovik.api.response.wallet.edit_card.EditCardResponse
import noty_team.com.masterovik.api.response.wallet.history.PaymentHistoryResponse
import noty_team.com.masterovik.api.response.wallet.my_cards.MyCardsResponse
import noty_team.com.masterovik.api.response.wallet.partners.PartnersResponse
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.HTTP
import retrofit2.http.POST

interface WalletApiInterface {

    @POST("credit-cards/view-cards")
    fun getMyCards(@Body data: SimpleAccessTokenRequest): Single<MyCardsResponse>

    @POST("credit-cards/add-money")
    fun addMoney(@Body data: AddMoneyRequest): Single<AddMoneyResponse>

    @POST("credit-cards/add-card")
    fun addCard(@Body data: AddCardRequest): Single<AddCardResponse>

    @POST("credit-cards/delete-card")
    fun deleteCard(@Body data: DeleteCardRequest): Single<DeleteCardResponse>

    @POST("credit-cards/edit-card")
    fun editCard(@Body data: EditCardRequest): Single<EditCardResponse>

    @POST("credit-cards/get-payment-history")
    fun getPaymentHistory(@Body data: PaymentHistoryRequest): Single<PaymentHistoryResponse>

    @POST("credit-cards/get-partners")
    fun getPartners(@Body data: SimpleAccessTokenRequest): Single<PartnersResponse>
}