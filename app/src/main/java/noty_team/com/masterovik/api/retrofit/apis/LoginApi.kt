package noty_team.com.masterovik.api.retrofit.apis

import android.content.Context
import io.reactivex.Single
import noty_team.com.masterovik.api.request.auth.login.LoginRequest
import noty_team.com.masterovik.api.request.auth.refresh_token.RefreshTokenRequest
import noty_team.com.masterovik.api.request.auth.register.RegistrationRequest
import noty_team.com.masterovik.api.response.auth.login.LoginResponse
import noty_team.com.masterovik.api.response.auth.refresh_token.RefreshTokenResponse
import noty_team.com.masterovik.api.response.auth.registration.RegistrationResponse
import noty_team.com.masterovik.api.retrofit.BaseRepository
import noty_team.com.masterovik.api.retrofit.api_interface.LoginApiInterface
import noty_team.com.masterovik.utils.Result
import retrofit2.Retrofit
import javax.inject.Inject

class LoginApi @Inject constructor(
        context: Context,
        retrofit: Retrofit,
        private var loginApi: LoginApiInterface): BaseRepository(context, retrofit) {

    fun refreshIoken(data: RefreshTokenRequest,
                     onResult: (Result<RefreshTokenResponse>) -> Unit): Single<RefreshTokenResponse> {
        return initRequestSingleForResult(loginApi.refreshToken(data), onResult)
    }

    fun login(data: LoginRequest,
              onResult: (Result<LoginResponse>) -> Unit): Single<LoginResponse> {
        return initRequestSingleForResult(loginApi.login(data), onResult)
    }

    fun register(data: RegistrationRequest,
                 onResult: (Result<RegistrationResponse>) -> Unit): Single<RegistrationResponse> {
        return initRequestSingleForResult(loginApi.register(data), onResult)
    }

}
