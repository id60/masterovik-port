package noty_team.com.masterovik.api.request.edit_profile

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class EditProfileRequest(

	@field:SerializedName("access_token")
	var accessToken: String = "",

	@field:SerializedName("company_description")
	var companyDescription: String? = null,

	@field:SerializedName("company_name")
	var companyName: String? = null,

	@field:SerializedName("last_name")
	var lastName: String? = null,

	@field:SerializedName("first_name")
	var firstName: String? = null,

	@field:SerializedName("city_id")
	var cityId: Int = -1,

	@field:SerializedName("email")
	var email: String? = null
)