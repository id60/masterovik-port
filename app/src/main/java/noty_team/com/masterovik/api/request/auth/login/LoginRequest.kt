package noty_team.com.masterovik.api.request.auth.login

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class LoginRequest(

	@field:SerializedName("phone")
	val phone: String? = null,

	@field:SerializedName("sms")
	val sms: String? = null
)