package noty_team.com.masterovik.api.response.city.cities_list

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.masterovik.api.response.BaseResponse
import noty_team.com.masterovik.ui.adapters.items_adapter.CityItem

@Generated("com.robohorse.robopojogenerator")
data class CitiesResponse(

	@field:SerializedName("response")
	val response: ArrayList<ResponseItem>? = null

): BaseResponse() {

	fun map(): ArrayList<CityItem> {
		return response?.flatMap { arrayListOf(
				CityItem(
						id = it.id,
						cityName = it.city
				)
		) } as ArrayList<CityItem>
	}

}