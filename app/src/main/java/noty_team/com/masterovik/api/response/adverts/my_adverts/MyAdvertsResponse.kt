package noty_team.com.masterovik.api.response.adverts.my_adverts

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.masterovik.api.response.BaseResponse
import noty_team.com.masterovik.ui.adapters.items_adapter.CatalogAdvertItem

@Generated("com.robohorse.robopojogenerator")
data class MyAdvertsResponse(

	@field:SerializedName("response")
	val response: ArrayList<ResponseItem>? = null

): BaseResponse(){

	fun map(): ArrayList<CatalogAdvertItem>{
		return response?.flatMap { arrayListOf(
				CatalogAdvertItem (
						advertId = it.id,
						advertName = "",
						dateCreated = it.dateEnd?:"",
						price = it.price?:"",
						shortDescription = it.description?:"",
						fullDescription = it.descriptionFull?:"",
						name = it.name?:"",
						phone = it.phone?:"",
						email = it.email?:"",
						dateEnded = it.dateEnd?:"",
						cityId = it.cityId?:-1,
						address = it.placeOfWork?:""
				)
		) } as ArrayList
	}

}