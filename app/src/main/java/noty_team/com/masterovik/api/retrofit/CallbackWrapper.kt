package noty_team.com.masterovik.api.retrofit

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import noty_team.com.masterovik.api.request.auth.refresh_token.RefreshTokenRequest
import noty_team.com.masterovik.api.response.BaseResponse
import noty_team.com.masterovik.api.retrofit.apis.LoginApi
import noty_team.com.masterovik.base.AppMasterovik
import noty_team.com.masterovik.ui.activities.login.LoginActivity
import noty_team.com.masterovik.utils.Failure
import noty_team.com.masterovik.utils.Result
import noty_team.com.masterovik.utils.Success
import noty_team.com.masterovik.utils.paper.PaperIO
import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException
import retrofit2.Retrofit
import java.lang.ref.WeakReference
import javax.inject.Inject

class CallbackWrapper<T, V>(
        private val contextWeakReference: WeakReference<Context>? = null,
        private var onResult:(Result<V>) -> Unit) : DisposableSingleObserver<T>(){

    companion object {
        const val ERROR_WRONG_SMS_CODE = 700
        const val ERROR_UNEXPECTED_CODE = 666
        const val ERROR_CONVERTING_DATA_CODE = 601
        const val ERROR_INTERNET_CONNECTION_CODE = 600
        const val ERROR_NOT_FOUND_CODE = 404
        const val ERROR_UNAUTHORIZED = 401
    }

    @Inject
    lateinit var retrofit: Retrofit

    @Inject
    lateinit var loginApi: LoginApi

    var request: Single<T>? = null
    private var repeatingUnauthorizedRequestCount = 0

    init {
        contextWeakReference?.get()?.let {
            AppMasterovik[it].appComponent.inject(this as CallbackWrapper<Any, Any>)
        }
    }

    override fun onSuccess(response: T) {

        onResult(Success(response as V))

//        if (response is BaseResponse) {
//            val data = response as BaseResponse
//            if (!data.response) {
//                when (data.error.code) {
////                    403 -> {
////                        val refreshToken = PaperIO.getRefreshToken()
////                        JoyApi().refreshToken(refreshToken, { res ->
////                            PaperIO.setBearerToken(res.data.bearerToken)
////                            PaperIO.setRefreshToken(res.data.refreshToken)
////                            repeatingUnauthorizedRequestCount++
////                            if (repeatingUnauthorizedRequestCount > 3)
////                                onError(data.error.message, data.error.code)
////                            else
////                                request?.subscribeWith(this)
////                        }, { errorMessage, errorCode ->
////                            onError(errorMessage, errorCode)
////                            contextWeakReference?.get()?.let {
////                                val intent = Intent(it, SuccessActivity::class.java)
////                                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
////                                val  pi = PendingIntent.getActivity(it, 0, intent, 0)
////                                pi.send()
////                            }
////                        })
////                    }
//                    else -> onError(data.error.message, data.error.code)
//                }
//            } else onSuccessResponse(response as V)
//        }
//        else if (response is ResponseBody){
//            try {
//                val responseString = response.string()
//                val jsonData = Gson().fromJson<BaseResponse>(responseString)
//                if (jsonData.response) {
//                    onSuccessResponse(responseString as V)
//                }
//                else {
//                    onError(jsonData.error.message, jsonData.error.code)
//                }
//            }
//            catch (e: JSONException){
//                onError(contextWeakReference?.get()?.getString(R.string.error_converting_data)?:"Ошибка преобразования данных", ERROR_CONVERTING_DATA_CODE)
//            }
//        }
    }

    override fun onError(t: Throwable) {
        val errorMessage: String
        val errorCode: Int
        if (t is IOException) {
            errorMessage = /*contextWeakReference?.get()?.getString(R.string.error_no_intenet_connection)?:*/"Проверьте интернет соединение"
            errorCode = ERROR_INTERNET_CONNECTION_CODE
        } else if (t is HttpException) {
            when (t.code()){
                ERROR_UNAUTHORIZED -> {
                    onUnauthorized()
                    return
                }
                else -> {
                    val errorWrapper = ErrorWrapper()
                    val error = errorWrapper.parseError(retrofit, t.response())
                    if (error != null) {
                        errorMessage = error.errorMessage?:""
                        if (error.statusCode?.isNotEmpty() == true)
                            errorCode = error.statusCode?.toInt()
                        else errorCode = t.code()
                    }
                    else {
                        errorMessage = /*contextWeakReference?.get()?.getString(R.string.error_unexpected)?:*/"Непредвиденная ошибка"
                        errorCode = ERROR_UNEXPECTED_CODE
                    }
                }
            }
        } else if (t is IllegalStateException) {
            errorMessage = /*contextWeakReference?.get()?.getString(R.string.error_converting_data)?:*/"Ошибка преобразования данных"
            errorCode = ERROR_CONVERTING_DATA_CODE
        } else {
            errorMessage = /*contextWeakReference?.get()?.getString(R.string.error_unexpected)?:*/"Непредвиденная ошибка"
            errorCode = ERROR_UNEXPECTED_CODE
        }
        onResult(Failure(errorMessage, errorCode))
    }

    private fun onUnauthorized(){
        val refreshToken = PaperIO.getRefreshToken()
        loginApi.refreshIoken(RefreshTokenRequest(refreshToken)) { res ->
            when(res){
                is Success -> {
                    if (res.value.accessToken?.isNotEmpty() == true){
                        PaperIO.setAccessToken(res.value.accessToken)

                        repeatingUnauthorizedRequestCount++
                        if (repeatingUnauthorizedRequestCount > 2) {
                            repeatingUnauthorizedRequestCount = 0
//                                        onResult(Failure(t.message(), t.code()))
                            contextWeakReference?.get()?.let {
                                val intent = Intent(it, LoginActivity::class.java)
                                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                val  pi = PendingIntent.getActivity(it, 0, intent, 0)
                                pi.send()
                            }
                        } else{
                            val callback = CallbackWrapper<T, V>(contextWeakReference, onResult)
                            callback.repeatingUnauthorizedRequestCount = repeatingUnauthorizedRequestCount
                            callback.request = request
                                        request?.doOnSuccess {
                                            callback.onSuccess(it)
                                        }?.doOnError {
                                            callback.onError(it)
                                        }?.subscribe({},{})
//                            request?.subscribeWith(callback)
                        }
                    }
                }
                is Failure -> {
                    onResult(Failure(res.errorMessage, res.errorCode))
                }
            }
        }.subscribe({}, {})
    }

    private inner class ErrorWrapper {
        fun parseError(retrofit: Retrofit, response: Response<*>): BaseResponse? {
            val converter: Converter<ResponseBody, BaseResponse> = retrofit
                    .responseBodyConverter(BaseResponse::class.java, emptyArray())
            try {
                response.errorBody()?.let {
                    return converter.convert(it)
                }
            } catch (e: Exception) {
                return BaseResponse()
            }
            return BaseResponse()
        }
    }
}