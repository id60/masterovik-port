package noty_team.com.masterovik.api.request.change_phone

data class ChangePhoneRequest(
	val accessToken: String = "",
	val phone: String = "",
	val sms: String? = null
)
