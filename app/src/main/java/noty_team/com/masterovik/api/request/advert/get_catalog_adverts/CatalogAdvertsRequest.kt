package noty_team.com.masterovik.api.request.advert.get_catalog_adverts

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.masterovik.ui.adapters.items_adapter.CityItem
import noty_team.com.masterovik.ui.adapters.items_adapter.ProvideServiceItem

@Generated("com.robohorse.robopojogenerator")
data class CatalogAdvertsRequest(

        @field:SerializedName("city_id")
        var cityId: Int? = null,

        @field:SerializedName("services_id")
        var servicesId: ArrayList<Int>? = null,

        @field:SerializedName("price")
        val price: ArrayList<Int>? = null,

        @field:SerializedName("date")
        val date: ArrayList<Long>? = null,

        @field:SerializedName("term")
        val term: ArrayList<Int>? = null,

        var cityItem: CityItem? = null,
        var servicesList: ArrayList<ProvideServiceItem>? = null

)