package noty_team.com.masterovik.api.retrofit

import android.content.Context
import android.util.Base64
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import noty_team.com.masterovik.api.request.auth.login.LoginRequest
import noty_team.com.masterovik.api.response.profile.ProfileResponse
import noty_team.com.masterovik.api.request.auth.refresh_token.RefreshTokenRequest
import noty_team.com.masterovik.api.request.auth.register.RegistrationRequest
import noty_team.com.masterovik.api.request.change_email.ChangeEmailRequest
import noty_team.com.masterovik.api.request.change_phone.ChangePhoneRequest
import noty_team.com.masterovik.api.request.create_service.CreateServiceRequest
import noty_team.com.masterovik.api.request.edit_profile.EditProfileRequest
import noty_team.com.masterovik.api.response.city.cities_list.CitiesResponse
import noty_team.com.masterovik.api.response.city.city.CityResponse
import noty_team.com.masterovik.api.response.auth.login.LoginResponse
import noty_team.com.masterovik.api.request.profile.ProfileRequest
import noty_team.com.masterovik.api.response.auth.refresh_token.RefreshTokenResponse
import noty_team.com.masterovik.api.response.auth.registration.RegistrationResponse
import noty_team.com.masterovik.api.response.change_email.ChangeEmailResponse
import noty_team.com.masterovik.api.response.change_phone.ChangePhoneResponse
import noty_team.com.masterovik.api.response.edit_profile.EditProfileResponse
import noty_team.com.masterovik.api.response.services.create_service.CreateServiceResponse
import noty_team.com.masterovik.api.response.services.get_service.GetServiceResponse
import noty_team.com.masterovik.base.AppMasterovik
import noty_team.com.masterovik.utils.Result
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Retrofit
import java.io.File
import java.lang.ref.WeakReference
import javax.inject.Inject

open class BaseRepository @Inject constructor(
        val context: Context,
        val retrofit: Retrofit) {

    init {
        AppMasterovik[context].appComponent.inject(this)
    }

//    private fun <T, V> initRequestSingle(request: Single<T>,
//                                         onSuccess: (response: V) -> Unit,
//                                         onError: (errorMessage: String, errorCode: Int) -> Unit): Disposable {
//        val requestSingle = request.subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//        val callback = CallbackWrapper<T, V>(WeakReference(context), retrofit, onSuccess, onError)
//        callback.request = requestSingle
//        return requestSingle.subscribeWith(callback)
//    }

    protected fun <T, V> initRequestSingleForResult(request: Single<T>,
                                         onResult: (Result<V>) -> Unit): Single<T> {
        val callback = CallbackWrapper<T, V>(WeakReference(context), onResult)
        val requestSingle = request.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess {
                    callback.onSuccess(it)
                }
                .doOnError {
                    callback.onError(it)
                }
        callback.request = requestSingle
        return requestSingle
    }

    protected fun <T> initRequestSingleForResult2(request: Single<T>): Single<T> {
        val callback = CallbackWrapper2<T>(WeakReference(context), retrofit)
        val requestSingle = request.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess {
                    callback.onSuccess(it)
                }
                .doOnError {
                    callback.onError(it)
                }
        return requestSingle
    }

    protected fun<T> getCallback(request: Single<T>) = CallbackWrapper2(WeakReference(context), retrofit, request)

    protected fun zip(requests: ArrayList<Single<*>>, onLoadingEnded: () -> Unit): Disposable {
        return Single.zip(requests) { result ->  }.subscribe({onLoadingEnded()}, {})
    }

    protected fun getJson(data: HashMap<String, Any>): RequestBody {
        return RequestBody.create(MediaType.parse("application/json; charset=utf-8"),
                (JSONObject(data)).toString())
    }

    protected fun getBase64String(file: File) = Base64.encodeToString(file.readBytes(), Base64.DEFAULT)

}
