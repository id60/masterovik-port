package noty_team.com.masterovik.api.response.wallet.add_card

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Response(

	@field:SerializedName("user_id")
	val userId: Int = -1,

	@field:SerializedName("id")
	val id: Int = -1,

	@field:SerializedName("card")
	val card: String = ""
)