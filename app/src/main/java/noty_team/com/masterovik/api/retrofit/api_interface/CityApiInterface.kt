package noty_team.com.masterovik.api.retrofit.api_interface

import io.reactivex.Single
import noty_team.com.masterovik.api.response.city.cities_list.CitiesResponse
import noty_team.com.masterovik.api.response.city.city.CityResponse
import retrofit2.http.GET
import retrofit2.http.Path

interface CityApiInterface {

    @GET("city")
    fun getCities(): Single<CitiesResponse>

    @GET("city/{cityId}")
    fun getCity(@Path("cityId") cityId: Int): Single<CityResponse>

}