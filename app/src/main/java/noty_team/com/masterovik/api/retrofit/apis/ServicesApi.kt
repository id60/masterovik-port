package noty_team.com.masterovik.api.retrofit.apis

import android.content.Context
import io.reactivex.Single
import noty_team.com.masterovik.api.request.create_service.CreateServiceRequest
import noty_team.com.masterovik.api.request.simple_request.SimpleAccessTokenRequest
import noty_team.com.masterovik.api.response.services.create_service.CreateServiceResponse
import noty_team.com.masterovik.api.response.services.get_service.GetServiceResponse
import noty_team.com.masterovik.api.response.services.get_services.GetServicesResponse
import noty_team.com.masterovik.api.retrofit.BaseRepository
import noty_team.com.masterovik.api.retrofit.api_interface.ServicesApiInterface
import noty_team.com.masterovik.utils.Result
import noty_team.com.masterovik.utils.paper.PaperIO
import retrofit2.Retrofit
import javax.inject.Inject

class ServicesApi @Inject constructor(
        context: Context,
        retrofit: Retrofit,
        private var servicesApi: ServicesApiInterface): BaseRepository(context, retrofit) {

    fun getServices(onSuccess: (Result<GetServicesResponse>) -> Unit): Single<GetServicesResponse> {
        return initRequestSingleForResult(servicesApi.getServices(), onSuccess)
    }

    fun getService(serviceId: Int,
                   onSuccess: (Result<GetServiceResponse>) -> Unit): Single<GetServiceResponse> {
        return initRequestSingleForResult(servicesApi.getService(serviceId), onSuccess)
    }

    fun createService(data: CreateServiceRequest,
                      onSuccess: (Result<CreateServiceResponse>) -> Unit): Single<CreateServiceResponse> {
        return initRequestSingleForResult(servicesApi.createService(data), onSuccess)
    }

    fun getUserServices(onSuccess: (Result<GetServicesResponse>) -> Unit): Single<GetServicesResponse> {
        val request = SimpleAccessTokenRequest()
        return initRequestSingleForResult(servicesApi.getUserServices(request), onSuccess)
    }
}
