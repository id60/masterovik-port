package noty_team.com.masterovik.api.retrofit.apis

import android.content.Context
import noty_team.com.masterovik.base.AppMasterovik
import javax.inject.Inject

class MasterovikApi @Inject constructor(val context: Context) {

    @Inject
    lateinit var loginApi: LoginApi

    @Inject
    lateinit var cityApi: CityApi

    @Inject
    lateinit var profileApi: ProfileApi

    @Inject
    lateinit var servicesApi: ServicesApi

    @Inject
    lateinit var advertsApi: AdvertsApi

    @Inject
    lateinit var walletApi: WalletApi

    init {
        AppMasterovik[context].appComponent.inject(this)
    }

}
