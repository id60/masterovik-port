package noty_team.com.masterovik.api.response.city.cities_list

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ResponseItem(

	@field:SerializedName("city")
	val city: String = "",

	@field:SerializedName("id")
	val id: Int = -1
)