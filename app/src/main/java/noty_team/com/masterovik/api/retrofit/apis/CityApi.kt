package noty_team.com.masterovik.api.retrofit.apis

import android.content.Context
import io.reactivex.Single
import noty_team.com.masterovik.api.response.city.cities_list.CitiesResponse
import noty_team.com.masterovik.api.response.city.city.CityResponse
import noty_team.com.masterovik.api.retrofit.BaseRepository
import noty_team.com.masterovik.api.retrofit.api_interface.CityApiInterface
import noty_team.com.masterovik.utils.Result
import retrofit2.Retrofit
import javax.inject.Inject

class CityApi @Inject constructor(
        context: Context,
        retrofit: Retrofit,
        private var ctyApi: CityApiInterface): BaseRepository(context, retrofit) {


    fun getCities(onResult: (Result<CitiesResponse>) -> Unit): Single<CitiesResponse> {
        return initRequestSingleForResult(ctyApi.getCities(), onResult)
    }

    fun getCity(cityId: Int,
                onResult: (Result<CityResponse>) -> Unit): Single<CityResponse> {
        return initRequestSingleForResult(ctyApi.getCity(cityId), onResult)
    }
}
