package noty_team.com.masterovik.api.response.wallet.history

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ResponseItem(

	@field:SerializedName("balance")
	val balance: Int? = null,

	@field:SerializedName("user_id")
	val userId: Int? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("sum")
	val sum: Int? = null,

	@field:SerializedName("id")
	val id: Int = -1,

	@field:SerializedName("operation")
	val operation: String? = null,

	@field:SerializedName("card_id")
	val cardId: Int? = null
)