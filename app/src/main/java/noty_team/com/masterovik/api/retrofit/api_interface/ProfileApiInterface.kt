package noty_team.com.masterovik.api.retrofit.api_interface

import io.reactivex.Single
import noty_team.com.masterovik.api.request.change_email.ChangeEmailRequest
import noty_team.com.masterovik.api.request.change_phone.ChangePhoneRequest
import noty_team.com.masterovik.api.request.edit_profile.EditProfileRequest
import noty_team.com.masterovik.api.response.profile.ProfileResponse
import noty_team.com.masterovik.api.request.profile.ProfileRequest
import noty_team.com.masterovik.api.request.services.AddServiceRequest
import noty_team.com.masterovik.api.request.simple_request.SimpleAccessTokenRequest
import noty_team.com.masterovik.api.response.BaseResponse
import noty_team.com.masterovik.api.response.change_email.ChangeEmailResponse
import noty_team.com.masterovik.api.response.change_phone.ChangePhoneResponse
import noty_team.com.masterovik.api.response.edit_profile.EditProfileResponse
import retrofit2.http.Body
import retrofit2.http.POST

interface ProfileApiInterface {

    @POST("user/get-user-data")
    fun getProfile(@Body data: SimpleAccessTokenRequest): Single<ProfileResponse>

    @POST("user/change-user-phone")
    fun changePhone(@Body data: ChangePhoneRequest): Single<ChangePhoneResponse>

    @POST("user/change-user-email")
    fun changeEmail(@Body data: ChangeEmailRequest): Single<ChangeEmailResponse>

    @POST("user/edit-user-data")
    fun editProfile(@Body data: EditProfileRequest): Single<EditProfileResponse>

    @POST("user/add-service")
    fun addService(@Body data: AddServiceRequest): Single<BaseResponse>

    @POST("user/remove-service")
    fun deleteService(@Body data: AddServiceRequest): Single<BaseResponse>

//    @POST("service/get-user-services")
//    fun getUserServices(@Body body: CreateServiceRequest): Single<CreateServiceResponse>

}