package noty_team.com.masterovik.api.retrofit.apis

import android.content.Context
import io.reactivex.Single
import noty_team.com.masterovik.api.response.profile.ProfileResponse
import noty_team.com.masterovik.api.request.change_email.ChangeEmailRequest
import noty_team.com.masterovik.api.request.change_phone.ChangePhoneRequest
import noty_team.com.masterovik.api.request.edit_profile.EditProfileRequest
import noty_team.com.masterovik.api.request.profile.ProfileRequest
import noty_team.com.masterovik.api.request.services.AddServiceRequest
import noty_team.com.masterovik.api.request.simple_request.SimpleAccessTokenRequest
import noty_team.com.masterovik.api.response.BaseResponse
import noty_team.com.masterovik.api.response.change_email.ChangeEmailResponse
import noty_team.com.masterovik.api.response.change_phone.ChangePhoneResponse
import noty_team.com.masterovik.api.response.edit_profile.EditProfileResponse
import noty_team.com.masterovik.api.retrofit.BaseRepository
import noty_team.com.masterovik.api.retrofit.api_interface.ProfileApiInterface
import noty_team.com.masterovik.utils.Result
import retrofit2.Retrofit
import javax.inject.Inject

class ProfileApi @Inject constructor(
        context: Context,
        retrofit: Retrofit,
        private var profileApi: ProfileApiInterface): BaseRepository(context, retrofit) {

    fun getProfile(onResult: (Result<ProfileResponse>) -> Unit): Single<ProfileResponse> {
        val data = SimpleAccessTokenRequest()
        return initRequestSingleForResult(profileApi.getProfile(data), onResult)
    }

    fun changePhone(data: ChangePhoneRequest,
                    onResult: (Result<ChangePhoneResponse>) -> Unit): Single<ChangePhoneResponse> {
        return initRequestSingleForResult(profileApi.changePhone(data), onResult)
    }

    fun changeEmail(data: ChangeEmailRequest,
                    onResult: (Result<ChangeEmailResponse>) -> Unit): Single<ChangeEmailResponse> {
        return initRequestSingleForResult(profileApi.changeEmail(data), onResult)
    }

    fun editProfile(data: EditProfileRequest,
                    onResult: (Result<EditProfileResponse>) -> Unit): Single<EditProfileResponse> {
        return initRequestSingleForResult(profileApi.editProfile(data), onResult)
    }

    fun addService(data: AddServiceRequest,
                   onResult: (Result<BaseResponse>) -> Unit): Single<BaseResponse> {
        return initRequestSingleForResult(profileApi.addService(data), onResult)
    }

    fun deleteService(data: AddServiceRequest,
                    onResult: (Result<BaseResponse>) -> Unit): Single<BaseResponse> {
        return initRequestSingleForResult(profileApi.deleteService(data), onResult)
    }

}
