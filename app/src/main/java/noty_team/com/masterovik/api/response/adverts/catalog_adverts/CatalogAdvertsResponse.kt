package noty_team.com.masterovik.api.response.adverts.catalog_adverts

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.masterovik.api.response.BaseResponse

@Generated("com.robohorse.robopojogenerator")
data class CatalogAdvertsResponse(

	@field:SerializedName("response")
	val response: ArrayList<ResponseItem>? = null

): BaseResponse()