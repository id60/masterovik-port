package noty_team.com.masterovik.api.retrofit.apis

import android.content.Context
import io.reactivex.Single
import noty_team.com.masterovik.api.request.advert.get_catalog_adverts.CatalogAdvertsRequest
import noty_team.com.masterovik.api.response.profile.ProfileResponse
import noty_team.com.masterovik.api.request.change_email.ChangeEmailRequest
import noty_team.com.masterovik.api.request.change_phone.ChangePhoneRequest
import noty_team.com.masterovik.api.request.edit_profile.EditProfileRequest
import noty_team.com.masterovik.api.request.profile.ProfileRequest
import noty_team.com.masterovik.api.request.services.AddServiceRequest
import noty_team.com.masterovik.api.request.simple_request.SimpleAccessTokenRequest
import noty_team.com.masterovik.api.request.wallet.add_card.AddCardRequest
import noty_team.com.masterovik.api.request.wallet.add_money.AddMoneyRequest
import noty_team.com.masterovik.api.request.wallet.delete_card.DeleteCardRequest
import noty_team.com.masterovik.api.request.wallet.edit_card.EditCardRequest
import noty_team.com.masterovik.api.request.wallet.history.PaymentHistoryRequest
import noty_team.com.masterovik.api.response.BaseResponse
import noty_team.com.masterovik.api.response.adverts.my_adverts.MyAdvertsResponse
import noty_team.com.masterovik.api.response.change_email.ChangeEmailResponse
import noty_team.com.masterovik.api.response.change_phone.ChangePhoneResponse
import noty_team.com.masterovik.api.response.edit_profile.EditProfileResponse
import noty_team.com.masterovik.api.response.wallet.add_card.AddCardResponse
import noty_team.com.masterovik.api.response.wallet.add_money.AddMoneyResponse
import noty_team.com.masterovik.api.response.wallet.delete_card.DeleteCardResponse
import noty_team.com.masterovik.api.response.wallet.edit_card.EditCardResponse
import noty_team.com.masterovik.api.response.wallet.history.PaymentHistoryResponse
import noty_team.com.masterovik.api.response.wallet.my_cards.MyCardsResponse
import noty_team.com.masterovik.api.response.wallet.partners.PartnersResponse
import noty_team.com.masterovik.api.retrofit.BaseRepository
import noty_team.com.masterovik.api.retrofit.api_interface.AdvertsApiInterface
import noty_team.com.masterovik.api.retrofit.api_interface.ProfileApiInterface
import noty_team.com.masterovik.api.retrofit.api_interface.WalletApiInterface
import noty_team.com.masterovik.utils.Result
import retrofit2.Retrofit
import javax.inject.Inject

class WalletApi @Inject constructor(
        context: Context,
        retrofit: Retrofit,
        private var walletApi: WalletApiInterface): BaseRepository(context, retrofit) {

    fun getMyCards(onResult: (Result<MyCardsResponse>) -> Unit): Single<MyCardsResponse> {
        val data = SimpleAccessTokenRequest()
        return initRequestSingleForResult(walletApi.getMyCards(data), onResult)
    }

    fun addMoney(data: AddMoneyRequest,
                 onResult: (Result<AddMoneyResponse>) -> Unit): Single<AddMoneyResponse> {
        return initRequestSingleForResult(walletApi.addMoney(data), onResult)
    }

    fun addCard(data: AddCardRequest,
                onResult: (Result<AddCardResponse>) -> Unit): Single<AddCardResponse> {
        return initRequestSingleForResult(walletApi.addCard(data), onResult)
    }

    fun editCard(data: EditCardRequest,
                 onResult: (Result<EditCardResponse>) -> Unit): Single<EditCardResponse> {
        return initRequestSingleForResult(walletApi.editCard(data), onResult)
    }

    fun deleteCard(data: DeleteCardRequest,
                   onResult: (Result<DeleteCardResponse>) -> Unit): Single<DeleteCardResponse> {
        return initRequestSingleForResult(walletApi.deleteCard(data), onResult)
    }

    fun getPaymentHistory(data: PaymentHistoryRequest,
                          onResult: (Result<PaymentHistoryResponse>) -> Unit): Single<PaymentHistoryResponse> {
        return initRequestSingleForResult(walletApi.getPaymentHistory(data), onResult)
    }

    fun getPartners(onResult: (Result<PartnersResponse>) -> Unit): Single<PartnersResponse> {
        val data = SimpleAccessTokenRequest()
        return initRequestSingleForResult(walletApi.getPartners(data), onResult)
    }
}
