package noty_team.com.masterovik.api.response.auth.registration

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.masterovik.api.response.BaseResponse

@Generated("com.robohorse.robopojogenerator")
data class RegistrationResponse(

	@field:SerializedName("access_token")
	val accessToken: String = "",

	@field:SerializedName("refresh_token")
	val refreshToken: String = ""
): BaseResponse()