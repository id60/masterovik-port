package noty_team.com.masterovik.api.request.wallet.delete_card

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.masterovik.api.request.simple_request.SimpleAccessTokenRequest

@Generated("com.robohorse.robopojogenerator")
data class DeleteCardRequest(

	@field:SerializedName("card_id")
	val cardId: Int = -1

): SimpleAccessTokenRequest()