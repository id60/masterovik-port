package noty_team.com.masterovik.api.response.change_email

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.masterovik.api.response.BaseResponse

@Generated("com.robohorse.robopojogenerator")
data class ChangeEmailResponse(

	@field:SerializedName("message")
	val message: String? = null

): BaseResponse()