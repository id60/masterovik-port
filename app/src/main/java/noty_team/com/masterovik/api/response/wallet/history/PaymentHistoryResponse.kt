package noty_team.com.masterovik.api.response.wallet.history

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.masterovik.api.response.BaseResponse
import noty_team.com.masterovik.ui.adapters.items_adapter.HistoryPaymentItem
import noty_team.com.masterovik.ui.adapters.items_adapter.MyCardsItem

@Generated("com.robohorse.robopojogenerator")
data class PaymentHistoryResponse(

        @field:SerializedName("response")
        val response: List<ResponseItem>? = null

) : BaseResponse() {

    fun map(cards: ArrayList<MyCardsItem>): ArrayList<HistoryPaymentItem> {
        return response?.flatMap { response ->
            arrayListOf(
                    HistoryPaymentItem(
                            type =
                            if (response.operation == "Пополнение баланса") HistoryPaymentItem.OperationType.REFILL
                            else HistoryPaymentItem.OperationType.WITHDRAWAL,
                            date = response.createdAt?:"",
                            balance = response.balance?:0,
                            amount = response.sum?:0,
                            cardNumber = cards.find { response.cardId == it.id }?.cardNumber?:"",
                            id = response.id
                    )
            )
        } as ArrayList<HistoryPaymentItem>
    }

}