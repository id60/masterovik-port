package noty_team.com.masterovik.api.response.services.get_services

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.masterovik.api.response.BaseResponse
import noty_team.com.masterovik.ui.adapters.items_adapter.ProvideServiceItem

@Generated("com.robohorse.robopojogenerator")
data class GetServicesResponse(

	@field:SerializedName("response")
	val response: List<ResponseItem> = arrayListOf()

): BaseResponse() {

	fun map(): ArrayList<ProvideServiceItem> {
		return response.flatMap { arrayListOf(
				ProvideServiceItem(
						id = it.id,
						serviceName = it.name?:"",
						serviceUrl = it.logo?:""
				)
		) } as ArrayList<ProvideServiceItem>
	}

}