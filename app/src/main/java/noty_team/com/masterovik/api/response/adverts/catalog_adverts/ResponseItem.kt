package noty_team.com.masterovik.api.response.adverts.catalog_adverts

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ResponseItem(

	@field:SerializedName("announcement_title")
	val announcementTitle: ArrayList<AnnouncementTitleItem>? = null,

	@field:SerializedName("phone")
	val phone: String? = null,

	@field:SerializedName("price")
	val price: String? = null,

	@field:SerializedName("place_of_work")
	val placeOfWork: String? = null,

	@field:SerializedName("description_full")
	val descriptionFull: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("date_end")
	val dateEnd: String? = null,

	@field:SerializedName("id")
	val id: Int = -1,

	@field:SerializedName("create_at")
	val createAt: String? = null,

	@field:SerializedName("email")
	val email: String? = null,

	@field:SerializedName("city_id")
	val cityId: String? = null
)