package noty_team.com.masterovik.api.request.auth.register

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class RegistrationRequest(

	@field:SerializedName("first_name")
	var firstName: String = "",

	@field:SerializedName("last_name")
	var lastName: String = "",

	@field:SerializedName("email")
	var email: String? = null,

    @field:SerializedName("phone")
    var phone: String = "",

	@field:SerializedName("city_id")
	var cityId: Int = -1,

    @field:SerializedName("company_name")
    var companyName: String? = null,

    @field:SerializedName("role_id")
    var roleId: Int = -1,

    @field:SerializedName("company_description")
    var companyDescription: String? = null

)