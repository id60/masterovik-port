package noty_team.com.masterovik.api.response.wallet.partners

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.masterovik.api.response.BaseResponse
import noty_team.com.masterovik.ui.adapters.items_adapter.PartnerItem

@Generated("com.robohorse.robopojogenerator")
data class PartnersResponse(

        @field:SerializedName("referal_code")
        val referalCode: String = "",

        @field:SerializedName("response")
        val response: List<ResponseItem>? = null

) : BaseResponse() {

    fun map(): ArrayList<PartnerItem> {
        return response?.flatMap {
            arrayListOf(
                    PartnerItem(
                            date = it.date ?: "",
                            activationType =
                            if (it.activate == PartnerItem.PartnerActivationType.ACTIVATED.typeString)
                                PartnerItem.PartnerActivationType.ACTIVATED
                            else PartnerItem.PartnerActivationType.NOT_ACTIVATED,
                            partnerName = it.name ?: ""
                    )
            )
        } as ArrayList<PartnerItem>
    }

}