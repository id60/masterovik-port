package noty_team.com.masterovik.api.request.profile

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ProfileRequest(

	@field:SerializedName("access_token")
	val accessToken: String = ""
)