package noty_team.com.masterovik.api.request.simple_request

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.masterovik.utils.paper.PaperIO

@Generated("com.robohorse.robopojogenerator")
open class SimpleAccessTokenRequest(

	@field:SerializedName("access_token")
	val accessToken: String = PaperIO.getAccessToken()

)