package noty_team.com.masterovik.api.response.city.city

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.masterovik.api.response.BaseResponse

@Generated("com.robohorse.robopojogenerator")
data class CityResponse(

		@field:SerializedName("city")
		val city: String = "",

		@field:SerializedName("id")
		val id: Int = -1

): BaseResponse()