package noty_team.com.masterovik.api.request.change_email

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ChangeEmailRequest(

	@field:SerializedName("access_token")
	val accessToken: String = "",

	@field:SerializedName("email")
	val email: String = ""
)