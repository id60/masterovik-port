package noty_team.com.masterovik.api.request.wallet.edit_card

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class EditCardRequest(

	@field:SerializedName("card_id")
	val cardId: Int = -1,

	@field:SerializedName("card")
	val card: String = ""
)