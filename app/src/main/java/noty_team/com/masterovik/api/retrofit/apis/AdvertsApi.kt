package noty_team.com.masterovik.api.retrofit.apis

import android.content.Context
import io.reactivex.Single
import noty_team.com.masterovik.api.request.advert.get_catalog_adverts.CatalogAdvertsRequest
import noty_team.com.masterovik.api.response.profile.ProfileResponse
import noty_team.com.masterovik.api.request.change_email.ChangeEmailRequest
import noty_team.com.masterovik.api.request.change_phone.ChangePhoneRequest
import noty_team.com.masterovik.api.request.edit_profile.EditProfileRequest
import noty_team.com.masterovik.api.request.profile.ProfileRequest
import noty_team.com.masterovik.api.request.services.AddServiceRequest
import noty_team.com.masterovik.api.request.simple_request.SimpleAccessTokenRequest
import noty_team.com.masterovik.api.response.BaseResponse
import noty_team.com.masterovik.api.response.adverts.my_adverts.MyAdvertsResponse
import noty_team.com.masterovik.api.response.change_email.ChangeEmailResponse
import noty_team.com.masterovik.api.response.change_phone.ChangePhoneResponse
import noty_team.com.masterovik.api.response.edit_profile.EditProfileResponse
import noty_team.com.masterovik.api.retrofit.BaseRepository
import noty_team.com.masterovik.api.retrofit.api_interface.AdvertsApiInterface
import noty_team.com.masterovik.api.retrofit.api_interface.ProfileApiInterface
import noty_team.com.masterovik.utils.Result
import retrofit2.Retrofit
import javax.inject.Inject

class AdvertsApi @Inject constructor(
        context: Context,
        retrofit: Retrofit,
        private var advertsApi: AdvertsApiInterface): BaseRepository(context, retrofit) {

    fun getMyAdverts(onResult: (Result<MyAdvertsResponse>) -> Unit): Single<MyAdvertsResponse> {
        val data = SimpleAccessTokenRequest()
        return initRequestSingleForResult(advertsApi.getMyAdverts(data), onResult)
    }

    fun getCatalogAdverts(data: CatalogAdvertsRequest,
                          onResult: (Result<MyAdvertsResponse>) -> Unit): Single<MyAdvertsResponse> {
        return initRequestSingleForResult(advertsApi.getCatalogAdverts(data), onResult)
    }
}
