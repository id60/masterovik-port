package noty_team.com.masterovik.api.response.auth.login

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.masterovik.api.response.BaseResponse

@Generated("com.robohorse.robopojogenerator")
data class LoginResponse(

	@field:SerializedName("access_token")
	val accessToken: String? = null,

	@field:SerializedName("refresh_token")
	val refreshToken: String? = null,

	@field:SerializedName("role_id")
	val roleId: Int? = -1,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("isExist")
	val isExist: Boolean? = null

): BaseResponse()