package noty_team.com.masterovik.api.retrofit.api_interface

import io.reactivex.Single
import noty_team.com.masterovik.api.request.auth.login.LoginRequest
import noty_team.com.masterovik.api.request.auth.refresh_token.RefreshTokenRequest
import noty_team.com.masterovik.api.request.auth.register.RegistrationRequest
import noty_team.com.masterovik.api.response.auth.login.LoginResponse
import noty_team.com.masterovik.api.response.auth.refresh_token.RefreshTokenResponse
import noty_team.com.masterovik.api.response.auth.registration.RegistrationResponse
import retrofit2.http.Body
import retrofit2.http.POST

interface LoginApiInterface {

    @POST("user/refresh-token")
    fun refreshToken(@Body data: RefreshTokenRequest): Single<RefreshTokenResponse>

    @POST("user/login")
    fun login(@Body data: LoginRequest): Single<LoginResponse>

    @POST("user/create-user")
    fun register(@Body data: RegistrationRequest): Single<RegistrationResponse>

//    @POST("auth/logout")
//    fun logout(@Body refreshToken: RequestBody): Single<LogoutResponse>
//
//    @POST("auth/refresh")
//    fun refreshToken(@Body refreshToken: RequestBody): Single<RefreshTokenResponse>
//
//    @PATCH("customers/{customerId}/types/switch")
//    fun switchRole(@Path("customerId") customerId: Int,
//                   @Body refreshToken: RequestBody): Single<ResponseBody>
}