package noty_team.com.masterovik.api.response.edit_profile

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class EditProfileResponse(

	@field:SerializedName("message")
	val message: String? = null
)