package noty_team.com.masterovik.api.response.change_phone

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.masterovik.api.response.BaseResponse

@Generated("com.robohorse.robopojogenerator")
data class ChangePhoneResponse(

	@field:SerializedName("message")
	val message: String? = null

): BaseResponse()