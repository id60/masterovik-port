package noty_team.com.masterovik.api.request.wallet.history

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.masterovik.api.request.simple_request.SimpleAccessTokenRequest
import noty_team.com.masterovik.ui.adapters.items_adapter.MyCardsItem

@Generated("com.robohorse.robopojogenerator")
data class PaymentHistoryRequest(

	@field:SerializedName("new")
	val jsonMemberNew: Boolean? = null,

	@field:SerializedName("operation")
	val operation: String? = null,

	@field:SerializedName("card_id")
	val cardId: Int? = null,

	val card: MyCardsItem? = null

): SimpleAccessTokenRequest()