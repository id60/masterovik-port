package noty_team.com.masterovik.api.response.profile

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.masterovik.api.response.BaseResponse

@Generated("com.robohorse.robopojogenerator")
data class ProfileResponse(

	@field:SerializedName("last_name")
	var lastName: String? = "",

	@field:SerializedName("invited_by")
	val invitedBy: Any? = null,

	@field:SerializedName("company_description")
	var companyDescription: String? = "",

	@field:SerializedName("balance")
	var balance: String? = null,

	@field:SerializedName("phone")
	var phone: String? = "",

	@field:SerializedName("role_id")
	val roleId: Int? = -1,

	@field:SerializedName("company_name")
	var companyName: String? = "",

	@field:SerializedName("id")
	val id: Int? = -1,

	@field:SerializedName("invite")
	val invite: Any? = null,

	@field:SerializedName("statistics_id")
	val statisticsId: Any? = null,

	@field:SerializedName("first_name")
	var firstName: String? = "",

	@field:SerializedName("email")
	var email: String? = "",

	@field:SerializedName("city_id")
	var cityId: Int = -1

): BaseResponse()