package noty_team.com.masterovik.api.response.wallet.my_cards

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.masterovik.api.response.BaseResponse
import noty_team.com.masterovik.ui.adapters.items_adapter.MyCardsItem

@Generated("com.robohorse.robopojogenerator")
data class MyCardsResponse(

	@field:SerializedName("response")
	val response: ArrayList<ResponseItem>? = null

): BaseResponse() {

	fun map(): ArrayList<MyCardsItem> {
		return response?.flatMap { arrayListOf(
				MyCardsItem (
						id = it.id,
						cardNumber = it.card?:"карта удалена"
				)
		) } as ArrayList<MyCardsItem>
	}

}