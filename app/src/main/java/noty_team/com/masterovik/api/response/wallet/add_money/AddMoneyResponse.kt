package noty_team.com.masterovik.api.response.wallet.add_money

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.masterovik.api.response.BaseResponse

@Generated("com.robohorse.robopojogenerator")
data class AddMoneyResponse(

	@field:SerializedName("response")
	val response: String? = null

): BaseResponse()