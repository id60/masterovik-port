package noty_team.com.masterovik.api.request.wallet.add_card

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.masterovik.api.request.simple_request.SimpleAccessTokenRequest

@Generated("com.robohorse.robopojogenerator")
data class AddCardRequest(

	@field:SerializedName("card")
	val card: String = ""

): SimpleAccessTokenRequest()