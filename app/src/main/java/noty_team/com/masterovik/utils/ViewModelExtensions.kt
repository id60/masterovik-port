package noty_team.com.masterovik.utils

import android.arch.lifecycle.*
import android.support.annotation.NonNull
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import java.lang.Exception

inline fun <reified T : ViewModel> FragmentActivity.injectViewModel
        (factory: ViewModelProvider.Factory): T {
    return ViewModelProviders.of(this, factory)[T::class.java]
}

inline fun <reified T : ViewModel> Fragment.injectViewModel
        (factory: ViewModelProvider.Factory): T {
    return ViewModelProviders.of(this, factory)[T::class.java]
}

fun<T> LiveData<T>.observe(@NonNull owner: LifecycleOwner, onUpdate:(t: T) -> Unit){
//    if (!this.hasObservers())
        this.observe(owner, Observer { data ->
            try {
                data?.let { onUpdate(data) }
            }
            catch (e: Exception){

            }
        })
}