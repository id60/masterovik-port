package noty_team.com.masterovik.utils.validator

import android.telephony.PhoneNumberUtils
import android.text.TextUtils
import java.util.regex.Pattern

class FieldValidator : FieldsValidationInterface {

    override fun isPasswordValid(password: String): Boolean {
        //val pattern = "^[a-zа-яA-ZА-Я0-9_@#\$%^&+=]*\$"
        val pattern = "^[\\S]*\$"
        val result = Pattern.compile(pattern).matcher(password).matches()
        return result
    }

    override fun isPromoCodeValid(password: String): Boolean {
        val pattern = "^[a-zа-яA-ZА-Я0-9]*\$"
        val result = Pattern.compile(pattern).matcher(password).matches()
        return result
    }

    override fun isPhoneValid(phone: String): Boolean {
        val newPhone = getValidPhone(phone)

        val phoneValidValue = PhoneNumberUtils.isGlobalPhoneNumber(newPhone)
        return phoneValidValue && (newPhone.length == 11 || newPhone.length == 12)
    }

    override fun isSmsValid(sms: String): Boolean {
        return sms.length == 4
    }

    override fun getValidPhone(phone: String): String {
        val regex = Regex("[^0-9]")
        val validPhone = regex.replace(phone, "")
        return validPhone
    }

    override fun isEmailValid(email: String): Boolean {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    override fun isPasswordLongEnough(password: String): Boolean {
        return password.length >= 6
    }

    override fun isPasswordsSame(password: String, repeatPassword: String): Boolean {
       return password == repeatPassword
    }

    override fun isCardNumberValid(cardNumber: String): Boolean {
        return TextUtils.isDigitsOnly(cardNumber) && cardNumber.length == 16
    }
}