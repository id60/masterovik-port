package noty_team.com.masterovik.utils

import android.app.Activity
import android.app.DatePickerDialog
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object CustomDatePicker {

    private const val datePattern = "dd MMM yyyy"

    fun show(activity: Activity, calendar: Calendar, onDateSelected: (String) -> Unit) {

        val callback: DatePickerDialog.OnDateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            calendar.set(Calendar.YEAR, year)
            calendar.set(Calendar.MONTH, monthOfYear)
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            onDateSelected(getFormattedDateString(calendar))
        }

        DatePickerDialog(activity, callback,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH))
                .show()
    }

    fun getFormattedDateString(calendar: Calendar): String {
        val dateFormat = SimpleDateFormat(datePattern, getLocale())
        try {
            return dateFormat.format(calendar.time)
        } catch (pe: ParseException) {
            return ""
        }
    }

    fun getFormattedDateString(timeStamp: Long): String {
        return getFormattedDateString(
                Calendar.getInstance(getLocale()).apply { timeInMillis = timeStamp })
    }
}