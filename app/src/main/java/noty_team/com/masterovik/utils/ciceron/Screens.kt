package noty_team.com.masterovik.utils.ciceron

import android.support.v4.app.Fragment

import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.base.mvvm.BaseViewModel

import ru.terrakok.cicerone.android.support.SupportAppScreen


object Screens {

    class FragmentScreen2<P: BaseViewModel, T: BaseFragment<P>>(var fragment : T): SupportAppScreen() {
        override fun getFragment(): Fragment {
            return fragment
        }
    }
}