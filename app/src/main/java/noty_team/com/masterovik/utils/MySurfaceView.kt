package noty_team.com.masterovik.utils

import android.content.Context
import android.util.AttributeSet
import android.view.SurfaceHolder
import android.view.SurfaceView
import android.view.View
import android.graphics.*
import android.view.MotionEvent
import kotlin.math.abs
import kotlin.math.pow
import kotlin.math.sqrt

class CircleSelectorAnimation : View {
    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes)

    private var circleSelector = CircleSelector()
    private var touchX = 0f
    private var touchY = 0f

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        circleSelector.width = (right - left).toFloat()
        circleSelector.height = (bottom - top).toFloat()
        circleSelector.reset(context)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        circleSelector.drawToPoint(canvas, touchX, touchY)
        postInvalidateOnAnimation()
    }

    fun reset(): CircleSelectorAnimation {
        circleSelector.reset(context)
        return this
    }

    fun setSpeed(speed: Float): CircleSelectorAnimation {
        circleSelector.step = speed
        return this
    }

    fun setLineWidth(lineWidth: Float): CircleSelectorAnimation {
        circleSelector.paint.strokeWidth = lineWidth
        return this
    }

    fun setTopCircleRadius(radius: Float): CircleSelectorAnimation {
        circleSelector.topCircleRadius = radius
        return this
    }

    fun setBottomCircleRadius(radius: Float): CircleSelectorAnimation {
        circleSelector.bottomCircleRadius = radius
        return this
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                touchX = event.x
                touchY = event.y
                circleSelector.resetByPoint(touchX, touchY)
                invalidate()
            }
//            MotionEvent.ACTION_MOVE -> {
//                moveTouch(x, y)
//                invalidate()
//            }
//            MotionEvent.ACTION_UP -> {
//                upTouch()
//                invalidate()
//            }
        }
        return true
    }
}

class CircleSelector(var width: Float = 1f, var height: Float = 1f) {
    var step = 1f
    var bottomCircleRadius = 1f
    var topCircleRadius = 1f

    var bottomCircleX = 1f
    var bottomCircleY = 1f

    private var stepX = 0f
    private var stepY = 0f
    private var lastSavedTouchX = 0f
    private var lastSavedTouchY = 0f

    val paint = Paint().apply {
        color = Color.CYAN
        style = Paint.Style.FILL
    }

    fun reset(context: Context){
        step = convertDpToPixel(20f, context)
        bottomCircleRadius = (width - 2 * convertDpToPixel(100f, context)) / 2
        topCircleRadius = convertDpToPixel(24f, context)
        paint.strokeWidth = convertDpToPixel(6f, context)

        bottomCircleY = height / 2
        bottomCircleX = width / 2

        lastSavedTouchX = width / 2
        lastSavedTouchY = topCircleRadius
    }

    fun resetByPoint(touchX: Float, touchY: Float) {
        var a = abs(lastSavedTouchX - touchX)
        a = if (a == 0f) 1f else a
        var b = abs(lastSavedTouchY - touchY)
        b = if (b == 0f) 1f else b
        val hypotenuse = sqrt(a.toDouble().pow(2.0) + b.toDouble().pow(2.0)).toFloat()
        stepX = hypotenuse / b
        stepY = hypotenuse / a
        val proportion = stepX / stepY

        val currentStep = width * step / hypotenuse
        val stepFixCoefficient = currentStep / step
        val stepFix = step / stepFixCoefficient

        if (stepX > stepY && stepX > step
                || stepX > stepY && stepX < step) {
            stepX = stepFix
            stepY = stepX / proportion
        } else if (stepY > stepX && stepY > step
                || stepY > stepX && stepY < step) {
            stepY = stepFix
            stepX = stepY * proportion
        }
        val ass = 90
    }

    fun drawToPoint(canvas: Canvas, touchX: Float, touchY: Float) {
        if (touchX != lastSavedTouchX) {
            if (touchX > lastSavedTouchX && lastSavedTouchX + stepX > touchX ||
                    touchX < lastSavedTouchX && lastSavedTouchX - stepX < touchX) {
                lastSavedTouchX = touchX
            }

            lastSavedTouchX += when {
                touchX > lastSavedTouchX -> {
                    stepX
                }
                touchX < lastSavedTouchX -> {
                    -stepX
                }
                else -> {
                    lastSavedTouchX = touchX
                    0f
                }
            }
        }

        if (touchY != lastSavedTouchY) {
            if (touchY > lastSavedTouchY && lastSavedTouchY + stepY > touchY ||
                    touchY < lastSavedTouchY && lastSavedTouchY - stepY < touchY) {
                lastSavedTouchY = touchY
            }
            lastSavedTouchY += when {
                touchY > lastSavedTouchY -> {
                    stepY
                }
                touchY < lastSavedTouchY -> {
                    -stepY
                }
                else -> {
                    lastSavedTouchY = touchY
                    0f
                }
            }
        }

        canvas.drawCircle(bottomCircleX, bottomCircleY, bottomCircleRadius, paint)
        canvas.drawLine(bottomCircleX, bottomCircleY, lastSavedTouchX, lastSavedTouchY, paint)
        canvas.drawCircle(lastSavedTouchX, lastSavedTouchY, topCircleRadius, paint)

    }
}
