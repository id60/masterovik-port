package noty_team.com.masterovik.utils

import android.content.Context
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.Snackbar
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.widget.FrameLayout

class MoveUpwardBehavior: CoordinatorLayout.Behavior<FrameLayout> {
    var chlen = true
//    interface ChlenInterface {
//        fun onChlenChanged(): Boolean
//    }
//    var chlenInterface: ChlenInterface = object: ChlenInterface{
//        override fun onChlenChanged() {
//            return chlen
//        }
//    }
    constructor(/*chlenInterface: ChlenInterface*/) : super(){
        /*this@MoveUpwardBehavior.chlenInterface = chlenInterface*/
    }
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)


    override fun layoutDependsOn(parent: CoordinatorLayout, child: FrameLayout, dependency: View): Boolean {
        return dependency is Snackbar.SnackbarLayout
    }

    override fun onDependentViewChanged(parent: CoordinatorLayout, child: FrameLayout, dependency: View): Boolean {
        if (chlen) {
            val diff = dependency.translationY - dependency.height
            val translationY = Math.min(0f, diff)
            child.translationY = translationY
//        Log.d("Snackbar", translationY.toString())
        }
        return true
    }

    override fun onDependentViewRemoved(parent: CoordinatorLayout, child: FrameLayout, dependency: View) {
        if (chlen) {
            val diff = dependency.translationY - dependency.height
            val translationY = Math.min(0f, diff)
            child.translationY = translationY
//        Log.d("Snackbar222", translationY.toString())
        }
    }
}