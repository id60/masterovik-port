package noty_team.com.masterovik.utils.ciceron

import noty_team.com.masterovik.base.BaseFragment
import noty_team.com.masterovik.base.mvvm.BaseViewModel

interface CiceroneFragmentManager {

    fun <P: BaseViewModel> replaceFragment(fragment: BaseFragment<P>)

    fun <P: BaseViewModel> addFragment(fragment: BaseFragment<P>)

    fun <P: BaseViewModel> newRootFragment(fragment: BaseFragment<P>)

    fun exit()
}