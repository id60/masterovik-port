package noty_team.com.masterovik.utils

import android.content.Context
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.Snackbar
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout

class MovableLayout: FrameLayout, CoordinatorLayout.AttachedBehavior {

    val behaviour = MoveUpwardBehavior()

    fun enableSnackBarBehaviour(change: Boolean){
        behaviour.chlen = change
    }

    constructor(context: Context): super(context) {

    }

    constructor(context: Context, attrs: AttributeSet): super(context, attrs) {

    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int): super(context, attrs, defStyleAttr) {

    }

    override fun getBehavior(): CoordinatorLayout.Behavior<*> {
        return behaviour
    }

    class EmptyBehavior: CoordinatorLayout.Behavior<FrameLayout> {
        constructor() : super()
        constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)

    }
}