package noty_team.com.masterovik.utils

object CreditCardUtils {

    fun getCardImageLink(number: String): String{
        var type: CardTypes? = null
        if (number.startsWith("4")){
            type = CardTypes.VISA
        }

        for(i in 50..55) {
            if (number.startsWith(i.toString())){
                type = CardTypes.MASTER_CARD
                break
            }
        }

        if (number.startsWith("6011") || number.startsWith("65"))
            type = CardTypes.DISCOVER
        for(i in 622126..622925) {
            if (number.startsWith(i.toString())){
                type = CardTypes.DISCOVER
                break
            }
        }
        for(i in 644..649) {
            if (number.startsWith(i.toString())){
                type = CardTypes.DISCOVER
                break
            }
        }

        if (number.startsWith("34") || number.startsWith("37"))
            type = CardTypes.AMERICAN_EXPRESS

        for(i in 300..305) {
            if (number.startsWith(i.toString())){
                type = CardTypes.DINERS_CLUB
                break
            }
        }
        if (number.startsWith("309") || number.startsWith("36")
                || number.startsWith("38") || number.startsWith("39")
        /*|| number.startsWith("54") || number.startsWith("55")*/)
            type = CardTypes.DINERS_CLUB

        for(i in 3528..3589) {
            if (number.startsWith(i.toString())){
                type = CardTypes.JCB
                break
            }
        }

        if (number.startsWith("5018") || number.startsWith("5020")
                || number.startsWith("5038") || number.startsWith("5612")
                || number.startsWith("5893") || number.startsWith("6304")
                || number.startsWith("6759") || number.startsWith("6761")
                || number.startsWith("6762") || number.startsWith("6763")
                || number.startsWith("0604") || number.startsWith("6390"))
            type = CardTypes.MAESTRO

        return if (type != null) "https://www.merchantequip.com/image/?logos=${type?.code}&height=128" else ""
    }

    enum class CardTypes(val code: String){
        VISA("v"),
        MASTER_CARD("m"),
        DISCOVER("d"),
        AMERICAN_EXPRESS("a"),
        DINERS_CLUB("dc"),
        JCB("jcb"),
        MAESTRO("me")
    }

}
