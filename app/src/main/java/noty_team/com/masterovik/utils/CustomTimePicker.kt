package noty_team.com.masterovik.utils

import android.app.Activity
import android.app.DatePickerDialog
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import android.widget.TimePicker
import android.app.TimePickerDialog

object CustomTimePicker {

    private const val timePattern = "H"
    private val timeFormat = SimpleDateFormat(timePattern, getLocale())

    fun show(activity: Activity, calendar: Calendar, onTimeSelected: (String) -> Unit) {
        val callback: TimePickerDialog.OnTimeSetListener
                = TimePickerDialog.OnTimeSetListener { timePicker, selectedHour, selectedMinute ->
            calendar.set(Calendar.HOUR_OF_DAY, selectedHour)
            calendar.set(Calendar.MINUTE, 0)
            onTimeSelected(getFormattedTimeString(calendar))
        }

        TimePickerDialog(activity, callback,
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                true)
                .show()
    }

    fun getTimeCalendar(timeString: String): Calendar {
        val cal = Calendar.getInstance(getLocale())
        return try {
            val date = timeFormat.parse(timeString)
            cal.time = date
            cal
        } catch (pe: ParseException) {
            cal
        }
    }

    fun getTimeCalendar(timeStamp: Long): Calendar {
        return Calendar.getInstance(getLocale()).apply { timeInMillis = timeStamp }
    }

    fun getFormattedTimeString(calendar: Calendar): String {
        return try {
            timeFormat.format(calendar.time)
        } catch (pe: ParseException) {
            ""
        }
    }

    fun getFormattedTimeString(timeStamp: Long): String {
        return getFormattedTimeString(getTimeCalendar(timeStamp))
    }
}