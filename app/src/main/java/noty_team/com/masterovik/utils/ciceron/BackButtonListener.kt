package noty_team.com.masterovik.utils.ciceron

interface BackButtonListener {
    fun onBackPressed(): Boolean
}