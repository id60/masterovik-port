package noty_team.com.masterovik.utils.enums

enum class UserRole(val id: Int){
    EMPLOYER(2),
    EXECUTOR(1)
}