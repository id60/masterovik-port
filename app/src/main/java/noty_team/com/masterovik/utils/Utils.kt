package noty_team.com.masterovik.utils

import android.content.Context
import android.content.res.Resources
import android.graphics.drawable.Drawable
import android.support.annotation.ArrayRes
import android.support.v4.text.HtmlCompat
import android.text.Spanned
import android.util.DisplayMetrics
import android.view.View
import android.view.ViewTreeObserver
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.ImageViewTarget
import com.bumptech.glide.request.target.Target
import java.util.*

fun getLocale() = Locale("ru")

fun getStringArray(@ArrayRes stringArray: Int, c: Context): Array<out String> = c.resources.getStringArray(stringArray)

fun convertPixelsToDp(px: Float, context: Context?): Float {
    return if (context != null) {
        val resources = context.resources
        val metrics = resources.displayMetrics
        px / (metrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
    } else {
        val metrics = Resources.getSystem().displayMetrics
        px / (metrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
    }
}

fun convertDpToPixel(dp: Float, context: Context?): Float {
    return if (context != null) {
        val resources = context.resources
        val metrics = resources.displayMetrics
        dp * (metrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
    } else {
        val metrics = Resources.getSystem().displayMetrics
        dp * (metrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
    }
}

fun String.fromHtml(): Spanned {
    return HtmlCompat.fromHtml(this, HtmlCompat.FROM_HTML_MODE_LEGACY)
}

fun View.listenForDrawn(onViewDrawn:(view: View)->Unit){
    val view = this
    view.viewTreeObserver.addOnGlobalLayoutListener(object: ViewTreeObserver.OnGlobalLayoutListener{
        override fun onGlobalLayout() {
            view.viewTreeObserver.removeOnGlobalLayoutListener(this)
            onViewDrawn(view)
        }
    })
}

fun ImageView.loadImage(url: String, progress: View? = null, onSuccess:(resource: Drawable)->Unit = {}, onError:()->Unit = {}) {
    if (url != "") {
//        var httpsUrl = ""
//        if (url.startsWith("http://"))
//            httpsUrl = url.replace("http://", "https://")

        progress?.visible()
        val options = RequestOptions()
                .encodeQuality(33)
                .centerInside()
                .centerCrop()
//                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
        Glide.with(this.context)
                .load(url)
                .apply(options)
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(e: GlideException?, model: Any, target: Target<Drawable>, isFirstResource: Boolean): Boolean {
                        progress?.gone()
                        onError()
                        return false
                    }

                    override fun onResourceReady(resource: Drawable, model: Any, target: Target<Drawable>, dataSource: DataSource, isFirstResource: Boolean): Boolean {
                        progress?.gone()
                        onSuccess(resource)
                        return false
                    }
                })
                .into(this)
    }
}