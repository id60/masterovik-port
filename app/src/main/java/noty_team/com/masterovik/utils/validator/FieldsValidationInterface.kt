package noty_team.com.masterovik.utils.validator

interface FieldsValidationInterface {
    fun isPasswordValid(password: String): Boolean
    fun isPromoCodeValid(password: String): Boolean
    fun isPhoneValid(phone: String): Boolean
    fun isSmsValid(sms: String): Boolean
    fun isEmailValid(email: String): Boolean
    fun isPasswordLongEnough(password: String): Boolean
    fun isPasswordsSame(password: String, repeatPassword: String): Boolean
    fun isCardNumberValid(cardNumber: String): Boolean

    fun getValidPhone(phone: String): String
}