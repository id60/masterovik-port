package noty_team.com.masterovik.utils.paper

import io.paperdb.Paper
import noty_team.com.masterovik.api.response.profile.ProfileResponse
import noty_team.com.masterovik.utils.enums.UserRole

class PaperIO {
    companion object {
        private const val ACCESS_TOKEN = "ACCESS_TOKEN"
        private const val REFRESH_TOKEN = "REFRESH_TOKEN"
        private const val USER_ROLE = "USER_ROLE"

        private const val USER_PROFILE = "USER_PROFILE"

        fun setAccessToken(token: String) {
            Paper.book().write(ACCESS_TOKEN, token)
        }
        fun getAccessToken(): String {
            return try {
                Paper.book().read(ACCESS_TOKEN)?:""
            } catch (e: Exception) {
                ""
            }
        }

        fun setRefreshToken(token: String) {
            Paper.book().write(REFRESH_TOKEN, token)
        }
        fun getRefreshToken(): String {
            return try {
                Paper.book().read(REFRESH_TOKEN)?:""
            } catch (e: Exception) {
                ""
            }
        }

        fun setRole(role: UserRole) {
            Paper.book().write(USER_ROLE, role)
        }
        fun getRole(): UserRole {
            return try {
                Paper.book().read(USER_ROLE)?:UserRole.EMPLOYER
            } catch (e: Exception) {
                UserRole.EMPLOYER
            }
        }

        fun setUserProfile(user: ProfileResponse) {
            Paper.book().write(USER_PROFILE, user)
        }
        fun getUserProfile(): ProfileResponse {
            return try {
                Paper.book().read(USER_PROFILE)?: ProfileResponse()
            } catch (e: Exception) {
                ProfileResponse()
            }
        }

        fun clear() {
            Paper.book().allKeys.forEach {
                Paper.book().delete(it)
            }
        }
    }
}